Changes in BWI project 2.24.0 (2025-02-17)
===================================================

Upstream merge ✨:

Features ✨:
- MESSENGER-6856 add link to the learning portal
- MESSNEGER-6854 show happy birthday popup
- MESSENGER-6879 add support for authenticated media
- MESSENGER-6790 add handling of expired jwt

Improvements 🙌:
- MESSENGER-6777 use baseURL from wellknown for jwt handling

Bugfix 🐛:

Translations 🗣   :

SDK API changes ⚠️:

Build 🧱:

Documentation 📄:

Changes in BWI project 2.23.1 (2025-01-08)
===================================================

Upstream merge ✨:

Features ✨:

Improvements 🙌:
- MESSENGER-6727 replace makesalt with a more secure function
- MESSENGER-6846 save sensible data in app space

Bugfix 🐛:
- MESSENGER-6726 force logout in case of incorrect pin

Translations 🗣   :

SDK API changes ⚠️:

Build 🧱:

Documentation 📄:

Changes in BWI project 2.23.0 (2024-11-04)
===================================================

Upstream merge ✨:
- v1.11.19

Features ✨:
- MESSNEGER-6570 show happy birthday popup
- MESSENGER-6595 add hash
- MESSENGER-6542 change appearance of matomo alert

Improvements 🙌:

Bugfix 🐛:
- MESSENGER-6440 security fix - removed unused library OLMKit 
- MESSENGER-6687 fix token fetching

Translations 🗣   :

SDK API changes ⚠️:

Build 🧱:

Documentation 📄:

Changes in BWI project 2.22.0 (2024-10-08)
===================================================

Upstream merge ✨:
- v1.11.18

Features ✨:
- MESSENGER-6522 add hash

Improvements 🙌:
- MESSENGER-6526 clarify what happens to room if last room member leaves
- MESSENGER-5783 clarify last room member with open invites

Bugfix 🐛:
- MESSENGER-6393 fix permalink pill for messages
- MESSENGER-6348 fix invite notifications

Translations 🗣   :

SDK API changes ⚠️:
- MESSENGER-6076 disable room message retention

Build 🧱:
- MESSENGER-6377 Use match for apple certificates

Documentation 📄:

Changes in BWI project 2.21.0 (2024-08-09)
===================================================

Upstream merge ✨:

Features ✨:
- MESSENGER-6162 add server selection protection with jwt
- MESSENGER-6273 add hash
- MESSENGER-6236 add hash
- MESSENGER-6229 add hash
- MESSENGER-6372 add hash

Improvements 🙌:

Bugfix 🐛:
- MESSENGER-5896 disable periodic maintenance on logout
- MESSENGER-4172 use fixed sdk version
- MESSENGER-4172 read message marker

Translations 🗣   :

SDK API changes ⚠️:

Build 🧱:
- MESSENGER-6243 change ci runner

Documentation 📄:
- MESSENGER-MESSENGER new license file

Changes in BWI project 2.20.0 (2024-07-04)
===================================================

Upstream merge ✨:

Features ✨:

Improvements 🙌:
- MESSENGER-6171 federated rooms are indicated is visible on invitation screens
- MESSENGER-6103 Nicer store grafics

Bugfix 🐛:
- MESSENGER-6173 Status events at room creation are visible again
- MESSENGER-6138 Exiting the app on first start does not cause passphrase generation to be skipped
- MESSENGER-6152 Fix crash on muting a user
- MESSENGER-5716 Fix error messages when entering a room that is no longer federated
- MESSENGER-6019 Fix eeor message when following a permalink to a message that can't be unencrypted because the user was not part of the room at that time

Translations 🗣   :

SDK API changes ⚠️:

Build 🧱:
- MESSENGER-6142 Removed some unused libraries (posthog, sentry, jitsi)

Changes in BWI project 2.19.0 (2024-06-03)
===================================================

Upstream merge ✨:

Features ✨:

Improvements 🙌:
- Alligne timeout information text with other platforms for simplified login (disabled) (#6018)

Bugfix 🐛:
- Fix multiple permalink bugs (#6019)
- Fix feature banner not visible has not joined a room yet (#6114)

Translations 🗣   :

SDK API changes ⚠️:

Build 🧱:

Changes in BWI project 2.18.0 (2024-05-03)
===================================================

Upstream merge ✨:
- v1.11.9

Features ✨:

Improvements 🙌:
- Add save alert for changing users avatar (#5799)
- Change matomo alert text (#5829)

Bugfix 🐛:
- Fixed a crash when trying to add system errors to matrix error list (#5950)
- Fix picture selection for simulators (#5872)
- Fix button font in dark mode (#5999)
- Deactivate save button for functions if no text is provided (#5791)

Translations 🗣   :

SDK API changes ⚠️:

Build 🧱:
- Use XCode 15 for builds (#5948)
- Parallelized builds (#5761)

Changes in BWI project 2.17.0 (2024-04-11)
===================================================

Upstream merge ✨:
- v1.11.8

Features ✨:
- Simplified login (disabled) (#5145)
- Federation announcement in new feature banner (#5458)

Improvements 🙌:
- Activate federation support with well-known (#5892)
- Reveal lab settings with hidden gesture (#5938)
- Use new text for unknown and remove filename for size (#5793)

Bugfix 🐛:
- Disable slash commands in old composer (#5951)
- Deleted location sharing events were shown as live location (#5806)
- Maintenance requests sometimes not handled in sync (#5556)

Translations 🗣   :

SDK API changes ⚠️:

Build 🧱:

Changes in BWI project 2.16.0 (2024-03-13)
===================================================

Upstream merge ✨:
- v1.11.6

Features ✨:
- Use new iOS Fotopicker (#5365)

Improvements 🙌:
- Federation Announcement (#5706)
- Federation explanation for avatars (disabled) (#5610)
- Federation Introduction (disabled) (#5660)
- Show ACL related status events in timeline (disabled) (#5575)
- Matomo Events for federation (disabled) (#5393)
- Success message when federating a room (disabled) (#5578)
- Remove "Remind later" Button from alert for unfederated rooms (#5715)
- Better text for passphrase reset (#5595)
- disallow http (#5759)
- Better keybackup UI (#5668)
- Better Readme (#5734)
- Change composer default to old composer (bwm only) (#5768)

Bugfix 🐛:
- Room and user Avatars without borders (#5697)
- Remove federation status from invites (#5715)
- Fix Links to answers leading out of the app (#5603)

Translations 🗣  :

SDK API changes ⚠️:

Build 🧱:

Changes in BWI project 2.15.0 (2024-02-13)
===================================================

Upstream merge ✨:
- v1.11.6

Features ✨:
- Added federation decision view for old rooms (disabled) (#5304)

Improvements 🙌:
- Changed layout position of matrix id in user search (#5205)
- Mark federated users when joining / leaving rooms (disabled) (#5290)
- Added federation in timeline (disabled) (#5223)
- Added federation in timeline header (disabled) (#5226)
- No federated admins (disabled) (#5381)
- Added federation info about users in member search (disabled) (#5300)
- Users can only be invited to a room if acl is set (disabled) (#5386)
- Check alias when acl settings are changed (disabled) (#5383)
- Use the latest avatar in timeline (#5506)

Bugfix 🐛:
- Swipe gesture for removing room members from a room have been removed (#5671)
- Fix multiple notes rooms (#5740)
- Fix permalink prefix (#5158)
- Fix ACL json (disabled) (#5681)

Translations 🗣  :

SDK API changes ⚠️:

Build 🧱:

Changes in BWI project 2.14.0 (2024-01-16)
===================================================

Upstream merge ✨:

Features ✨:
- Change federation setting in room settings (disabled) (#5410)
- Change fedration setting on room creation (disabled) (#4846)

Improvements 🙌:
- Federation info about users in member details (disabled) (#5204)

Bugfix 🐛:
- Better handling of permalink prefix on testservers (#5158)
- Outdated app versions shown as pop up (#5276)
- Copyright claims for location sharing set correctly from style.json (#5279)
- Corrected appstore link for app version popup (#5276)

Translations 🗣  :

SDK API changes ⚠️:

Build 🧱:
- Enable Renovate
- Fix Sonarcube warnings

Changes in BWI project 2.12.1 (2023-12-11)
===================================================

Upstream merge ✨:

Features ✨:

Improvements 🙌:
- Ability to call SSO over browsers via MDM (#5316, #5308)
- Crosssigning request is now triggered by a button (#5271)
- UI Update for server display in login view (+5319s)

Bugfix 🐛:
- Fix half shown button in room list for new user (#5307)

Translations 🗣  :

SDK API changes ⚠️:

Build 🧱:

Changes in BWI project 2.12.0 (2023-11-21)
===================================================

Upstream merge ✨:

Features ✨:

Improvements 🙌:
- Layout changes OIDC login (#5033)
- Added restore key backup in settings (#5184)
- Added federation icons for beta app (#5203, #5208, #5220)
- Remove grey shield (#5236)
- Tracking of decryption duration (#5265)

Bugfix 🐛:
- Fix empty chat bubble (#5297)

Translations 🗣  :

SDK API changes ⚠️:

Build 🧱:

Changes in BWI project 2.11.0 (2023-10-24)
===================================================

Upstream merge ✨:
- v1.11.4

Features ✨:

Improvements 🙌:
- Corrected behaviour on errors in maintenance (#5031)
- Tracking of Rust encryption errors with matomo (#4956)
- Tracking of room size dimension for encryption errors (#4821)
- Use content scanner upload size correctly for all file types (#4433)

Bugfix 🐛:
- Crashfix notes room favorite check (#5240)

Translations 🗣  :

SDK API changes ⚠️:

Build 🧱:

Changes in BWI project 2.10.0 (2023-09-26)
===================================================

Upstream merge ✨:
- v1.11.1

Features ✨:
- New WYSIWYG composer (#4941)

Improvements 🙌:
- UI optimization room overview (#4704, #4806)
- Change date format for poll details (#4798)
- Add additional information to the developer menu for Beta app (#4565)
- Update huddle permissions for new rooms (#4928)
- Update element call permissions when resetting room permissions (#5035)
- Disable all WYSIWYG commands (#4955)
- Change date format for maintenance to ISO-8601 (#5071)
- Update notes room layout in room overview (#4802)
- Enable/disable notes room in settings (#4730)

Bugfix 🐛:
- Crashfix for dm creation when WYSIWYG composer is enabled (#5133)
- Change permalink URL scheme (#4910)
- No popup for successful password change (#4951)

Translations 🗣  :
- German translations voice message (#5017)
- German translations wrong password (#4857)
- Add special characters for password creation/resetting (#4952)

SDK API changes ⚠️:

Build 🧱:

Changes in BWI project 2.9.1 (2023-09-08)
===================================================

Upstream merge ✨:

Features ✨:

Improvements 🙌:

Bugfix 🐛:
- Maintenance Popup on server selection (#5130)

Translations 🗣  :

SDK API changes ⚠️:

Build 🧱:

Changes in BWI project 2.9.0 (2023-08-31)
===================================================

Upstream merge ✨:

- v1.10.14

Features ✨:

Improvements 🙌:
- Maintenance fixes (#4979, #4976)
- Maintenance ignore blocking (#4982)
- Maintenance adhoc messages (#4295)
- Maintenance time zone change (#5071)
- Huddle set correct rights (#5035, #4928)

Bugfix 🐛:
- Crosssigning web

Translations 🗣  :

SDK API changes ⚠️:

Build 🧱:


Changes in BWI project 2.8.0 (2023-08-03)
===================================================

Upstream merge ✨:

- v1.10.14

Features ✨:
- New show participants toggle for polls (#4393)

Improvements 🙌:
- Migration progress information (#4905)
- Additional information for maintance (#4295)
- Poll history information (#4484)
- Notification times (#3580)

Bugfix 🐛:

Translations 🗣  :

SDK API changes ⚠️:

Build 🧱:

Changes in BWI project 2.7.0 (2023-07-04)
===================================================

Upstream merge ✨:

- v1.10.12

Features ✨:
- New show participants toggle for polls (#4393)

Improvements 🙌:
- Roomavatar can now be deleted (#4743)
- Remove "black" theme (#4744)
- Open links in system browser (#1678)
- Add imprint (#4682)
- Text changes for DM creation (#4736)
- Add accessibility statement (#4772)
- Matomo tracking of poll creation (#4795)
- Matomo tracking of voice messages (#4795)
- Matomo tracking of forwarding messages (#4795)

Bugfix 🐛:
- Disable logout when there is no internet connection (#3539)
- Disable permalink sharing for private rooms (#4390)
- Fix manual verification (#4710)
- Fix QR code scanning (#4748)
- Show app logo in pin code screen (#4828)
- Update "all chats" filter on logout/login (#4573)

Translations 🗣   :
- English translations passphrase (#4706)

SDK API changes ⚠️:

Build 🧱:


Changes in BWI project 2.6.0 (2023-05-09)
===================================================

Upstream merge ✨:

- v1.10.9

Features ✨:
- New device Manager (#4516)

Improvements 🙌:
- Matomo tracking consent (#4450)
- Matomo tracking Server configuration (#4454)
- Matomo tracking of encrypted messages (#4415)
- Possibility to block requests on server maintenance (#4174)
- enable rust encryption for beta app

Bugfix 🐛:
- Reset matomo tracking id on logout (#4494)
- Fix wrong behaviour in server selection (#4643)

Translations 🗣  :
- English translations onboarding (#4377)

SDK API changes ⚠️:

Build 🧱:


Changes in BWI project 2.5.0 (2023-04-16)
===================================================

Upstream merge ✨:

- v1.10.5

Features ✨:

Improvements 🙌:
- Better new function banner (#2638)
- No confusing error messages when backend not available (#4174)

Bugfix 🐛:
- Fix for passphrase error (#4171)
- Fix more than one person in DM (#4209)
- Fix wrong number for room invites in chat overview (#4173)
- Fix dark font in dark mode bug (#4446)

Translations 🗣  :

SDK API changes ⚠️:

Build 🧱:
- Fix Open code compile problems (#4210)


Changes in BWI project 2.4.0 (2023-03-14)
===================================================

Upstream merge ✨:

- v1.10.2

Features ✨:
- Poll history

Improvements 🙌:
- Redesigned notes room in chat overview (#4211)
- New login flow for BwM (#4231)
- Added default server settings (#4218) 
- Add changeable permalink config (#3425) 
- Better permalink-header handling (#4146)
- Display user search results alphabetically (#3076)
- Text changes for downtime info (#4157)
- Changed text of reset passphrase button (#4167)
- Changed pin confirmation text (#4168)
- Color changes for ui elements (#4179)
- Improved forgot password text (#4161)

Bugfix 🐛:
- Fix for Cross-Signing error (#4303)
- Fix for status message getting cut off (#4197)
- Fix wrong number for room invites in chat overview (#4173)

Translations 🗣  :

SDK API changes ⚠️:

+Build 🧱:



Changes in BWI project 2.3.0 (2023-02-14)
===================================================

Upstream merge ✨:

- v1.9.15

Features ✨:
- New Design activated (#3790) 
- Netiquette (4016)
- Cross-Signing activated
- BuM only: Server selection via QR Code (#3895)

Improvements 🙌:
- Remove contextmenu for status messages (#4029)
- Downtime info on login view (#2821)
- Locationsahring without gps (#4235)
- Emoji naming maded the same for all plattforms (#4088)

Bugfix 🐛:

Translations 🗣  :

SDK API changes ⚠️:

+Build 🧱:
- Better versioning for nexus upload (#4074)

Changes in BWI project 2.2.1 (2022-01-20)
===================================================

 
Upstream merge ✨:
- v1.9.14

Features ✨:

Improvements 🙌:

Bugfix 🐛:
- Resetting Passphrase does not potentially lead to errors anymore (#4227)

Translations 🗣  :

SDK API changes ⚠️:

+Build 🧱:

Changes in BWI project 2.2.0 (2022-12-16)
===================================================

 
Upstream merge ✨:

- v1.9.14

Features ✨:

Improvements 🙌:
- Text and screen changes for crosssigning (#3418)

Bugfix 🐛:
- Poll not usable after logging out and back in. (#7070)

Translations 🗣  :

SDK API changes ⚠️:

+Build 🧱:

Changes in BWI project 2.1.0 (2022-12-14)
===================================================

 
Upstream merge ✨:

- v1.9.10

Features ✨:
- New Login Flow Setup Pin integration (#3732)
- New Login Flow Server Setup for BuM (#3707)
- Show data privacy infos before login BuM (#3616)
- Welcome Experience BuM (#3717)


Improvements 🙌:
- Contentscanner works with custom backend (#3602)
- Better dialog if backend not in whitelist (#3662)
- New BuM Beta Logo (#3954)
- Remove Element terms of service (#3791)
- BuM Animation for waiting screen (#3648)
- BwM use savety measures by default (#3729)

Bugfix 🐛:

Translations 🗣  :

SDK API changes ⚠️:

+Build 🧱:

Changes in BWI project 2.0.0 (2022-11-23)
===================================================

 
Upstream merge ✨:

- v1.9.10

Features ✨:
- Chat bubbles enabled
- Praise the birthday (#3646)

Improvements 🙌:
- New Logo (#3719)
- Refactored Buildsettings (#3626)
- Remove Element terms of service view (#3791)
- Remove room settings for DMs to be more similar to Android (#3639)
- being able to change pusher url and change it to push-local (#3637)

Bugfix 🐛:
- Disable sharing toolbar for pdfs (#3880)
- Fix going into app without pin log in some cases (#3891)

Translations 🗣  :

SDK API changes ⚠️:

+Build 🧱:


Changes in BWI project 1.26.0 (2022-10-21)
===================================================

 
Upstream merge ✨:

- v1.9.8

Features ✨:
- Zooming pictures with double tap (#3699)
- Enable animation handling with Lottie (for BuM) (#3620) 

Improvements 🙌:
- Rename closed polls to hidden polls (#3652)
- New Copyright URL (#3626)
- Better button contrast (#3688)
- Remove room settings for DMs to be more similar to Android (#3639)

Bugfix 🐛:
- Security fix (Missing decoration for events decrypted with untrusted Megolm sessions ([Security advisory])

Translations 🗣  :

SDK API changes ⚠️:

+Build 🧱:

Changes in BWI project 1.25.0 (2022-09-27)
===================================================

Changes in BWI project 1.25.0 (2022-09-27)
===================================================
 
Upstream merge ✨:

- v1.9.5

Features ✨:
- Mentions in room overview marked red
- Better handling of iOS 16 in UI

Improvements 🙌:
- Earlier deleting of unencrypted files from rooms (#3346)
- History for new features (#3546)
- Login protection for matrix id logins (#3416)
- Preparation of analytics activation (#3608)

Bugfix 🐛:
- Crashfix for login (Cherry pick from 1.9.6)
- Fix for secure contacts file  (#3344)
- Fix icloud exclusion of contacts (#3588)

Translations 🗣  :

SDK API changes ⚠️:

+Build 🧱:

Changes in BWI project 1.24.0 (2022-09-08)
===================================================
 
Upstream merge ✨:

- v1.8.24

Features ✨:
- Chat bubbles -> not activated because of cut messages bug
- Closed polls

Improvements 🙌:
- Onboarding for BuM

Bugfix 🐛:
- Crashfix for unusual uploaded files (#3495)
- Fix for clear text notifications (#3493)
- Fix for voice messages continuing in background

Translations 🗣  :

SDK API changes ⚠️:

+Build 🧱:
- Testflight build for MDM (#3382)
- Better separation of string tables (#3304)

Changes in BWI project 1.23.0 (2022-08-02)
===================================================
 
Upstream merge ✨:

- v1.8.18

Features ✨:

Improvements 🙌:
- Better ignored users screen
- preparation for old app version warning

Bugfix 🐛:

Translations 🗣  :

SDK API changes ⚠️:

+Build 🧱:
- Nexus upload for artifacts
- testflight builds for BundesMessenger and BundesMessenger-Beta

Changes in BWI project 1.22.0 (2022-06-25)
===================================================
 
Upstream merge ✨:

- v1.8.18

Features ✨:
- Personal state messages
- Detailed alert message when changing the password

Improvements 🙌:
- Minor text changes

Bugfix 🐛:
- Fixed a critical bug freezing the app after login

Translations 🗣  :

SDK API changes ⚠️:

+Build 🧱:

Changes in BWI project 1.19.0 (2022-04-20)
===================================================

Upstream merge ✨:

- v1.8.13

Features ✨:

Improvements 🙌:

Bugfix 🐛:

Translations 🗣 :

SDK API changes ⚠️:

Build 🧱:

Changes in BWI project 1.18.0 (2022-03-16)
===================================================

Upstream merge ✨:

- v1.8.3

Features ✨:
- Polling feature enabled

Improvements 🙌:

Bugfix 🐛:
- Display of private notes room

Translations 🗣:
- German texts for poll feature

SDK API changes ⚠️:

Build 🧱:

Changes in BWI project 1.17.0 (2022-02-14)
===================================================

Upstream merge ✨:

- v1.6.12h

Features ✨:
- Emoji history (#1601)
- Advertising new Features (#2469)


Improvements 🙌:
- Beta app has flexible server backends (#2699)

Bugfix 🐛:
- DMs don't have roles & rights any more (#1919)

Translations 🗣:

SDK API changes ⚠️:

Build 🧱:

Changes in BWI project 1.16.0 (2022-01-12)
===================================================

Upstream merge ✨:

- v1.6.11h

Features ✨:
- Downtime warnings (#2597)
- Forward messages
- Mention users

Improvements 🙌:
- Removed hardened usage


Bugfix 🐛:
- Reset passphrase dialog flow not cancelable
- Fixed a crash when trying to access the personal notes room

Translations 🗣:

SDK API changes ⚠️:

Build 🧱:

Changes in BWI project 1.14.0 (2021-11-04)
===================================================

Upstream merge ✨:

- v1.6.5h

Features ✨:
- Happy Birthday (#2538)
- Spaces in a deactivated state

Improvements 🙌:
- Integration of Matomo Tracking for Beta App (#2519, #2525)
- Register button in login screen (#2536)

Bugfix 🐛:
- Some crash fixes regarding Realm and iOS 15
- Crosssigning in Security settings deactivated (#2456)
- Translated and cleared error messages on user invite (#2470)

Translations 🗣:

SDK API changes ⚠️:

Build 🧱:

Changes in BWI project 1.13.0 (2021-10-06)
===================================================

Upstream merge ✨:

- v1.5.4h

Features ✨:
- Voice Messages activated
- Rules & Rights (#2310)
- Reset room rights (#2312)
- New Notification setting menu

Improvements 🙌:
- Camera: The quality of video when filming in-app is significantly higher.
- Mark iOS 11 as deprecated and show different version check alerts.
- Moved converted voice messages to their own folder. Cleaning up all temporary files on on reload and logout. 
- Popping the user back to the home screen after leaving a room. 

Bugfix 🐛:
- The last admin of a room is not able to unpower himself (#2101)
- Fixed home view being clipped when search is active.

Translations 🗣:

SDK API changes ⚠️:

Build 🧱:

Changes in BWI project 1.12.0 (2021-09-15)
===================================================

Upstream merge ✨:

- v1.5.1h

Features ✨:
- Voice Messages
- Personal Notes room (#2223)
- Feature Announce Banner (#2214)
- Show/Hide Timeline events (#1552)


Improvements 🙌:

Bugfix 🐛:
- Corrected behaviour when faceID/toucgID is available but deactivated for the app (#2247)

Translations 🗣:

SDK API changes ⚠️:

Build 🧱:

Changes in BWI project 1.11.0 (2021-08-12)
===================================================

Upstream merge ✨:

- v1.4.7h

Features ✨:
- Show/Hide password while typing (#1967)
- Password forgotten? Infotext for what to do (#1818)


Improvements 🙌:
- Better Icons for DMs / Rooms (#2069, #2071)
- Show weekdays in timeline (#2080)
- Show Element version in about settings (#2098)
- Better asynchronous decryption (NV #3777)

Bugfix 🐛:

Translations 🗣:

SDK API changes ⚠️:

Build 🧱:


Other changes:

Changes in BWI project 1.10.0 (2021-07-14)
===================================================

Upstream merge ✨:

- v1.4.1h

Features ✨:
- Avatar can by restored to standard avatar (#1905)

Improvements 🙌:
- Crypto: Do not decrypt synchronously. It asynchronously happens upstream now

Bugfix 🐛:
- SettingsViewController: Fix crash when changing the app language
- UserSessionsService: Fix room lists lost after a reset cache
- From MatrixSDK 0.19.1: App can fail to perform an incremental sync on reopening (#4417) suggested by Giom

Translations 🗣:
SDK API changes ⚠️:
Build 🧱:


Other changes:

Changes in BWI project 1.9.0 (2021-06-17)
===================================================

Upstream merge ✨:

- v1.3.9h

Features ✨:

Improvements 🙌:

Bugfix 🐛:
Translations 🗣:
SDK API changes ⚠️:
Build 🧱:

- Added automated UI testing (#1700)
- Disable Logging (#1787)

Other changes:


Changes in BWI project 1.8.0 (2021-05-20)
===================================================

Upstream merge ✨:

- v1.3.5h

Features ✨:

- Redesigned Composer (writing mode and action mode)
- Typing notifications

Improvements 🙌:

- Disable Integrations (#1832)
- Disable stickers for composer (#1832)

Bugfix 🐛:
Translations 🗣:
SDK API changes ⚠️:
Build 🧱:
Other changes:

- Rest Time UI and Logic (apart from Notifications) (#1800, #1803, #1804)


Changes in BWI project 1.7.0 (2021-04-19)
===================================================

Upstream merge ✨:

v1.3.1h

Features ✨:

- Rooms can be changed from privat to public and back by the room admin (#912)

Improvements 🙌:

- Deactivate Cross signing for web client release (#1742)
- Allow change display name depending on server config (#1668)
- Disable voice call settings (#1744)
- Disable NSFW setting (#1744)

Bugfix 🐛:

- Clearify Face/Touch ID error message (#1659)

Translations 🗣:

- Change "Passphrase" to "Verschlüsselungskennwort"
- Add translations for New Room details (#1744)


SDK API changes ⚠️:
Build 🧱:

Create Beta build config (#1732)

Other changes:

Allow test server login in Beta build (#1732)
Enable Rageshake in Beta build (#1623)

Changes in BWI project 1.6.0 (2021-03-23)
===================================================

Upstream merge ✨:
 - v1.2.6h

Features ✨:
 - Rooms can be changed from privat to public and back by the room admin (#912)

Improvements 🙌:
- Rooms may not be left by the only admin (#1629)
- Accepting invitations takes only one click (#749)

Bugfix 🐛:


Translations 🗣:
 - German translation for integrity check (#1689)

SDK API changes ⚠️:
 - 

Build 🧱:
 -

Other changes:
 -

Changes in BWI project 1.5.1 (2021-02-15)
===================================================

Upstream merge ✨:
 - v1.1.2h
 - v1.1.5h

Features ✨:
 -

Improvements 🙌:
 - Added timestamp to all chat messages.
 - Added Face ID support
 - Changed room details (members) icon backround in olive green
 - Hide Pin change description text and localized some strings
 - Removed room encryption alert view entry
 - Javascipt support disabled
 - Removed invite friends to BwMessenger
 - Removed "Direktnachricht" from support.html 

Bugfix 🐛:
 - Login screen icon centered.

Translations 🗣:
 -

SDK API changes ⚠️:
 - 

Build 🧱:
 -

Other changes:
 -


Changes in BWI project 1.0.21 (2020-11-25)
===================================================

Upstream merge ✨:
 -

Features ✨:
 -

Improvements 🙌:
 - Removed files and messages from search tab
 - No popup for selecting the file size appears anymore
 - Increased pin storage security (MESSENGER-834)

Bugfix 🐛:
 - Pin bypass (MESSENGER-936)

Translations 🗣:
 -

SDK API changes ⚠️:
 - 

Build 🧱:
 -

Other changes:
 -

 Changes in BWI project 1.0.18 (2020-11-09)
 ===================================================

 Upstream merge ✨:
  -

 Features ✨:
  -

 Improvements 🙌:
  -

 Bugfix 🐛:
  - Hotfix to disable matomo tracking

 Translations 🗣:
  -

 SDK API changes ⚠️:
  - 

 Build 🧱:
  -

 Other changes:
  - removed other element specific urls in project and buildsettings as they are opened on startup

Changes in BWI project 1.0.16 (2020-10-30)
 ===================================================

 Upstream merge ✨:
  -

 Features ✨:
  -

 Improvements 🙌:
  - Improved login screen
  - Pin gracetime 3min
  - Logout and delete all files if jailbreak detected

 Bugfix 🐛:
  - Fix pincode bypass on fast switching (#59).

 Translations 🗣:
  - Push message notification text in german

 SDK API changes ⚠️:
  - 

 Build 🧱:
  -

 Other changes:
  -

Changes in BWI project 1.0.15 (2020-10-20)
===================================================

Upstream merge ✨:
-

Features ✨:
-

Improvements 🙌:
-

Bugfix 🐛:
- 

Translations 🗣:
-

SDK API changes ⚠️:
- 

Build 🧱:
-

Other changes:
- Removed text below mobile icon to be conform for Apple Review.


Changes in BWI project 1.0.13.7 (2020-10-15)
===================================================

Upstream merge ✨:
 -

Features ✨:
 -

Improvements 🙌:
 -

Bugfix 🐛:
 - Removed audio background mode from info.plist (app store review issue)

Translations 🗣:
 -

SDK API changes ⚠️:
 - 

Build 🧱:
 -

Other changes:
 -
 
 
 Changes in BWI project 1.0.13.6 (2020-10-12)
===================================================

Upstream merge ✨:
 -

Features ✨:
 -

Improvements 🙌:
 - New splash screen with camou background and BWI logo

Bugfix 🐛:
 - Face ID now completely deactivated

Translations 🗣:
 -

SDK API changes ⚠️:
 - 

Build 🧱:
 -

Other changes:
 -



Changes in BWI project 1.0.13.2 (2020-10-09)
===================================================

Upstream merge ✨:
 -

Features ✨:
 - 

Improvements 🙌:
 - Hide security info
 - Support setting
 - Removed Terms and Conditions setting
 - Added Support setting
 - Reset passphrase workflow fixed

Bugfix 🐛:
 -

Translations 🗣:
 - Secure recovery
 - DM leave
 - Reset passphrase


SDK API changes ⚠️:
 - 

Build 🧱:
 -

Other changes:
 -




Changes in BWI project 1.0.13.0 (2020-10-01)
===================================================

Upstream merge ✨:
 - Hardened 1.0.13

Features ✨:
 - New appicon design
 - New Splash screen design
 - Removed email field under settings

Improvements 🙌:
 - Themes contains olive green tint color for button and text elements.

Bugfix 🐛:
 -

Translations 🗣:
 -

SDK API changes ⚠️:
 - 

Build 🧱:
 -

Other changes:
 -




Changes in BWI project 1.0.11.0 (2020-09-18)
===================================================

Features ✨:
 - Update to Element 1.0.11

Improvements 🙌:
 - Changed hint text in search field for people
 - Hide trusted text in attachments like in images 
 - Security header in bold
 - Removed Advanced section under Security

Bugfix 🐛:
 - GIFs are animated again after uploading them 

Translations 🗣:
 - 

SDK API changes ⚠️:
 - 

Build 🧱:
 -

Other changes:
 - Disable Xcode Warning from Third Party Vendors
 - Upgrade minor Xcode project changes to v11.x 


Changes in BWI project 1.0.4.11 (2020-09-13)
===================================================

Features ✨:
 -

Improvements 🙌:
 - Settings header in bold
 - No context menu when file is infected
 - Hide presence status of user
 - Hide email text from settings
 - Compress every image to max. 1600x1200 px
 - Hide "Direct" and "Low Priority" Button from swipe menu on room, "Leave room" is red now
 - Hide AV status text if it is "Trusted"

Bugfix 🐛:
 - Fixed problem with removing Face ID after first time login.

Translations 🗣:
 - German Translations for Jailbreak info
 - AV texts
 - Improved password change texts

SDK API changes ⚠️:
 - 

Build 🧱:
 -

Other changes:
 - Login text username only (no email)

 



Changes in BWI project 1.0.4.10 (2020-09-04)
===================================================

Features ✨:
 -

Improvements 🙌:
 - string change "Personen" to "Direktnachrichten 
 - Moved sign out button from top to bottom of settings.
 - Removed choose language feature from settings.
 - Hide Home and Communnities in Bottom Bar, DM is default
 - Disabled iCloud link
 - Disabled Siri
 - Password change dialog with rules text
 - BwMessenger splash screen inspired from Android version
 - Display name readonly


Bugfix 🐛:
 -

Translations 🗣:
 - Pin Code dialog

SDK API changes ⚠️:
 - 

Build 🧱:
 -

Other changes:
 - Disabled Face ID





=======================================================
+        TEMPLATE WHEN PREPARING A NEW RELEASE        +
=======================================================


Changes in BWI project 1.X.X (2020-XX-XX)
===================================================

Upstream merge ✨:
 -

Features ✨:
 -

Improvements 🙌:
 -

Bugfix 🐛:
 -

Translations 🗣:
 -

SDK API changes ⚠️:
 - 

Build 🧱:
 -

Other changes:
 -
