<div align="center">
  <img src="https://gitlab.opencode.de/bwi/bundesmessenger/info/-/raw/main/images/logo.png?inline=false" alt="BundesMessenger Logo" width="128" height="128">
</div>

<div align="center">
  <h2 align="center">BundesMessenger-iOS</h2>
</div>

----

Wir freuen uns, dass Du Dich für den BundesMessenger interessierst. 

Fangen wir mit dem Wichtigsten an. Hier findest Du die offizielle App für iOS:

<p align="center">  
    <a href="https://apps.apple.com/de/app/bundesmessenger/id1616866351" style="display: inline-block;">
    <img src="https://toolbox.marketingtools.apple.com/api/v2/badges/download-on-the-app-store/black/de-de?releaseDate=1671408000" alt="Im App Store laden" style="width: 245px; height: 82px; vertical-align: middle; object-fit: contain;" />
    </a>
</p>

Wenn Dir die App gefällt, lass gerne eine positive Bewertung da.

BundesMessenger-iOS ist ein iOS Matrix Client basierend auf [Element iOS](https://github.com/element-hq/element-ios)
von [Element Software](https://element.io/).

Allgemeine Infos zum Thema BundesMessenger und was dahinter steckt findet ihr [hier](https://gitlab.opencode.de/bwi/bundesmessenger/info).


## Grundsätzliches

BundesMessenger ist ein Artefakt, welches durch die BWI im Herstellungsprozess für den BwMessenger alle 4 Wochen hergestellt wird.

Hierzu durchlaufen wir folgenden Prozess:

<p align="center">  
  <img src="docs/img/element-bum-bwm.png" width="600">
</p>

Aufgrund der starken Bindung an *Element iOS* ist es aktuell nicht vorgesehen, dass ihr über das Repository Einfluss auf den BundesMessenger nehmen könnt. Wenn ihr euch beteiligen wollt, müsst ihr eure Contribution direkt in [Element iOS](https://github.com/element-hq/element-ios) einfließen lassen. Diese werden in der Regel im Anschluss in den BundesMessenger übernommen. 

Wenn ihr euch unsicher seid, haltet hierzu gerne [Rücksprache mit uns](#kontakt).

> Übrigens: Diesen Prozess leben wir selber auch für bestimmte Features.

**Warum veröffentlichen wir hier den Quellcode?**

Wir möchten 100% transparent sein und euch die Möglichkeit geben den Source Code einzusehen.

Wir freuen uns über euer Feedback. Öffnet gerne neue Issues für eure Fragen oder Probleme hier im GitLab.

## Repo

https://gitlab.opencode.de/bwi/bundesmessenger/clients/bundesmessenger-ios

## Fehler und Verbesserungsvorschläge

https://gitlab.opencode.de/bwi/bundesmessenger/clients/bundesmessenger-ios/-/issues


## Abhängigkeiten

[Element iOS](https://github.com/element-hq/element-iOS)

[BundesMessenger iOS Matrix SDK](https://gitlab.opencode.de/bwi/bundesmessenger/clients/bundesmessenger-ios-matrix-sdk)

## Für Entwickler

Weiterführende Dokumentation zum Projekt, dem Betrieb und der Architektur findest Du [hier](https://gitlab.opencode.de/bwi/bundesmessenger/info).

Nachdem das Projekt ausgecheckt ist, reichen folgende Anweisungen zum Compilat und zum Start der App

```
$ bundle install                                        # Installiert Abhängigkeiten falls notwendig
$ xcodegen                                              # Erzeugt das xcodeproj mit allen Quellcodedateien
$ pod install                                           # Erzeugt den xcworkspace mit allen Projektabhängigkeiten
$ Öffne Riot.xcworkspace                                # Öffnet Xcode
$ Stelle das target auf BuM-Beta oder BundesMessenger
```

## Updates

In der Regel werden wir hier alle 4 Wochen ein Update veröffentlichen. In etwa zeitgleich erfolgt das Update der Apps in den App Stores.

Sollte es zu Sicherheitsvorfällen kommen, stellen wir hier und im App Store auch kurzfristig Hotfixes zur Verfügung.

## Forks

Du hast die Möglichkeit einen Fork von diesem Projekt zu machen. Bitte beachte, dass die [kontinuierliche Pflege](#updates) sehr viel Aufwand und Entwicklungsressourcen benötigt. 

Wir nehmen euch diese Arbeit ab, da wir dies für den BwMessenger ohnehin machen müssen. Daher empfehlen wir  euch für den produktiven Einsatz in der öffentlichen Verwaltung die gepflegten BundesMessenger Apps aus den App Stores zu verwenden. Ziel ist es nicht euch auszuschließen, sondern eine stetige hohe Qualität und Sicherheit zur Verfügung zu stellen.

| :warning: Wichtig: Dieses Repository ist ein Mirror eines internen BWI Repos. Die aktive Entwicklung findet nicht in diesem Repository statt. Es müssen BWI spezifische Build Anteile entfernt werden (z.B. GitLab CI Informationen). Um dies zu gewährleisten wird die Git History beim Mirroring neu geschrieben. Wenn ihr einen Fork von diesem Repository erstellt, müsst ihr mit erhöhten Aufwänden bei Updates rechnen. |
| --- |

## Nutzung

Um die BundesMessenger App aus dem Play Store nutzen zu können, müsst ihr euer Backend registrieren lassen. Weitere Infos dazu [hier](https://messenger.bwi.de/ich-will-bum).

Wenn Du Dein Backend noch nicht erfolgreich aufgebaut hast, aber trotzdem schon einen Blick in die App werfen möchtest, bieten wir Dir eine Demo Umgebung an. Bitte kontaktiere uns per [Email](mailto:bundesmessenger@bwi.de&subject=Ich%20will%20testen).

## Rechtliches

**BundesMessenger** ist sowohl als Logo- und als Textmarke durch die [BWI GmbH](https://www.bwi.de) geschützt.

Eine Nutzung derselben ist nur mit Freigabe durch die BWI möglich.

| :warning: Wichtig: Falls ihr einen Fork erstellt, müssen alle Verweise (Texte & Bilder) auf BundesMessenger und die BWI zwingend entfernt werden! Das betrifft nicht die Git History, sondern die Releases der jeweiligen neuen Apps. |
| --- |

Die Lizenz des BundesMessenger iOS ist die [AGPLv3](./LICENSE).

### Copyright

- [BWI GmbH](https://messenger.bwi.de/copyright)
- [Element](https://element.io/copyright)

Copyright (c) 2014-2017 OpenMarket Ltd  
Copyright (c) 2017 Vector Creations Ltd  
Copyright (c) 2017-2024 New Vector Ltd

SPDX-License-Identifier: AGPL-3.0-only. Please see [LICENSE](LICENSE) in the repository root for full details.

## Kontakt

Für den Austausch zum BundesMessenger haben wir einen [Matrix Raum](https://matrix.to/#/#opencodebum:matrix.org) erstellt.

<div align="center">
  <img src="https://gitlab.opencode.de/bwi/bundesmessenger/info/-/raw/main/images/qr_matrix_room.png" alt="QR Code Matrix" width="128" height="128"/>
</div>

Kein Matrix Client zur Hand, dann auch gerne über unser [Email Postfach](mailto:bundesmessenger@bwi.de).

Wir freuen uns auf den Austausch.
