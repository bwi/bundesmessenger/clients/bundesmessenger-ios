//
/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#import "RoomViewController+ContentScanner.h"
#import "GeneratedInterface-Swift.h"

@implementation RoomViewController (AntivirusScan)

- (void)registerContentScannerCells
{
    if (@available(iOS 14.0, *)) {
        [self.bubblesTableView registerClass:ContentScannerStatusCell.class forCellReuseIdentifier:ContentScannerStatusCell.defaultReuseIdentifier];
        [self.bubblesTableView registerClass:ContentScannerThumbnailCell.class forCellReuseIdentifier:ContentScannerThumbnailCell.defaultReuseIdentifier];
    }
}

- (RoomTimelineCellIdentifier)contentScannerClassForBubbleCellData:(RoomBubbleCellData*)roomBubbleCellData
{
    if (roomBubbleCellData.attachment.type == MXKAttachmentTypeVoiceMessage)
    {
        MXEventScan *eventScan = [roomBubbleCellData eventScanForFirstComponent];
        
        if (eventScan && eventScan.antivirusScanStatus == MXAntivirusScanStatusTrusted)
        {
            // let the FOSS decide what to do
            return ContentScannerIdentifierUndecided;
        }
    }
    
    if (@available(iOS 14.0, *)) {
        if (!roomBubbleCellData.isAttachmentWithThumbnail) {
            return ContentScannerIdentiferStatus;
        }
        
        MXEventScan *eventScan = [roomBubbleCellData eventScanForFirstComponent];
        
        // here too, no changes needed to FOSS if scanstatus is trusted
        if (eventScan && eventScan.antivirusScanStatus != MXAntivirusScanStatusTrusted)
        {
            
            return ContentScannerIdentifierThumbnail;
        }
    }
    
    return ContentScannerIdentifierUndecided;

}

@end
