//
/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import UIKit

@objcMembers
final class ContentScannerStatusCreator {
    
    func viewData(from cellData: MXKRoomBubbleCellData) -> ContentScannerStatusViewData? {
        guard cellData.attachment != nil,
            let component = cellData.getFirstBubbleComponentWithDisplay(),
            let scanStatus = component.eventScan?.antivirusScanStatus,
            let attachment = self.attachment(from: component) else {
                return nil
        }
        return ContentScannerStatusViewData(attachment: attachment, scanStatus: scanStatus)
    }
    
    private func attachment(from component: MXKRoomBubbleComponent) -> String? {
        return component.attributedTextMessage?.string ?? component.textMessage
    }
}
