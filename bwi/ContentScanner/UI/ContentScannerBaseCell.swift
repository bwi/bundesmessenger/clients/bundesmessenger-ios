//
/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation

import UIKit

@objcMembers
class ContentScannerBaseCell: SizableBaseRoomCell, RoomCellReadReceiptsDisplayable, RoomCellReactionsDisplayable {
        
    var innerContent: ContentScannerContentDelegate?
    var scanStatusViewData: ContentScannerStatusViewData?
    
    private let scanStatusViewDataBuilder: ContentScannerStatusCreator
        
    // MARK: - Setup

    required init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        self.scanStatusViewDataBuilder = ContentScannerStatusCreator()
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = .clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func render(_ cellData: MXKCellData!) {
        self.updateCellStatus(cellData)
        super.render(cellData)
        
        guard let innerContent = self.innerContent,
              let roomBubbleCellData = cellData as? RoomBubbleCellData,
              let viewData = self.scanStatusViewDataBuilder.viewData(from: roomBubbleCellData) else {
               return
        }
        innerContent.render(with: viewData)
        self.scanStatusViewData = viewData
    }
    
    override func update(theme: Theme) {
        super.update(theme: theme)
        self.innerContent?.theme = theme
    }
    
    func updateCellStatus(_ cellData: MXKCellData!) {
        guard let roomBubbleCellData = cellData as? RoomBubbleCellData else {
            return
        }
        self.roomCellContentView?.showEncryptionStatus = roomBubbleCellData.containsBubbleComponentWithEncryptionBadge
        self.roomCellContentView?.showSenderInfo = !roomBubbleCellData.shouldHideSenderInformation
        self.roomCellContentView?.showPaginationTitle = roomBubbleCellData.isPaginationFirstBubble
    }
}
