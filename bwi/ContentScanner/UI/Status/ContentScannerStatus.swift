//
/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import UIKit

@available(iOS 14.0, *)
class ContentScannerStatus: ObservableObject, ContentScannerContentDelegate {
    
    var theme: Theme!
    
    @Published var scanStatus = ""
    @Published var fileName = ""
    @Published var statusImagePath = ""
    @Published var progressing = false
    
    @Published var scanStatusVisibility = false
    
    @Published var backgroundColor: UIColor = .white
    @Published var tintColor: UIColor = .black
    @Published var statusColor: UIColor = .black
    
    func render(with viewData: ContentScannerStatusViewData) {
 
        theme = ThemeService.shared().theme
        
        let scanStatus = viewData.scanStatus
      
        switch scanStatus {
        case .inProgress:
            statusImagePath = ""
        case .infected:
            statusImagePath = Asset.Images.fileScanInfected.name
        case .unknown:
            // bwi #5793 use new icon for unknown
            statusImagePath = Asset.Images.mediaFileUnavailable.name
        case .trusted:
            statusImagePath = Asset.Images.fileAttachmentIcon.name
        @unknown default:
            statusImagePath = Asset.Images.fileAttachmentIcon.name
        }
        
        self.fileName = viewData.attachment
        self.scanStatus = self.scanStatusMessage(for: viewData.scanStatus)
        
        self.progressing = statusImagePath.isEmpty
        
        self.scanStatusVisibility = (scanStatus == .infected) ? false : true
        
        self.backgroundColor = (scanStatus == .infected) ? self.theme.warningColor : self.theme.baseColor
        self.tintColor = (scanStatus != .infected) ? self.theme.textSecondaryColor : self.theme.warningColor
        self.statusColor = (scanStatus != .infected) ? self.theme.textPrimaryColor : self.theme.textSecondaryColor
    }
}
