//
/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import SwiftUI

@available(iOS 14.0, *)
struct ContentScannerStatusContentView: View {
    @ObservedObject var scanStatus: ContentScannerStatus
    
    var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            HStack {
                ZStack {
                    if scanStatus.progressing {
                        ProgressView()
                    } else {
                        Image(scanStatus.statusImagePath)
                            .foregroundColor(.white)
                    }
                }
                .background(Color(scanStatus.backgroundColor))
                .cornerRadius(5)
                
                Text(scanStatus.fileName)
                    .font(.system(size: 15))
                    .foregroundColor(Color(scanStatus.statusColor))
               
            }
            .background(Color.clear)
            Text(scanStatus.scanStatus)
                .font(.system(size: 12))
                .foregroundColor(Color(scanStatus.tintColor))
        }
        .frame(
            maxWidth: .infinity,
            maxHeight: .infinity,
            alignment: .topLeading
        )
        .background(Color.clear)
    }
}

@available(iOS 14.0, *)
struct ContentScannerScanStatusContentView_Previews: PreviewProvider {
    
    static var previews: some View {
        let scanStatus = ContentScannerStatus()
        ContentScannerStatusContentView(scanStatus:scanStatus)
    }
}
