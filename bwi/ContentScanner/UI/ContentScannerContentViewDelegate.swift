//
/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import UIKit

protocol ContentScannerContentDelegate  {
    
    var theme: Theme! { get set }
    
    func render(with viewData: ContentScannerStatusViewData)
}

extension ContentScannerContentDelegate {
    func scanStatusMessage(for scanStatus: MXAntivirusScanStatus) -> String {
        let message: String
        
        switch scanStatus {
        case .trusted:
            message = BWIL10n.contentScanStatusTrusted
        case .inProgress:
            message = BWIL10n.contentScanStatusInProgress
        case .unknown:
            message = BWIL10n.contentScanStatusFailed
        case .infected:
            message = BWIL10n.contentScanStatusInfected
        @unknown default:
            fatalError()
        }
        return message
    }
    
    func scanStatusColor(for scanStatus: MXAntivirusScanStatus) -> UIColor {
        ((scanStatus == .infected) || (scanStatus == .unknown)) ? self.theme.textPrimaryColor : self.theme.textSecondaryColor
    }
}

