//
/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import SwiftUI

@available(iOS 14.0, *)
struct ContentScannerThumbnailContentView: View {
    @ObservedObject var scanStatus: ContentScannerThumbnail
    
    var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            HStack {
                ZStack {
                    Rectangle()
                        .fill(Color(scanStatus.backgroundColor))
                    if scanStatus.progressing {
                        ProgressView()
                    } else {
                        Image(scanStatus.imagePath)
                    }
                }
                .frame(
                    minHeight: 150
                )
            }
            ThumbnailAttributedStatus(status: scanStatus.status,
                                      filename: scanStatus.fileName,
                                      statusColor: Color(scanStatus.statusColor),
                                      filenameColor: Color(scanStatus.filenameColor))
        }
        .frame(
            maxWidth: .infinity,
            maxHeight: .infinity,
            alignment: .topLeading
        )
        .background(Color.clear)
    }
}


@available(iOS 14.0, *)
struct ThumbnailAttributedStatus: View {
    let status: String
    let filename: String
    let statusColor: Color
    let filenameColor: Color
    
    var body: some View {

        HStack {
            if BWIBuildSettings.shared.showContentScannerStatusWithFilename {
                Text(verbatim: filename)
                    .foregroundColor(filenameColor)
                    .font(.system(size: 15))
            }
            Text(verbatim: status)
                .foregroundColor(statusColor)
                .font(.system(size: 15))
        }
    }
}


@available(iOS 14.0, *)
struct ContentScannerThumbnailContentView_Previews: PreviewProvider {
    
    static var previews: some View {
        let scanStatus = ContentScannerThumbnail()
        ContentScannerThumbnailContentView(scanStatus:scanStatus)
    }
}
