//
/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import UIKit
import SwiftUI

@available(iOS 14.0, *)
@objcMembers
class ContentScannerThumbnailCell: ContentScannerBaseCell {
        
    required init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupInnerContentView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupInnerContentView() {
        let innerContent = ContentScannerThumbnail()

        let controller = UIHostingController(rootView:ContentScannerThumbnailContentView(scanStatus: innerContent))
        
        innerContent.theme = ThemeService.shared().theme
        
        controller.view.backgroundColor = .clear

        self.roomCellContentView?.innerContentView.vc_addSubViewMatchingParentSafeArea(controller.view)
        self.innerContent = innerContent
    }
}
