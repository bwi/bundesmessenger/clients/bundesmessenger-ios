//
/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import UIKit

@available(iOS 14.0, *)
class ContentScannerThumbnail: ObservableObject, ContentScannerContentDelegate {
    
    var theme: Theme!
    
    @Published var fileName = ""
    @Published var status = ""
    @Published var imagePath = ""
    
    @Published var progressing = false
    
    @Published var backgroundColor: UIColor = .white
    @Published var tintColor: UIColor = .black
    @Published var filenameColor: UIColor = .black
    @Published var statusColor: UIColor = .black
   
    
    func render(with viewData: ContentScannerStatusViewData) {
               
        let scanStatus = viewData.scanStatus
        
        self.fileName = viewData.attachment
        
        switch viewData.scanStatus {
        case .infected:
            imagePath = Asset.Images.fileScanInfected.name
        case .unknown:
            // bwi #5793 use new text for unknown and remove filename for size
            imagePath = Asset.Images.mediaFileUnavailable.name
            self.fileName = ""
        default:
            imagePath = ""
        }
        
        self.status = self.scanStatusMessage(for: viewData.scanStatus)
        
        self.progressing = imagePath.isEmpty
                
        self.backgroundColor = (scanStatus == .infected) ? self.theme.textPrimaryColor : self.theme.baseColor
        self.tintColor = (scanStatus != .infected) ? self.theme.backgroundColor : self.theme.textSecondaryColor
        self.filenameColor = (scanStatus != .infected) ? self.theme.textPrimaryColor : self.theme.textSecondaryColor
        self.statusColor = self.theme.textSecondaryColor
    }
}
