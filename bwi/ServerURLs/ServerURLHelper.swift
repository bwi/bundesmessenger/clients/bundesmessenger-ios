//
/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation

@objcMembers class ServerURLHelper : NSObject {
    static let shared = ServerURLHelper()
    
    private let settingsKey = "serverSettings"
    private let serverUrlKey = "serverURL"
    private let pusherUrlKey = "pusherURL"
    private let flavorUrlKey = "flavor"
    private let nameKey = "name"
    private let permalinkKey = "permalink"
    private let analyticsKey = "analytics"
    
    var serverSettings = Array<ServerSetting>()
    
    var selectedIndex: Int {
        get {
            if UserDefaults.standard.object(forKey: "Bwi_Server_Url_Helper_Index") != nil {
                return UserDefaults.standard.integer(forKey: "Bwi_Server_Url_Helper_Index")
            } else {
                return -1
            }
        }
        set(newSelectedIndex) {
            UserDefaults.standard.set(newSelectedIndex, forKey: "Bwi_Server_Url_Helper_Index")
        }
    }
    
    private func loadURLs() {
        if let path = Bundle.main.path(forResource: "serverurls", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? Dictionary<String, AnyObject>, let urls = jsonResult[settingsKey] as? [Any] {
    
                    for urlSet in urls  {
                        if let dict = urlSet as? Dictionary<String,String> {
                            if let server = dict[serverUrlKey],
                                let flavor = dict[flavorUrlKey],
                                let name = dict[nameKey],
                                let permalink = dict[permalinkKey] {
                                serverSettings.append(ServerSetting(name:name, serverUrl: server, pusherUrl: "", flavor: flavor, permalink:permalink, analytics: ""))
                            }
                        }
                    }
                }
            } catch {
                 // handle error
            }
        }
    }
    
    override init() {
        super.init()
        self.loadURLs()
    }
    
    func indexOf(_ serverUrl: String) -> Int {
        for setting in self.serverSettings {
            if setting.serverUrl == serverUrl {
                if let index = self.serverSettings.firstIndex(of: setting) {
                    return index
                }
            }
        }
        return NSNotFound
    }
    
    func serverUrl()  -> String? {
        if serverSettings.indices.contains(selectedIndex)  {
            return serverSettings[selectedIndex].serverUrl
        } else {
            return nil
        }
    }
    
    
    func pusherUrl()  -> String? {
        if serverSettings.indices.contains(selectedIndex)  {
            return serverSettings[selectedIndex].pusherUrl
        } else {
            return nil
        }
    }
    
    func flavor() -> String? {
        if serverSettings.indices.contains(selectedIndex)  {
            return serverSettings[selectedIndex].flavor
        } else {
            return nil
        }
    }
    
    func name() -> String? {
        if serverSettings.indices.contains(selectedIndex)  {
            return serverSettings[selectedIndex].name
        } else {
            return nil
        }
    }
    
    func permalink() -> String? {
        if serverSettings.indices.contains(selectedIndex)  {
            return serverSettings[selectedIndex].permalink
        } else {
            return nil
        }
    }
    
    func httpsPermalink() -> String? {
        if serverSettings.indices.contains(selectedIndex)  {
            return "https://" + serverSettings[selectedIndex].permalink
        } else {
            return nil
        }
    }
    
    func analytics() -> String? {
        if serverSettings.indices.contains(selectedIndex) {
            return serverSettings[selectedIndex].analytics
        } else {
            return nil
        }
    }
}
