//
/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation

@objcMembers class IntegrityCheck : NSObject, SecurityCheck {
    
    func isBreached() -> Bool {
        return (BWIBuildSettings.shared.forcedPinProtection && !PinCodePreferences.shared.isPinSet)
    }
    
    func breachAlert() -> UIAlertController {
        self.dismissAnyAlertControllerIfPresent()
        
        let alert = UIAlertController(title: BWIL10n.integrityAlertTitle, message: BWIL10n.integrityAlertMessage, preferredStyle: .alert)
            
        alert.addAction(UIAlertAction(title: VectorL10n.close, style: .destructive, handler: { _ in
            exit(0)
        }))
        
        alert.addAction(UIAlertAction(title: VectorL10n.settingsSignOut, style: .default, handler: { _ in
            AppDelegate.theDelegate().logout(withConfirmation: false) { (loggedOut) in
                if (!loggedOut) {
                    exit(0)
                }
            }
        }))
        
        return alert
    }
}
