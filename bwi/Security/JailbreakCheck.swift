//
/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation
import DTTJailbreakDetection

@objcMembers class JailbreakCheck : NSObject, SecurityCheck {
    
    func isBreached() -> Bool {
        if BWIBuildSettings.shared.forcedNoneJailbroken {
            return DTTJailbreakDetection.isJailbroken()
        }
        return false
    }
    
    func breachAlert() -> UIAlertController {
        self.dismissAnyAlertControllerIfPresent()
        
        self.logoutAndCleanUp()
        
        let alert = UIAlertController(title: BWIL10n.jailbreakAlertTitle, message: BWIL10n.jailbreakAlertMessage(AppInfo.current.displayName), preferredStyle: .alert)
            
        alert.addAction(UIAlertAction(title: VectorL10n.close, style: .destructive, handler: { _ in
            exit(0)
        }))
        
        return alert
    }
    
    private func logoutAndCleanUp() {
        /*
         * Logout when the user is still logged in.
         */
        let appDelegate = AppDelegate.theDelegate()
        
        appDelegate.logoutSendingRequestServer(true, completion: { (finished: Bool) -> Void in
                    
            if (finished) {
                // PART I - Remove all caches.
                //URLCache.shared.removeAllCachedResponses()

                // PART II - Remove the whole library folder.
                let pathLibraries = NSSearchPathForDirectoriesInDomains(.allLibrariesDirectory, .userDomainMask, true)
                let mainLibraryPath = pathLibraries[0]

                do {
                    let fileManager = FileManager .default
                    let fileArray = try fileManager .contentsOfDirectory(atPath: mainLibraryPath)

                    for filename in fileArray {
                        let writePath = NSURL(fileURLWithPath: mainLibraryPath).appendingPathComponent(filename)
                        try fileManager.removeItem(at: writePath!)
                    }
                }
                catch {
                }

                // PART III - Remove the whole documnent folder.
                let pathDocument = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
                let mainDocumentPath = pathDocument[0]

                do {
                    let fileManager = FileManager .default
                    let fileArray = try fileManager .contentsOfDirectory(atPath: mainDocumentPath)

                    for filename in fileArray {
                        let writePath = NSURL(fileURLWithPath: mainDocumentPath).appendingPathComponent(filename)
                        try fileManager.removeItem(at: writePath!)
                    }
                }
                catch {
                }
            }
        })
    }
}
