//
/*
 * Copyright (c) 2023 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import SwiftUI
import Down

struct MarkDownView: View {
    var markdownString: String
    @State var labelHeight: CGFloat = .zero
    
    var body: some View {
        GeometryReader { geometry in
            ScrollView(.vertical) {
                UIMarkDownWrapper(markDownString: markdownString, height: $labelHeight)
                    .frame(width: geometry.size.width - 20)
                    .frame(minHeight: labelHeight)
                    .padding(10)
            }
            .background(Color(ThemeService.shared().theme.backgroundColor))
        }
    }
}

struct UIMarkDownWrapper: UIViewRepresentable {
    var markDownString: String
    @Binding var height: CGFloat
    
    func makeUIView(context: Context) -> UILabel {
        let label = UILabel(frame: .zero)
        label.numberOfLines = 0
        label.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        return label
    }
    
    func updateUIView(_ uiView: UILabel, context: Context) {
        let down = Down(markdownString: markDownString)

        guard let attributedString = try? down.toAttributedString(stylesheet: "* {font-family: sans-serif; font-size: 16pt; } ") else { return }
        let mutableString = NSMutableAttributedString(attributedString: attributedString)
        mutableString.addAttributes([.foregroundColor: ThemeService.shared().theme.textPrimaryColor], range: NSRange(location: 0, length: attributedString.length))
        
        uiView.attributedText = mutableString
        DispatchQueue.main.async {
            height = uiView.sizeThatFits(CGSize(width: uiView.bounds.width, height: CGFloat.greatestFiniteMagnitude)).height
        }
    }
    
}
