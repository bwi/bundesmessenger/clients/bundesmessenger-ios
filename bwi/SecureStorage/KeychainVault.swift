//
/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import KeychainAccess
import Foundation


class KeychainVault: KeyValueVault {
    
    private let keychain: Keychain
    
    init(_ keychain: Keychain) {
        self.keychain = keychain
    }
    
    // MARK: -
    
    func objectExists(withKey key: String) -> Bool {
        do {
            return try keychain.contains(key)
        } catch {
            return false
        }
    }
    
    func removeObject(forKey key: String) throws {
        try keychain.remove(key)
    }
    
    func reset() throws {
        try keychain.removeAll()
    }

    // MARK: - Getter
    
    func bool(forKey key: String) throws -> Bool? {
        return try keychain.getBool(key)
    }
    
    func integer(forKey key: String) throws -> Int? {
        if let stringValue = try keychain.getString(key) {
            return Int(stringValue)
        } else {
            return nil
        }
    }
    
    func unsignedInteger(forKey key: String) throws -> UInt? {
        if let stringValue = try keychain.getString(key) {
            return UInt(stringValue)
        } else {
            return nil
        }
    }
    
    func string(forKey key: String) throws -> String? {
        return try keychain.getString(key)
    }

    func data(forKey key: String) throws -> Data? {
        return try keychain.getData(key)
    }

    // MARK: - Setter
    
    func set(_ value: Bool?, forKey key: String) throws {
        if let value = value {
            try keychain.set(value, key: key)
        } else {
            try removeObject(forKey: key)
        }
    }
    
    func set(_ value: Int?, forKey key: String) throws {
        if let value = value {
            try keychain.set(String(value), key: key)
        } else {
            try removeObject(forKey: key)
        }
    }
    
    func set(_ value: UInt?, forKey key: String) throws {
        if let value = value {
            try keychain.set(String(value), key: key)
        } else {
            try removeObject(forKey: key)
        }
    }
    
    func set(_ value: String?, forKey key: String) throws {
        if let value = value {
            try keychain.set(value, key: key)
        } else {
            try removeObject(forKey: key)
        }
    }

    func set(_ value: Data?, forKey key: String) throws {
        if let value = value {
            try keychain.set(value, key: key)
        } else {
            try removeObject(forKey: key)
        }
    }

}
