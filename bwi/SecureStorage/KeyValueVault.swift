//
/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation


protocol KeyValueVault {
    func objectExists(withKey key: String) -> Bool
    func removeObject(forKey key: String) throws
    func reset() throws

    func bool(forKey key: String) throws -> Bool?
    func integer(forKey key: String) throws -> Int?
    func unsignedInteger(forKey key: String) throws -> UInt?
    func string(forKey key: String) throws -> String?
    func data(forKey key: String) throws -> Data?

    func set(_ value: Bool?, forKey key: String) throws
    func set(_ value: Int?, forKey key: String) throws
    func set(_ value: UInt?, forKey key: String) throws
    func set(_ value: String?, forKey key: String) throws
    func set(_ value: Data?, forKey key: String) throws
}
