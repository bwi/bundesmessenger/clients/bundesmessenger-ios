//
/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation


class MemoryVault: KeyValueVault {
    
    private(set) var dict = [String: Any]()
        
    init(_ dict: [String: Any] = [:]) {
        self.dict = dict
    }
    
    // MARK: -
    
    func objectExists(withKey key: String) -> Bool {
        return object(forKey: key) != nil
    }
    
    func removeObject(forKey key: String) throws {
        dict.removeValue(forKey: key)
    }
    
    func reset() throws {
        dict.removeAll()
    }
    
    private func setObject(_ value: Any?, forKey key: String) {
        if let value = value {
            dict[key] = value
        } else {
            try? removeObject(forKey: key)
        }
    }

    private func object(forKey key: String) -> Any? {
        return dict[key]
    }

    // MARK: - Getter
    
    func bool(forKey key: String) throws -> Bool? {
        return object(forKey: key) as? Bool
    }
    
    func integer(forKey key: String) throws -> Int? {
        return object(forKey: key) as? Int
    }
    
    func unsignedInteger(forKey key: String) throws -> UInt? {
        return object(forKey: key) as? UInt
    }
    
    func string(forKey key: String) throws -> String? {
        return object(forKey: key) as? String
    }

    func data(forKey key: String) throws -> Data? {
        return object(forKey: key) as? Data
    }

    // MARK: - Setter
    
    func set(_ value: Bool?, forKey key: String) throws {
        setObject(value, forKey: key)
    }

    func set(_ value: Int?, forKey key: String) throws {
        setObject(value, forKey: key)
    }
    
    func set(_ value: UInt?, forKey key: String) throws {
        setObject(value, forKey: key)
    }
    
    func set(_ value: String?, forKey key: String) throws {
        setObject(value, forKey: key)
    }

    func set(_ value: Data?, forKey key: String) throws {
        setObject(value, forKey: key)
    }

}
