//
/*
 * Copyright (c) 2023 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation
@objc class FederationIconHelper: NSObject {
    @objc static let shared = FederationIconHelper()
    
    // MARK: Federation pill with text
    
    // bwi: 5216 and 5204 - federation
    @objc func roomNameWithFederationPill(roomDisplayName: String?, font: UIFont) -> NSAttributedString? {
        guard let roomDisplayName = roomDisplayName else {
            return nil
        }
        
        let attributedString = NSMutableAttributedString(string: roomDisplayName + " ")
        
        // get the correct asset for the current theme and language
        let image: UIImage
        image = getFederationPillImage()
        
        // append the pill to the room name
        if image.size.width > 0.0 && image.size.height > 0.0 {
            let attachment = NSTextAttachment()
            attachment.image = image
            attachment.bounds = calculateFederationPillBounds(image:image, pillHeight: CGFloat(20.0), font: font)
            attributedString.append(NSAttributedString(attachment: attachment))
        }
        
        return attributedString
    }
    
    // #5226 us12 add federation pill to navigationViewTitle
    @objc func getRoomFederationPillView(textView: UITextField) -> UIView {
        if let font = textView.font {
            let image = getFederationPillImage()
            if image.size.width > 0.0 && image.size.height > 0.0 {
                let imageView = UIImageView(frame: calculateFederationPillBounds(image: image, pillHeight: textView.frame.size.height, font: font))
                imageView.contentMode = .center
                imageView.clipsToBounds = true
                let view = UIView()
                view.bounds = calculateFederationPillBounds(image: image, pillHeight: textView.frame.size.height, font: font)
                view.addSubview(imageView)
                imageView.image = image
                return view
            } else {
                return UIView()
            }
        } else {
            return UIView()
        }
    }
    
    @objc func getFederationPillImage() -> UIImage {
        // get the correct asset for the current theme and language
        let image: UIImage
        if Locale.current.languageCode == "de" {
            image = ThemeService.shared().isCurrentThemeDark() ? Asset.Images.federationPillDeBumDark.image : Asset.Images.federationPillDeBumLight.image
        } else {
            image = ThemeService.shared().isCurrentThemeDark() ? Asset.Images.federationPillEnBumDark.image : Asset.Images.federationPillEnBumLight.image
        }
        
        return image
    }
    
    // bwi 5575 insert acl exclamationmark here
    @objc func getFederationExclamationMarkImage() -> UIImage {
        return ThemeService.shared().isCurrentThemeDark() ? Asset.Images.federationExclamationMarkDark.image : Asset.Images.federationExclamationMarkLight.image
    }
    
    private func calculateFederationPillBounds(image: UIImage, pillHeight: CGFloat, font: UIFont) -> CGRect
    {
        let aspectRatio = image.size.width / image.size.height
        let offset = 0.5 * (font.capHeight - pillHeight)
        return CGRect(x: 0, y: offset, width: pillHeight * aspectRatio, height: pillHeight)
    }
    
    // MARK: Federation icon
    
    // Calculate the size and position of the federated pill for an avatar ImageView
    @objc func getFederatedIconImageView(avatarFrame: CGRect) -> UIImageView {
        // because of svg we use fixed values here for the raw image size
        let pillImageWidth: CGFloat = 23.0
        let pillImageHeight: CGFloat = 18.0
        let pillBorderSize: CGFloat = 3.0
        
        // compensation for the bottom gap in the asset
        let verticalShift: CGFloat = ((avatarFrame.size.height * 0.34) / pillImageHeight) * 1.5
        
        let avatarSize = avatarFrame.size.height
        let pillHeight: CGFloat = avatarSize * 0.34   // 34% relative size by design rule
        let pillWidth: CGFloat = pillHeight / pillImageHeight * pillImageWidth
        let scaledBorderSize: CGFloat = pillBorderSize * pillHeight / pillImageHeight
        let frame = CGRect(x: avatarFrame.origin.x + avatarSize - pillWidth + scaledBorderSize, y: verticalShift + avatarFrame.origin.y + avatarSize - pillHeight, width: pillWidth, height: pillHeight)
        return UIImageView(frame: frame)
    }
    
    @objc func federationBadgeImage() -> UIImage {
        if ThemeService.shared().isCurrentThemeDark() {
            return Asset.Images.roomFederatedBumIconDark.image
        } else {
            return Asset.Images.roomFederatedBumIconLight.image
        }
    }
}
