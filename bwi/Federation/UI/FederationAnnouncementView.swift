//
/*
 * Copyright (c) 2024 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import SwiftUI

struct FederationAnnouncementView: View {
    @Environment(\.dismiss) var dismissView
    @EnvironmentObject var themeService: BWIThemeService
    
    let imageStackScale = 0.30
    let imageStackShift = 5.0
    let outerLineWith = 5.0
    let innerLineWith = 1.0
    let spacing = 16.0
    
    var announcementText: AttributedString {
        do {
            return try AttributedString(markdown: (BWIL10n.federationAnnouncementText), options: AttributedString.MarkdownParsingOptions(interpretedSyntax: .inlineOnlyPreservingWhitespace))
        } catch {
            return AttributedString(BWIL10n.federationAnnouncementText)
        }
    }
    
    var body: some View {
        GeometryReader { geo in
            ScrollView(.vertical) {
                VStack(spacing: spacing) {
                    
                    Spacer()
                    
                    ZStack(alignment: .center) {
                        Circle()
                            .fill(Color(themeService.theme.colors.quinaryContent))
                            .padding(outerLineWith)
                            .offset(x:((getImageSize(width: geo.size.width, height: geo.size.height) * imageStackScale) / imageStackShift))
                    
                        Image(uiImage: Asset.SharedImages.loginFlowLogo.image)
                            .resizable()
                            .aspectRatio(contentMode: .fill)
                            .clipShape(Circle())
                            .padding(innerLineWith)
                            .background(Color(themeService.theme.colors.quinaryContent))
                            .clipShape(Circle())
                            .padding(outerLineWith)
                            .background(Color(themeService.theme.colors.background))
                            .clipShape(Circle())
                            .offset(x:-((getImageSize(width: geo.size.width, height: geo.size.height) * imageStackScale) / imageStackShift))
                    }
                    .frame(width: getImageSize(width: geo.size.width, height: geo.size.height) * imageStackScale, height: getImageSize(width: geo.size.width, height: geo.size.height) * imageStackScale)
                    
                    LazyVStack(spacing: spacing) {
                        Text(BWIL10n.federationAnnouncementTitle)
                            .font(.system(size: 24).bold())
                            .multilineTextAlignment(.center)
                            .lineLimit(nil)
                            .foregroundColor(Color(themeService.theme.colors.primaryContent))
                        Text(announcementText)
                            .font(Font(themeService.theme.fonts.subheadline))
                            .multilineTextAlignment(.center)
                            .lineLimit(nil)
                            .foregroundColor(Color(themeService.theme.colors.primaryContent))
                        Text(BWIL10n.federationAnnouncementSubText)
                            .font(Font(themeService.theme.fonts.subheadline))
                            .multilineTextAlignment(.center)
                            .lineLimit(nil)
                            .foregroundColor(Color(themeService.theme.colors.secondaryContent))
                    }
                    .padding(spacing)
                    
                    Spacer()
                    
                    Button {
                        dismissView()
                    } label: {
                        Text(BWIL10n.federationAnnouncementButton)
                    }
                    .buttonStyle(PrimaryActionButtonStyle())
                    .accessibilityIdentifier("federationAnnouncementCloseButton")
                    .padding(spacing)
                }
                .frame(minHeight: geo.size.height)
            }
            .interactiveDismissDisabled()
            .frame(width: geo.size.width, height: geo.size.height)
            .background(Color(themeService.theme.colors.background))
        }
    }
    
    func getImageSize(width: CGFloat, height: CGFloat) -> CGFloat {
        return (width > height ? height : width)
    }
}
