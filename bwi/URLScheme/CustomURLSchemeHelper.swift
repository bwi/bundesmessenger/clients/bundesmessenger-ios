//
/*
 * Copyright (c) 2023 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation

@objcMembers class CustomURLSchemeHelper : NSObject {
    static let shared = CustomURLSchemeHelper()
    
    func overrideURLSchemeIfNeeded(_ urlComponents: URLComponents) -> URLComponents {
        if AppConfigService.shared.externalUrlScheme() != nil {
            guard let url = urlComponents.url else {
                return urlComponents
            }
            // Check if link is a permalink
            if MXKTools.isLinkPermalink(url.absoluteString) {
                return urlComponents
            }
            // Only override http / https
            if (urlComponents.scheme ?? "http").elementsEqual("http") || (urlComponents.scheme ?? "https").elementsEqual("https") {
                var newURLComponents = urlComponents
                if let urlScheme = AppConfigService.shared.externalUrlScheme() {
                    newURLComponents.scheme = urlScheme
                }
                return newURLComponents
            } else {
                return urlComponents
            }
        } else {
            return urlComponents
        }
    }
}
