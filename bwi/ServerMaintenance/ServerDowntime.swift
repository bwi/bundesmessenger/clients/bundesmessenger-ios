//
/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation

struct ServerDowntime : Codable {
    let warningStartTime: String
    let startTime: String
    let endTime: String
    let type: String
    let description: String?
    let blocking: Bool?
    
    private enum CodingKeys: String, CodingKey {
        case warningStartTime = "warning_start_time"
        case startTime = "start_time"
        case endTime = "end_time"
        case type
        case description
        case blocking
    }

    init(dict: [String: Any]) throws {
        let jsonData = try JSONSerialization.data(withJSONObject: dict, options: [])
        self = try JSONDecoder().decode(Self.self, from: jsonData)
    }
}

