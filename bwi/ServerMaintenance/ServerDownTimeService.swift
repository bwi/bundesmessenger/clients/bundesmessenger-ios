//
/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation
import SwiftUI

enum ServerDowntimeStatus {
    case none
    case warning
    case ongoing
}

enum ServerMaintenanceAlertType {
    case showServerMaintenanceInfoMessageAlert, showServerMaintenanceDefaultAlert, showInvalidAppVersionAlert, showDowntimeTimeAlert
}

enum ServerDowntimeType: String {
    case adhocMessage = "ADHOC_MESSAGE"
    case maintenance = "MAINTENANCE"
}

protocol ServerDowntimeService {
    func fetchDowntimes(session: MXSession, completion: @escaping () -> Void)
    func loadDowntimes(downtimes: [[String: Any]]) throws
    func isDowntimePresentable() -> Bool
    func downtimeText() -> String
    func downtimeColor() -> UIColor
    func downtimeTextColor() -> UIColor
    func isSameDay() -> Bool
    func isBlocking() -> Bool
    func setManuallyIgnored()
    func isManuallyIgnored() -> Bool
    func alert( alertType:ServerMaintenanceAlertType, completion: @escaping () -> Void) -> Alert
    func showAlert() -> Bool
    func alertType() -> ServerMaintenanceAlertType
}
