//
/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation
import UIKit

@objcMembers class ServerDowntimeBadge : NSObject {
    
    func applyBadgeToButton(button: UIButton, color: UIColor) -> UIButton {
        let badge = UILabel.init(frame: CGRect.init(x: 20, y: 0, width: 15, height: 15))
        badge.backgroundColor = color
        badge.clipsToBounds = true
        badge.layer.cornerRadius = 7
        badge.textColor = UIColor.white
        badge.textAlignment = .center

        button.addSubview(badge)
        button.bringSubviewToFront(badge)
        
        return button
    }
    
    func createBadgeBarButton(color: UIColor, target: AnyObject, selector:Selector) -> UIBarButtonItem {
        
        let button = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 30, height: 30))
        button.setImage(Asset.Images.settingsIcon.image, for: .normal)
        button.addTarget(target, action: selector, for: .touchUpInside)
        
        return UIBarButtonItem.init(customView: self.applyBadgeToButton(button: button, color: color))
    }
}
