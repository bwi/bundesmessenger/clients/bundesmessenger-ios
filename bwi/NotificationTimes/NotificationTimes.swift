//
/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation
import Combine

@available(iOS 14.0, *)
@objcMembers class NotificationTimes: NSObject, Codable {
    static let shared = NotificationTimes()
    static let sharedUserDefaultsKey = "notificationTimes"
    
    var isEnabled: Bool
    var monday, tuesday, wednesday, thursday, friday, saturday, sunday : NotificationTimesWeekday
    var rooms: [String : Bool]

    func weekday(index: Int) -> NotificationTimesWeekday {
        switch index {
        case 0:
            return monday
        case 1:
            return tuesday
        case 2:
            return wednesday
        case 3:
            return thursday
        case 4:
            return friday
        case 5:
            return saturday
        case 6:
            return sunday
        default:
            assertionFailure("weekday index outside range 0...6")
            return monday
        }
    } 
    
    override init() {
        isEnabled = false
        
        monday = NotificationTimesWeekday(id: 0,
                                          name: BWIL10n.bwiNotificationTimesMonday,
                         shortName: BWIL10n.bwiNotificationTimesMondayShort)
        tuesday = NotificationTimesWeekday(id: 1,
                          name: BWIL10n.bwiNotificationTimesTuesday,
                          shortName: BWIL10n.bwiNotificationTimesTuesdayShort)
        wednesday = NotificationTimesWeekday(id: 2,
                            name: BWIL10n.bwiNotificationTimesWednesday,
                                             shortName: BWIL10n.bwiNotificationTimesWednesdayShort)
        thursday = NotificationTimesWeekday(id: 3,
                                            name: BWIL10n.bwiNotificationTimesThursday,
                           shortName: BWIL10n.bwiNotificationTimesThursdayShort)
        friday = NotificationTimesWeekday(id: 4,
                         name: BWIL10n.bwiNotificationTimesFriday,
                         shortName: BWIL10n.bwiNotificationTimesFridayShort)
        saturday = NotificationTimesWeekday(id: 5,
                           name: BWIL10n.bwiNotificationTimesSaturday,
                           shortName: BWIL10n.bwiNotificationTimesSaturdayShort)
        sunday = NotificationTimesWeekday(id: 6,
                         name: BWIL10n.bwiNotificationTimesSunday,
                         shortName: BWIL10n.bwiNotificationTimesSundayShort)
        
        rooms = [:]
    }
    
    func isEnabledForRoom(roomID: String, isDirect: Bool) -> Bool {
        if isDirect {
            return rooms[roomID] ?? false
        } else {
            return rooms[roomID] ?? true
        }
    }

    func enableForRoom(roomID: String) {
        rooms[roomID] = true
    }

    func disableForRoom(roomID: String) {
        rooms[roomID] = false
    }
    
    static func storeToSharedUserDefaults() {
        if let data = try? JSONEncoder().encode(NotificationTimes.shared) {
            let sharedUserDefaults = MXKAppSettings.standard().sharedUserDefaults
            sharedUserDefaults?.set(data, forKey: NotificationTimes.sharedUserDefaultsKey)
        }
    }

}
