//
/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation

@available(iOS 14.0, *)
class NotificationTimesWeekday: ObservableObject, Identifiable, Codable {
    @Published var isEnabled: Bool
    @Published var name: String
    @Published var shortName: String
    @Published var startTime: Date
    @Published var endTime: Date
    let id: Int
    
    private enum CodingKeys : String, CodingKey {
        case fromHour = "from_hour"
        case toHour = "to_hour"
        case fromMinute = "from_minute"
        case toMinute = "to_minute"
        case isEnabled = "is_enabled"
    }

    init(id: Int, name: String, shortName: String, isEnabled: Bool = false, startTime: Date = Date.from(hour: 8, minute: 0), endTime: Date = Date.from(hour: 17, minute: 0)) {
        self.id = id
        self.name = name
        self.shortName = shortName
        self.isEnabled = isEnabled
        self.startTime = startTime
        self.endTime = endTime
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let fromHour = try container.decode(Int.self, forKey: .fromHour)
        let toHour = try container.decode(Int.self, forKey: .toHour)
        let fromMinute = try container.decode(Int.self, forKey: .fromMinute)
        let toMinute = try container.decode(Int.self, forKey: .toMinute)
        
        isEnabled = try container.decode(Bool.self, forKey: .isEnabled)

        id = 0
        name = ""
        shortName = ""
        startTime = Date.from(hour: fromHour, minute: fromMinute)
        endTime = Date.from(hour: toHour, minute: toMinute)
    }
    
    func encode(to encoder: Encoder) throws {
        let calender = Calendar.current
        let fromHour = calender.component(.hour, from: startTime)
        let toHour = calender.component(.hour, from: endTime)
        let fromMinute = calender.component(.minute, from: startTime)
        let toMinute = calender.component(.minute, from: endTime)
        
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(isEnabled, forKey: .isEnabled)
        try container.encode(fromHour, forKey: .fromHour)
        try container.encode(toHour, forKey: .toHour)
        try container.encode(fromMinute, forKey: .fromMinute)
        try container.encode(toMinute, forKey: .toMinute)
    }

    func copy(with zone: NSZone? = nil) -> NotificationTimesWeekday {
        return NotificationTimesWeekday(id: id, name: name, shortName: shortName, isEnabled: isEnabled, startTime: startTime, endTime: endTime)
    }
}
