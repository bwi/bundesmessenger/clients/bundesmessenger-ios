//
/*
 * Copyright (c) 2023 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import SwiftUI

enum VisibleScreen {
    case screen1
    case screen2
    case screen3
}

struct IntroduceFederationView: View {
    static let imageSize: CGFloat = 108
    static let topConstraintTitle: CGFloat = 390
    @EnvironmentObject var themeService: BWIThemeService
    @State var visibleScreen = VisibleScreen.screen1
    @State var redrawKey = UUID()

    var body: some View {
        TabView(selection: $visibleScreen) {
            IntroduceFederationScreen1().tag(VisibleScreen.screen1).navigationBarHidden(true)
            IntroduceFederationScreen2().tag(VisibleScreen.screen2).navigationBarHidden(true)
            IntroduceFederationScreen3().tag(VisibleScreen.screen3).navigationBarHidden(true)
        }
        .id(redrawKey)
        .tabViewStyle(.page(indexDisplayMode: .always))
        .background {
            Color(themeService.theme.backgroundColor)
                .edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
        }
        .overlay(alignment: .bottom) {
            HStack {
                if visibleScreen != .screen1 {
                    Button(action: presentPreviousScreen) {
                        Image(systemName: "arrow.left")
                            .foregroundColor(Color(themeService.theme.textSecondaryColor))
                            .padding()
                    }
                }
                Spacer()
                if visibleScreen != .screen3 {
                    Button(action: presentNextScreen) {
                        Image(systemName: "arrow.right")
                            .foregroundColor(Color(themeService.theme.textSecondaryColor))
                            .padding()
                    }
                }
            }
            .padding()
        }
        .onAppear {
          setupIndicatorColors()
        }
        .onChange(of: themeService.isCurrentThemeDark) { _ in
            setupIndicatorColors()
            redrawKey = UUID()
        }
    }
    
    private func setupIndicatorColors() {
        UIPageControl.appearance().currentPageIndicatorTintColor = themeService.theme.textPrimaryColor
        UIPageControl.appearance().pageIndicatorTintColor = themeService.theme.textSecondaryColor
    }
    
    private func presentNextScreen() {
        withAnimation {
            switch visibleScreen {
            case .screen1:
                visibleScreen = .screen2
            case .screen2:
                visibleScreen = .screen3
            case .screen3:
                break
            }
        }
    }
    
    private func presentPreviousScreen() {
        withAnimation {
            switch visibleScreen {
            case .screen1:
                break
            case .screen2:
                visibleScreen = .screen1
            case .screen3:
                visibleScreen = .screen2
            }
        }
    }

}
