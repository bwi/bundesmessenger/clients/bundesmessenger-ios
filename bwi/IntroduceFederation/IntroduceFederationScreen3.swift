//
/*
 * Copyright (c) 2023 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import SwiftUI

struct IntroduceFederationScreen3: View {
    @EnvironmentObject var themeService: BWIThemeService
    @Environment(\.dismiss) var dismiss

    var body: some View {
        VStack(spacing: 26) {
            Image(uiImage: Asset.Images.introduceFederation3.image)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(height: IntroduceFederationView.topConstraintTitle)
                .padding(.bottom, 42)
            Text(BWIL10n.introduceFederationScreen3Title)
                .foregroundColor(Color(themeService.theme.textPrimaryColor))
                .font(.system(size: 28, weight: .semibold))
                .multilineTextAlignment(.center)
            Text(BWIL10n.introduceFederationScreen3Description)
                .foregroundColor(Color(themeService.theme.textPrimaryColor))
                .font(.system(size: 17))
                .multilineTextAlignment(.center)
            Spacer()
            Button(action: onConfirm) {
                Text(BWIL10n.introduceFederationStart)
            }
            .buttonStyle(PrimaryActionButtonStyle())
            .padding(.bottom, 80)
        }
        .padding(.horizontal)
   }
    
    private func onConfirm() {
        dismiss()
    }
}
