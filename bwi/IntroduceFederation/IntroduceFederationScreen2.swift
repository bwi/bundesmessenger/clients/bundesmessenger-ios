//
/*
 * Copyright (c) 2023 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import SwiftUI

struct IntroduceFederationScreen2: View {
    @EnvironmentObject var themeService: BWIThemeService
    
    var body: some View {
        ZStack(alignment: .top) {
            Color(themeService.theme.backgroundColor)
                
            VStack(spacing: 26) {
                Image(uiImage: getImage(darkmode: themeService.isCurrentThemeDark))
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(height: IntroduceFederationView.imageSize + 12)
                    .padding(.bottom, 42)
                Text(BWIL10n.introduceFederationScreen2Title)
                    .foregroundColor(Color(themeService.theme.textPrimaryColor))
                    .font(.system(size: 28, weight: .semibold))
                    .multilineTextAlignment(.center)
                Text(BWIL10n.introduceFederationScreen2Description)
                    .foregroundColor(Color(themeService.theme.textPrimaryColor))
                    .font(.system(size: 17))
                    .multilineTextAlignment(.center)
                Spacer()
            }
            .padding(.horizontal)
            .padding(.top, IntroduceFederationView.topConstraintTitle - IntroduceFederationView.imageSize - 12)
        }
    }
    
    private func getImage(darkmode: Bool) -> UIImage {
        return darkmode ? Asset.Images.introduceFederation2Dark.image : Asset.Images.introduceFederation2Light.image
    }
}
