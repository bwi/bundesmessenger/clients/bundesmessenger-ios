//
/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation
import Combine

@objcMembers class Mentions: NSObject, Codable {
    static let sharedUserDefaultsKey = "mentions"
    
    var mentionsInRoom: [String : Int] = [:]

    static func countMentions(roomID: String) -> Int {
        guard let sharedUserDefaults = MXKAppSettings.standard().sharedUserDefaults else {
            return 0
        }
        guard let data = sharedUserDefaults.data(forKey: Mentions.sharedUserDefaultsKey), let mentions = try? JSONDecoder().decode(Mentions.self, from: data) else {
            return 0
        }
        return mentions.mentionsInRoom[roomID] ?? 0
    }
    
    static func setCountMentionsToZero(roomID: String) {
        guard let sharedUserDefaults = MXKAppSettings.standard().sharedUserDefaults else {
            return
        }
        guard let data = sharedUserDefaults.data(forKey: Mentions.sharedUserDefaultsKey), let mentions = try? JSONDecoder().decode(Mentions.self, from: data) else {
            return
        }
        
        mentions.mentionsInRoom.removeValue(forKey: roomID)
        
        if let data = try? JSONEncoder().encode(mentions) {
            sharedUserDefaults.set(data, forKey: Mentions.sharedUserDefaultsKey)
        }
    }
}
