//
/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation

struct WellknownSecurity {
    let backupRequired: Bool
    let backupMethods: Array<String>?
    let preSharingMode: String?
    
    init(dict: [String: Any]) throws {
        let jsonData = try JSONSerialization.data(withJSONObject: dict, options: [])
        let decoder = JSONDecoder()
        self = try decoder.decode(Self.self, from: jsonData)
    }
}

extension WellknownSecurity: Decodable {
    enum CodingKeys: String, CodingKey {
        case backupRequired = "secure_backup_required"
        case backupMethods = "secure_backup_setup_methods"
        case preSharingMode = "outbound_keys_pre_sharing_mode"
    }
}
