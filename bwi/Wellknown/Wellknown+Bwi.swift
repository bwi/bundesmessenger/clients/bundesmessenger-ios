//
/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation

public extension MXWellKnown {
    @objc func backupRequired() -> Bool {
    
        if let dict = self.jsonDictionary()["io.element.e2ee"] as? [String : Any], let security = try? WellknownSecurity(dict: dict) {
            return security.backupRequired
        } else {
            return true
        }
    }
    
    @objc func backupMethods() -> Array<String>? {
    
        if let dict = self.jsonDictionary()["io.element.e2ee"] as? [String : Any], let security = try? WellknownSecurity(dict: dict) {
            return security.backupMethods
        }
        else {
            return ["passphrase"]
        }
    }
    
    @objc func isBackupMethodKeySupported() -> Bool {
        return self.backupMethods()?.contains("key") == true
    }
    
    @objc func isBackupMethodPassphraseSupported() -> Bool {
        return self.backupMethods()?.contains("passphrase") != nil
    }
    
    @objc func preSharingMode() -> MXKKeyPreSharingStrategy {
        do {
            let security = try WellknownSecurity(dict: self.jsonDictionary()["io.element.e2ee"] as! [String : Any])
            
            guard let preSharingMode = security.preSharingMode else {
                return .whenTyping
            }
            
            if preSharingMode == "on_room_opening" {
                return .whenEnteringRoom
            } else if preSharingMode == "on_typing" {
                return .whenTyping
            } else {
                return .none
            }
        }
        catch {
            return .whenTyping
        }
    }
    
    // returns true if the federation introduction screen should be presented to the new users
    @objc func shouldShowFederationIntroduction() -> Bool {
        do {
            let bwi = try WellknownBWI(dict: self.jsonDictionary()["de.bwi"] as! [String : Any])
            return bwi.federation?.showIntroduction ?? false
        } catch {
            return false
        }
    }
    
    @objc func shouldShowFederationAnnouncement() -> Bool {
        do {
            guard let bwiDict = self.jsonDictionary()["de.bwi"] as? [String : Any] else {
                return false
            }
            
            let bwi = try WellknownBWI(dict: bwiDict)
            if let federation = bwi.federation {
                return federation.showAnnouncement ?? false
            } else {
              return false
            }
        }
        catch {
            return false
        }
    }
    
    @objc func updateFederationStatus() {
        do {
            guard let bwiDict = self.jsonDictionary()["de.bwi"] as? [String : Any] else {
                BWIBuildSettings.shared.isWellknownFederationEnabled = false
                return
            }
            
            let bwi = try WellknownBWI(dict: bwiDict)
            if let federation = bwi.federation {
                BWIBuildSettings.shared.isWellknownFederationEnabled = federation.enable ?? false
            } else {
                BWIBuildSettings.shared.isWellknownFederationEnabled = false
            }
        }
        catch {
            BWIBuildSettings.shared.isWellknownFederationEnabled = false
        }
    }

    // returns true if there is a valid url or no url, only the case "is url" but not valid is wrong
    @objc func isValidDataPrivacyURL() -> Bool {
        do {
            let bwi = try WellknownBWI(dict: self.jsonDictionary()["de.bwi"] as! [String : Any])
            
            if let urlString = bwi.dataPrivacyURL {
                if URL(string: urlString) != nil {
                    return true
                } else {
                    return false
                }
            } else {
                return true
            }
        } catch {
            return true
        }
    }
    
    @objc func dataPrivacyURL() -> String? {
        do {
            guard let bwiDict = self.jsonDictionary()["de.bwi"] as? [String : Any] else {
                return nil
            }
            
            let bwi = try WellknownBWI(dict: bwiDict)
            return  bwi.dataPrivacyURL
        }
        catch {
            return nil
        }
    }
    
    @objc func imprintURL() -> String? {
        do {
            guard let bwiDict = self.jsonDictionary()["de.bwi"] as? [String : Any] else {
                return nil
            }
            
            let bwi = try WellknownBWI(dict: bwiDict)
            return  bwi.imprintURL
        }
        catch {
            return nil
        }
    }
}
