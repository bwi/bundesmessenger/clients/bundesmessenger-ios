//
/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation

struct WellknownBWI: Decodable {
    let dataPrivacyURL: String?
    let imprintURL: String?
    let federation: WellknownFederation?
    
    init(dict: [String: Any]) throws {
        let jsonData = try JSONSerialization.data(withJSONObject: dict, options: [])
        let decoder = JSONDecoder()
        self = try decoder.decode(Self.self, from: jsonData)
    }
    
    enum CodingKeys: String, CodingKey {
        case dataPrivacyURL = "data_privacy_url"
        case imprintURL = "imprint_url"
        case federation
    }
}

struct WellknownFederation: Decodable {
    // show announcement (1 screen)
    let showAnnouncement: Bool?
    
    // show federation tutorial (3 screens)
    let showIntroduction: Bool?
    
    // enables the federation feature for the app - show federation views, activate invitation rules, show federation settings
    let enable: Bool?
    
    enum CodingKeys: String, CodingKey {
        case showAnnouncement = "show_announcement"
        case showIntroduction = "show_introduction"
        case enable
    }
    
}
