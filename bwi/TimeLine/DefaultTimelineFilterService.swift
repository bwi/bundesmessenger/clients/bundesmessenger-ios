/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation

@objcMembers class DefaultTimelineFilterService : NSObject {
    func isDisplayNameChangeEvent(_ event: MXEvent) -> Bool {
        guard let content = event.content, let prevContent = event.prevContent else {
            return false
        }
        
        if let displayName = content["displayname"] as? String, let prevDisplayName = prevContent["displayname"] as? String {
            return displayName != prevDisplayName
        }
        
        return false
    }
    
    func isAvatarChangeEvent(_ event: MXEvent) -> Bool {
        guard let content = event.content, let prevContent = event.prevContent else {
            return false
        }
        
        if let avatarUrl = content["avatar_url"] as? String, let prevAvatarUrl = prevContent["avatar_url"] as? String {
            return avatarUrl != prevAvatarUrl
        }
        
        return false
    }
    
    func isRoomExitEvent(_ event: MXEvent) -> Bool {
        guard let content = event.content else {
            return false
        }
        
        if let membership = content["membership"] as? String {
            return membership == "leave"
        }
        
        return false
    }
    
    func isRoomEntryEvent(_ event: MXEvent) -> Bool {
        guard let content = event.content else {
            return false
        }
        
        if let membership = content["membership"] as? String {
            if membership == "join" {
                return content["avatar_url"] == nil
            }
        }
        
        return false
    }
    
    func isRoomInviteEvent(_ event: MXEvent) -> Bool {
        guard let content = event.content else {
            return false
        }
        
        if let membership = content["membership"] as? String {
            return membership == "invite"
        }
        
        return false
    }
}

extension DefaultTimelineFilterService : TimeLineFilterService {
    func shouldFilter(_ cellData: MXKRoomBubbleCellData?) -> Bool {
        
        guard let cellData = cellData else {
            return false
        }
        
        var filtered = false

        for event in cellData.events {
            
            if !RiotSettings.shared.settingUserAvatarChangeEnabled || RiotSettings.shared.settingSimpleTimelineEnabled {
                filtered = isAvatarChangeEvent(event) || isDisplayNameChangeEvent(event)
                if filtered {
                    break
                }
            }
            if !RiotSettings.shared.settingEnterRoomEnabled || RiotSettings.shared.settingSimpleTimelineEnabled {
                filtered = isRoomEntryEvent(event) || isRoomExitEvent(event) || isRoomInviteEvent(event)
                if filtered {
                    break
                }
            }
        }

        return filtered
    }
}
