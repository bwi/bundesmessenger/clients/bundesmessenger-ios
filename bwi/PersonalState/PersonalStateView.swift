//
/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import SwiftUI

/// Helper  class for making our SwiftUI view available to ObjectiveC
@objcMembers class PersonalStateViewController: NSObject {

    @available(iOS 14.0, *)
    class func makeViewController(session: MXSession) -> UIViewController {
        return UIHostingController(rootView: PersonalStateView(session: session))
    }
}

// MARK: -

@available(iOS 14.0, *)
struct PersonalStateView: View {
    @Environment(\.presentationMode) var presentationMode
    @State private var statusMessage: String = ""
    @State private var oldStatusMessage: String = ""
    private var hasChanges: Bool {
        return statusMessage.compare(oldStatusMessage) != .orderedSame
    }

    let session: MXSession?
    
    var body: some View {
        VStack(alignment: .leading, spacing: 20) {
            Text("bwi_edit_personal_state_my_state", tableName: "Vector")
                .font(.headline)
                .padding(.top, 40)

            Text("bwi_edit_personal_state_description", tableName: "Vector")
                .multilineTextAlignment(.leading)
            
            HStack {
                TextEditor(text: $statusMessage)
                    .frame(height: 100)
                    .accentColor(Color(ThemeService.shared().theme.tintColor))
                    .overlay(
                        RoundedRectangle(cornerRadius: 5)
                            .stroke(Color.init(red: 0.3, green: 0.3, blue: 0.3), lineWidth: 1)
                    )
                
                Button(action: clearTextField) {
                    Image(systemName: "delete")
                }
            }
            
            VStack(alignment: .center, spacing: 10) {
                Button {
                    statusMessage = BWIL10n.bwiEditPersonalStateOptionHomeoffice
                } label: {
                    Text("bwi_edit_personal_state_option_homeoffice", tableName: "Vector")
                        .foregroundColor(Color(ThemeService.shared().theme.tintColor))
                }
                
                Button {
                    statusMessage = BWIL10n.bwiEditPersonalStateOptionWork
                } label: {
                    Text("bwi_edit_personal_state_option_work", tableName: "Vector")
                        .foregroundColor(Color(ThemeService.shared().theme.tintColor))
                }
                
                Button {
                    statusMessage = BWIL10n.bwiEditPersonalStateOptionNotAvailable
                } label: {
                    Text("bwi_edit_personal_state_option_not_available", tableName: "Vector")
                        .foregroundColor(Color(ThemeService.shared().theme.tintColor))
                }
                
                if statusMessage.lengthOfBytes(using: .utf8) > 0 {
                    Button {
                        statusMessage = ""
                    } label: {
                        Text("bwi_edit_personal_state_option_reset", tableName: "Vector")
                            .foregroundColor(Color.red)
                    }
                }
            }
            .frame(maxWidth: .infinity)
            
            Spacer()
        }
        .padding()
        .navigationTitle(BWIL10n.bwiEditPersonalStateTitle)
        .navigationBarTitleDisplayMode(.inline)
        .toolbar {
            ToolbarItem(placement: .navigationBarTrailing) {
                Button(action: onDoneButton) {
                    Text(BWIL10n.bwiNotificationTimesDoneAction)
                        .foregroundColor(hasChanges ? Color(ThemeService.shared().theme.tintColor) : Color.secondary)
                }
                .disabled(!hasChanges)
            }
        }
        .onAppear{
            if let account = MXKAccountManager.shared().activeAccounts.first {
                statusMessage = account.userStatusMessage ?? ""
                oldStatusMessage = account.userStatusMessage ?? ""
            }
        }
    }

    private func clearTextField() {
        statusMessage = ""
    }
    
    private func onDoneButton() {
        if let account = MXKAccountManager.shared().activeAccounts.first {
            account.setUserPresence(.offline, andStatusMessage: statusMessage) {
            }
        }
        self.presentationMode.wrappedValue.dismiss()
    }
}

@available(iOS 14.0, *)
struct PersonalStateView_Previews: PreviewProvider {
    static var previews: some View {
        PersonalStateView(session: nil)
    }
}
