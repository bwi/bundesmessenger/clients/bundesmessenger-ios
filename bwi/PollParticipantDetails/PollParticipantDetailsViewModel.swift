//
/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation

import Combine
import SwiftUI

struct PollParticipantDetailsViewModelParameters {
    let poll: PollProtocol
    let room: MXRoom
}

typealias PollParticipantDetailsViewModelType = StateStoreViewModel<PollParticipantDetailsViewState, PollParticipantDetailsViewAction>

class PollParticipantDetailsViewModel: PollParticipantDetailsViewModelType, PollParticipantDetailsViewModelProtocol {
    
    init(parameters: PollParticipantDetailsViewModelParameters) {
        var state = PollParticipantDetailsViewState(poll: PollParticipantPoll(name: parameters.poll.text))
        
        var answers: [PollParticipantAnswer] = []
        
        let room = parameters.room
        
        for answerOption in parameters.poll.answerOptions {

            let answer = PollParticipantAnswer.buildPollParticipantAnswer(answerOption: answerOption, parameters: parameters)
            answers.append(answer)
        }
        answers.sort {
            $0.votes > $1.votes
        }
        
        state.answers = answers
        
        super.init(initialViewState: state)
    }
    
    // MARK: - Public
    
    override func process(viewAction: PollParticipantDetailsViewAction) {
        switch viewAction {
            
        case .openAllParticipants(index: let index):
            state.answers[index].expanded = true
            state.answers[index].visibleVotes = state.answers[index].votes
        case .closeAllParticipants(index: let index):
            state.answers[index].expanded = false
            state.answers[index].visibleVotes = min(state.answers[index].votes, state.poll.voterRows)
        }
    }
}
