//
/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation
import SwiftUI

struct PollParticipantDetailsView: View {
    // MARK: - Properties
    
    // MARK: Private
    
    @Environment(\.theme) private var theme: ThemeSwiftUI
    
    // MARK: Public
    
    @ObservedObject var viewModel: PollParticipantDetailsViewModel.Context
    
    var body: some View {
        NavigationView {
            VStack {
                PollParticipantPollHeaderView(poll: viewModel.viewState.poll)
                    .padding(.top, 10)
                
                List {
                    ForEach(Array(viewModel.viewState.answers.enumerated()), id: \.offset) { index, answer in
                        SwiftUI.Section(header: PollParticipantSectionHeaderView(answer: answer)) {
                            if answer.votes > 0 {
                                VStack {
                                    ForEach(answer.voters.prefix(upTo: answer.visibleVotes)) { voter in
                                        PollParticipantVoterView(voter: voter)
                                    }
                                    
                                    if answer.votes > answer.visibleVotes && !answer.expanded {
                                        Button(action: { onExpandButton(index: index) }) {
                                            Text(BWIL10n.pollParticipantDetailsShowMore(Int(answer.votes-answer.visibleVotes)))
                                        } 
                                    }
                                }
                            }
                        }
                    }
                }
                .listStyle(.grouped)
            }
        }
        .accentColor(theme.colors.accent)
        .navigationViewStyle(StackNavigationViewStyle())
        .navigationTitle(BWIL10n.pollParticipantDetailsTitle)
        .navigationBarTitleDisplayMode(.inline)
    }
    
    private func onExpandButton(index: Int) {
        viewModel.send(viewAction: .openAllParticipants(index: index))
    }
}

struct PollParticipantVoterView: View {
    @Environment(\.theme) private var theme: ThemeSwiftUI
    
    var voter: PollParticipantVoter
    
    var body: some View {
        HStack(alignment: .center, spacing: 10) {
            AvatarImage(avatarData: voter.userAvatarData, size: .medium)
                .border()
            
            VStack(alignment: .leading, spacing: 3) {
                Text(voter.displayName)
                    .font(theme.fonts.body)
                    .foregroundColor(theme.colors.primaryContent)
                Text(voter.formattedVotingTime)
                    .font(theme.fonts.footnote)
                    .foregroundColor(theme.colors.secondaryContent)
            }
            Spacer()
        }
    }
}

struct PollParticipantPollHeaderView: View {
    @Environment(\.theme) private var theme: ThemeSwiftUI
    
    var poll: PollParticipantPoll
    
    var body: some View {
        Text(poll.name)
            .font(theme.fonts.title3)
            .foregroundColor(theme.colors.primaryContent)
    }
}

struct PollParticipantSectionHeaderView: View {
    @Environment(\.theme) private var theme: ThemeSwiftUI
    
    var answer: PollParticipantAnswer
    
    var body: some View {
        HStack(alignment: .center) {
            Text(answer.name)
                .font(theme.fonts.headline)
                .foregroundColor(theme.colors.primaryContent)
            Spacer()
            Text(answer.votesText)
                .font(theme.fonts.footnote)
                .foregroundColor(theme.colors.secondaryContent)
                .accessibilityIdentifier("PollAnswerOptionCount")
        }
    }
}
