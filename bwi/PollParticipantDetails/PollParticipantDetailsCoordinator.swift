//
/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation
import SwiftUI
import UIKit

struct PollParticipantDetailsCoordinatorParameters {
    let room: MXRoom
    let poll: PollProtocol
}

final class PollParticipantDetailsCoordinator: Coordinator, Presentable {

    // MARK: Private
    
    private let parameters: PollParticipantDetailsCoordinatorParameters
    private let pollParticipantDetailsHostingController: UIViewController
    private var pollParticipantDetailsViewModel: PollParticipantDetailsViewModelProtocol
    
    // MARK: Public
    
    var childCoordinators: [Coordinator] = []
    
    var completion: (() -> Void)?
    
    // MARK: - Setup
    
    init(parameters: PollParticipantDetailsCoordinatorParameters) {
        self.parameters = parameters
        
        var viewModel: PollParticipantDetailsViewModel
        
        viewModel = PollParticipantDetailsViewModel.init(parameters: PollParticipantDetailsViewModelParameters(poll: parameters.poll, room: parameters.room))
        
        let view = PollParticipantDetailsView(viewModel: viewModel.context)
            .environmentObject(AvatarViewModel(avatarService: AvatarService(mediaManager: parameters.room.mxSession.mediaManager)))
        
        pollParticipantDetailsViewModel = viewModel
        pollParticipantDetailsHostingController = VectorHostingController(rootView: view)
    }
    
    // MARK: - Public

    func start() {
 
    }
    
    // MARK: - Presentable
    
    func toPresentable() -> UIViewController {
        pollParticipantDetailsHostingController
    }
}
