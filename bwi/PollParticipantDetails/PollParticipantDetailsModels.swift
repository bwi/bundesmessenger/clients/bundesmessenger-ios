//
/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation

struct PollParticipantDetailsViewState: BindableState {    
    var answers: [PollParticipantAnswer] = []
    var poll: PollParticipantPoll
}

enum PollParticipantDetailsMode {
    case someParticipants
    case allParticipants
}

enum PollParticipantDetailsViewAction {
    case openAllParticipants(index: Int)
    case closeAllParticipants(index: Int)
}

struct PollParticipantVoter: Identifiable, BindableState {
    var id: String {
        displayName
    }
    
    var displayName: String
    var userAvatarData: AvatarInputProtocol
    var formattedVotingTime: String
    
    static func buildPollParticipantVoter( event: MXEvent, room: MXRoom) -> PollParticipantVoter? {
        if let user = room.mxSession.user(withUserId: event.sender) {
            let avatarData = AvatarInput(mxContentUri: user.avatarUrl, matrixItemId: event.sender, displayName: user.displayname)
            
            let votingTime = Date(timeIntervalSince1970: TimeInterval(event.originServerTs / 1000))
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.calendar = Calendar.current
            var strDate = ""
            if Calendar.current.isDateInToday(votingTime) {
                dateFormatter.dateFormat = "HH:mm"
                strDate = BWIL10n.pollParticipantDetailsDateStringToday.appending(dateFormatter.string(from: votingTime))
            } else {
                dateFormatter.dateFormat = "E, d. MMM yyyy, HH:mm"
                strDate = dateFormatter.string(from: votingTime)
            }
            strDate.append(contentsOf: BWIL10n.pollParticipantDetailsClockString)
            
            return PollParticipantVoter(displayName: user.displayname, userAvatarData: avatarData, formattedVotingTime: strDate)
        } else {
            return nil
        }
    }
}

struct PollParticipantAnswer: Identifiable, BindableState {
    var id: String {
        originalId
    }
    
    var name: String
    var votes: Int
    var visibleVotes: Int
    var votesText: String
    var originalId: String
    var voters: [PollParticipantVoter]
    var expanded: Bool = false
    
    static func buildPollParticipantAnswer( answerOption: PollAnswerOptionProtocol, parameters: PollParticipantDetailsViewModelParameters) -> PollParticipantAnswer {
        var voters: [PollParticipantVoter] = []
        for participantEvent in answerOption.voters {
            if let voter = PollParticipantVoter.buildPollParticipantVoter(event: participantEvent, room: parameters.room) {
                voters.append(voter)
            }
        }
       
        let votesText = answerOption.count == 1 ? VectorL10n.pollTimelineOneVote : VectorL10n.pollTimelineVotesCount(Int(answerOption.count))
        return PollParticipantAnswer.init( name: answerOption.text,
                                                votes: Int(answerOption.count),
                                         visibleVotes: min(Int(answerOption.count), BWIBuildSettings.shared.bwiPollVisibleVotes),
                                            votesText: votesText,
                                           originalId: answerOption.id,
                                               voters: voters)
    }
}

struct PollParticipantPoll : BindableState {
    var name: String
    let voterRows: Int = 2
}
