//
/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import UIKit
import Lottie

extension NSNotification.Name {
    static let cancelLaunchLoadingAnimation = NSNotification.Name(rawValue: "cancelLaunchLoadingAnimation")
}

@objc class BUMLaunchLoadingViewController: UIViewController {

    private var toFrame: CGFloat? = nil
    
    init() {
        self.toFrame = nil
        super.init(nibName: nil, bundle: nil)
    }
    
    init(toFrame: CGFloat) {
        self.toFrame = toFrame
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .white
        
        let animationView = LottieAnimationView(name: "bum_lottie_animation")
        animationView.loopMode = .playOnce
        animationView.contentMode = .scaleAspectFit
        
        if let toFrame = toFrame {
            animationView.play(toFrame: toFrame, loopMode: .loop)
        } else {
            animationView.play()
        }
        view.addSubview(animationView)
        
        animationView.translatesAutoresizingMaskIntoConstraints = false
        animationView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
        animationView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        gestureRecognizer.numberOfTapsRequired = 1
        view.addGestureRecognizer(gestureRecognizer)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        NotificationCenter.default.post(name: .cancelLaunchLoadingAnimation, object: nil, userInfo: nil)
    }

    @objc class func makeView() -> UIView {
        let animationView = LottieAnimationView(name: "bum_lottie_animation")
        animationView.contentMode = .scaleAspectFit
        animationView.play(toFrame: 60, loopMode: .loop)

        return animationView
    }
}
