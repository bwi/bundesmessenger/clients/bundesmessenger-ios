/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation

public extension MXRestClient {
        
    @discardableResult
    func getWellKnown(completion: @escaping (_ jsonResponse: Dictionary<AnyHashable, Any>?, _ error: Error?) -> Void) -> MXHTTPOperation? {
        
        guard let httpClient = httpClient else {
            completion(nil, "HTTPClient not available")
            return nil
        }

        let path = ".well-known/matrix/client"

        return httpClient.request(withMethod: "GET", path: path, parameters: nil, success: { response in
            guard response != nil else {
                completion(nil, "Response empty")
                return
            }
            completion(response, nil)
        }, failure: { (error) in
           completion(nil, error)
        })
    }
    
    @discardableResult
    func getServerCapabilities(completion: @escaping (_ jsonResponse: Dictionary<AnyHashable, Any>?, _ error: Error?) -> Void) -> MXHTTPOperation? {
        
        guard let httpClient = httpClient else {
            completion(nil, "HTTPClient not available")
            return nil
        }

        let path = apiPathPrefix + "/capabilities"

        return httpClient.request(withMethod: "GET", path: path, parameters: nil, success: { response in
            guard response != nil else {
                completion(nil, "Response empty")
                return
            }
            completion(response, nil)
        }, failure: { (error) in
           completion(nil, error)
        })
    }

    @discardableResult
    func getAccountData(forType type: String,
                        completion: @escaping (_ jsonResponse: Dictionary<AnyHashable, Any>?, _ error: Error?) -> Void) -> MXHTTPOperation? {
        guard let userID = credentials.userId else {
            completion(nil, "No user set")
            return nil
        }
        guard let httpClient = httpClient else {
            completion(nil, "HTTPClient not available")
            return nil
        }

        let path = apiPathPrefix + "/user/" + userID + "/account_data/" + type

        return httpClient.request(withMethod: "GET", path: path, parameters: nil, success: { response in
            guard response != nil else {
                completion(nil, "Response empty")
                return
            }
            completion(response, nil)
        }, failure: { (error) in
           completion(nil, error)
        })
    }
    
    @discardableResult
    func getAccountData(forType type: String,
                             success: @escaping (_ jsonResponse: Dictionary<AnyHashable, Any>?) -> Void,
                             failure: @escaping (_ error: Error?) -> Void) -> MXHTTPOperation? {
        
        //let path = String.init(format: "%@/user/%@/account_data/%@", apiPathPrefix, credentials.userId, type)
        
        guard let userID = credentials.userId else {
            return nil
        }
        
        let path = apiPathPrefix + "/user/" + userID + "/account_data/" + type
        
        return httpClient.request(withMethod: "GET", path: path, parameters: nil, success: { response in
            guard response != nil else {
                failure (nil)
                return
            }
            success(response)
        }, failure: { (error) in
           failure(error)
        })
    }
    
    @discardableResult
    func getDowntime(completion: @escaping (_ jsonResponse: Dictionary<AnyHashable, Any>?, _ error: Error?) -> Void) -> MXHTTPOperation? {
        guard let httpClient = httpClient else {
            return nil
        }

        let path = "/_matrix/cmaintenance"

        return httpClient.request(withMethod: "GET", path: path, parameters: nil, success: { response in
            guard response != nil else {
                completion(nil, "Response empty")
                return
            }
            completion(response, nil)
        }, failure: { (error) in
           completion(nil, error)
        })
    }
    
    @discardableResult
    func getKeyBackupVersion(completion: @escaping (_ jsonResponse: Dictionary<AnyHashable, Any>?, _ error: Error?) -> Void) -> MXHTTPOperation? {
        guard let httpClient = httpClient else {
            return nil
        }

        let path = "/_matrix/client/v3/room_keys/version"

        return httpClient.request(withMethod: "GET", path: path, parameters: nil, success: { response in
            guard response != nil else {
                completion(nil, "Response empty")
                return
            }
            completion(response, nil)
        }, failure: { (error) in
           completion(nil, error)
        })
    }
}
