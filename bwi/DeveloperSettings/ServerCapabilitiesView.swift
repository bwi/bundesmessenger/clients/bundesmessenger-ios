//
/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import SwiftUI

struct ServerCapabilitiesView: View {
    @State var jsonString = ""
    var session: MXSession?
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading) {
                HStack {
                    Text(jsonString)
                        .font(.caption)
                        .padding()
                    Spacer()
                }
            }
        }
        .navigationTitle(BWIL10n.bwiSettingsDeveloperCapabilities)
        .navigationBarTitleDisplayMode(.inline)
        .onAppear {
            Task {
                await fetchData()
            }
        }
    }
    
    private func fetchData() async {
        guard let restClient = session?.matrixRestClient else {
            return
        }
        
        let responseDict = await withCheckedContinuation { continuation in
            restClient.getServerCapabilities { jsonResponse, error in
                continuation.resume(returning: jsonResponse)
            }
        }
        await MainActor.run {
            do {
                if let responseDict = responseDict {
                    let data = try JSONSerialization.data(withJSONObject: responseDict as Any, options: [.prettyPrinted])
                    jsonString = String(data: data, encoding: .utf8) ?? ""
                } else {
                    jsonString = ""
                }
            } catch {
                jsonString = ""
            }
        }
    }
}

struct ServerCapabilitiesView_Previews: PreviewProvider {
    static var previews: some View {
        ServerCapabilitiesView()
    }
}
