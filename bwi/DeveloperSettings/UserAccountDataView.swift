//
/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import SwiftUI

fileprivate let availableAccountDataTypes: [String] = [
    "de.bwi.personal_notes_room",
    "de.bwi.should_show_ios_release_notes",
    "de.bwi.notification_times",
    "de.bwi.top_banner_features",
    "de.bwi.notifications",
    "de.bwi.analytics",
    "im.vector.setting.integration_provisioning",
    "im.vector.setting.allowed_widgets"
]

struct UserAccountDataView: View {
    @State var selectedAccountDataType = "de.bwi.personal_notes_room"
    @State var jsonString = ""
    var session: MXSession?
    
    var body: some View {
        Form {
            SwiftUI.Section {
                Picker(BWIL10n.bwiSettingsDeveloperUserAccountDataType, selection: $selectedAccountDataType) {
                    ForEach(availableAccountDataTypes, id:\.self) { type in
                        Text(type).tag(type)
                    }
                }
                .onChange(of: selectedAccountDataType) { newValue in
                    Task {
                        await fetchData(forType: newValue)
                    }
                }
            }
            if !jsonString.isEmpty {
                SwiftUI.Section {
                    Text(jsonString)
                        .font(.caption)
                        .padding()
                }
            }
        }
        .navigationTitle(BWIL10n.bwiSettingsDeveloperUserAccountData)
        .navigationBarTitleDisplayMode(.inline)
        .onAppear {
            Task {
                await fetchData(forType: selectedAccountDataType)
            }
        }
    }
    
    private func fetchData(forType accountDataType: String) async {
        guard let restClient = session?.matrixRestClient else {
            return
        }
        
        let responseDict = await withCheckedContinuation { continuation in
            restClient.getAccountData(forType: accountDataType) { jsonResponse, error in
                continuation.resume(returning: jsonResponse)
            }
        }
        await MainActor.run {
            do {
                if let responseDict = responseDict {
                    print(responseDict)
                    let data = try JSONSerialization.data(withJSONObject: responseDict as Any, options: [.prettyPrinted])
                    jsonString = String(data: data, encoding: .utf8) ?? ""
                } else {
                    jsonString = ""
                }
            } catch {
                jsonString = ""
            }
        }
    }
}

struct UserAccountDataView_Previews: PreviewProvider {
    static var previews: some View {
        UserAccountDataView()
    }
}
