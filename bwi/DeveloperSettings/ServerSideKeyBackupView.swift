//
/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import SwiftUI

struct ServerSideKeyBackupView: View {
    @State var algorithm: String?
    @State var count: Int?
    @State var etag: String?
    @State var version: String?
    var session: MXSession?

    var body: some View {
        Form {
            if let algorithm = algorithm {
                TitleAndValueView(title: NSLocalizedString(BWIL10n.bwiSettingsDeveloperKeyBackupAlgorithm, comment: ""), value: algorithm)
            }
            if let count = count {
                TitleAndValueView(title: NSLocalizedString(BWIL10n.bwiSettingsDeveloperKeyBackupCount, comment: ""), value: "\(count)")
            }
            if let etag = etag {
                TitleAndValueView(title: NSLocalizedString(BWIL10n.bwiSettingsDeveloperKeyBackupEtag, comment: ""), value: etag)
            }
            if let version = version {
                TitleAndValueView(title: NSLocalizedString(BWIL10n.bwiSettingsDeveloperKeyBackupVersion, comment: ""), value: version)
            }
        }
        .navigationTitle(BWIL10n.bwiSettingsDeveloperKeyBackup)
        .navigationBarTitleDisplayMode(.inline)
        .onAppear {
            Task {
                await fetchData()
            }
        }
    }
    
    private func fetchData() async {
        guard let restClient = session?.matrixRestClient else {
            return
        }
        
        let responseDict = await withCheckedContinuation { continuation in
            restClient.getKeyBackupVersion { jsonResponse, error in
                continuation.resume(returning: jsonResponse)
            }
        }
        await MainActor.run {
            if let responseDict = responseDict {
                algorithm = responseDict["algorithm"] as? String
                count = responseDict["count"] as? Int
                etag = responseDict["etag"] as? String
                version = responseDict["version"] as? String
            } else {
                algorithm = nil
                count = nil
                etag = nil
                version = nil
            }
        }
    }

}

struct TitleAndValueView: View {
    var title: String
    var value: String
    
    var body: some View {
        HStack(alignment: .firstTextBaseline) {
            Text(title)
            Spacer()
            Text(value)
                .foregroundColor(.secondary)
        }
    }
}

struct ServerSideKeyBackupView_Previews: PreviewProvider {
    static var previews: some View {
        ServerSideKeyBackupView()
    }
}
