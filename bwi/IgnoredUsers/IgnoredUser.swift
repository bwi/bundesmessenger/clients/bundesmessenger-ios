//
/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation

class IgnoredUser: ObservableObject, Identifiable {
    let id = UUID()
    
    let displayName: String
    let matrixId: String
    let avatar: String
    
    init( displayName: String, matrixId: String, avatar: String) {
        self.displayName = displayName
        self.matrixId = matrixId
        self.avatar = avatar
    }
    
    func avatarImage() -> UIImage {
        if let cachePath = MXMediaManager.thumbnailCachePath(forMatrixContentURI: self.avatar, andType: "image/jpeg", inFolder: nil, toFitViewSize: CGSize(width: 12, height: 12), with: MXThumbnailingMethodCrop) {
            if let image = MXMediaManager.loadThroughCache(withFilePath: cachePath) {
                return image
            }
        }
        return AvatarGenerator.generateAvatar(forMatrixItem: self.matrixId, withDisplayName: self.displayName)
    }
}
