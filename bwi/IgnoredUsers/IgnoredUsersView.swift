//
/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import SwiftUI

/// Helper  class for making our SwiftUI view available to ObjectiveC
@objcMembers class IgnoredUsersViewController: NSObject {

    class func makeViewController(session: MXSession) -> UIViewController {
        return UIHostingController(rootView: IgnoredUsersView(session: session))
    }
}

struct IgnoredUsersView: View {
        
    @Environment(\.theme) private var theme
    
    var session: MXSession?
    var ignoredUsers: [IgnoredUser]
    
    @State private var appearanceSavedColor: UIColor?
    
    init(session: MXSession?) {
        self.session = session
        ignoredUsers = []
        
        if let session = session {
            for matrixId in session.ignoredUsers {
                var avatar = ""
                var displayName = ""
                if let mxUser = session.user(withUserId: matrixId) {
                    displayName = mxUser.displayName ?? ""
                    avatar = mxUser.avatarUrl ?? ""
                }

                ignoredUsers.append(IgnoredUser(displayName: displayName, matrixId: matrixId, avatar: avatar))
            }
        }
    }
    
    var body: some View {
        ZStack {
            theme.colors.system
                .ignoresSafeArea()
            List(ignoredUsers) { user in
                IgnoredUserView(session:session, user: user)
            }
            .onAppear {
                // Set the default to clear
                appearanceSavedColor = UITableView.appearance().backgroundColor
                UITableView.appearance().backgroundColor = .clear
            }
            .onDisappear() {
                if let appearanceSavedColor = appearanceSavedColor {
                    UITableView.appearance().backgroundColor = appearanceSavedColor
                }
            }
        }
        .navigationTitle(BWIL10n.bwiSettingsIgnoredUsersText)
        .navigationBarTitleDisplayMode(.inline)
    }
}

fileprivate struct IgnoredUserView: View {
    @State private var showingAlert = false

    let session: MXSession?
    let user: IgnoredUser
    
    @Environment(\.theme) private var theme
    
    var body: some View {
        Button (action: {
            showingAlert = true
        }) {
            HStack {
                Image(uiImage: user.avatarImage())
                    .clipShape(Circle())
                VStack(alignment: .leading, spacing: 6) {
                    Text(user.displayName)
                        .font(Font.body.bold())
                        .foregroundColor(theme.colors.primaryContent)
                    
                    Text(user.matrixId)
                        .foregroundColor(theme.colors.secondaryContent)
                }
            }
        }
        .listRowBackground(theme.colors.background)
        .alert(isPresented:$showingAlert) {
            Alert(
                title: Text(VectorL10n.settingsUnignoreUser(self.user.displayName)),
                message: Text(BWIL10n.settingsUnignoreUserMessage),
                primaryButton: .default(Text(VectorL10n.yes)) {
                    unignoreUser(self.session, self.user)
                },
                secondaryButton: .cancel()
            )
        }
    }
}

struct IgnoredUsersView_Previews: PreviewProvider {
    static var previews: some View {
        IgnoredUsersView(session: nil)
    }
}


fileprivate func unignoreUser(_ session:MXSession?,_ user:IgnoredUser) {
    if let session = session {
        session.unIgnore(users: [user.matrixId], completion: { response in
            
        })
    }
}

/*
 MXSession* session = self.mainSession;

 NSString *ignoredUserId = session.ignoredUsers[row];

 if (ignoredUserId)
 {
     [currentAlert dismissViewControllerAnimated:NO completion:nil];

     __weak typeof(self) weakSelf = self;
     
     UIAlertController *unignorePrompt = [UIAlertController alertControllerWithTitle:[VectorL10n settingsUnignoreUser:ignoredUserId] message:nil preferredStyle:UIAlertControllerStyleAlert];

     [unignorePrompt addAction:[UIAlertAction actionWithTitle:[VectorL10n yes]
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action) {
                                                        
                                                        if (weakSelf)
                                                        {
                                                            typeof(self) self = weakSelf;
                                                            self->currentAlert = nil;
                                                            
                                                            MXSession* session = self.mainSession;
                                                            
                                                            // Remove the member from the ignored user list
                                                            [self startActivityIndicator];
                                                            [session unIgnoreUsers:@[ignoredUserId] success:^{
                                                                
                                                                [self stopActivityIndicator];
                                                                
                                                            } failure:^(NSError *error) {
                                                                
                                                                [self stopActivityIndicator];
                                                                
                                                                MXLogDebug(@"[SettingsViewController] Unignore %@ failed", ignoredUserId);
                                                                
                                                                NSString *myUserId = session.myUser.userId;
                                                                [[NSNotificationCenter defaultCenter] postNotificationName:kMXKErrorNotification object:error userInfo:myUserId ? @{kMXKErrorUserIdKey: myUserId} : nil];
                                                                
                                                            }];
                                                        }
                                                        
                                                    }]];
     
     [unignorePrompt addAction:[UIAlertAction actionWithTitle:[VectorL10n no]
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action) {
                                                        
                                                        if (weakSelf)
                                                        {
                                                            typeof(self) self = weakSelf;
                                                            self->currentAlert = nil;
                                                        }
                                                        
                                                    }]];
     
     [unignorePrompt mxk_setAccessibilityIdentifier: @"SettingsVCUnignoreAlert"];
     [self presentViewController:unignorePrompt animated:YES completion:nil];
     currentAlert = unignorePrompt;
 }
 */
