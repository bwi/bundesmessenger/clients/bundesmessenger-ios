//
/*
 * Copyright (c) 2023 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import SwiftUI

@objcMembers class AccessibilityDeclarationViewController: NSObject {

    @available(iOS 14.0, *)
    class func makeViewController() -> UIViewController {
        var accessibilityDeclarationFilePath: URL? = nil
        if !BWIBuildSettings.shared.accessibilityDeclarationFileDe.isEmpty && Bundle.main.preferredLocalizations[0].elementsEqual("de") {
            accessibilityDeclarationFilePath = Bundle.main.url(forResource: BWIBuildSettings.shared.accessibilityDeclarationFileDe, withExtension: "md")
        } else if !BWIBuildSettings.shared.accessibilityDeclarationFileEn.isEmpty {
            accessibilityDeclarationFilePath = Bundle.main.url(forResource: BWIBuildSettings.shared.accessibilityDeclarationFileEn, withExtension: "md")
        }
        
        if let url = accessibilityDeclarationFilePath {
            guard let string = try? String(contentsOf: url) else {
                return UIHostingController(rootView: EmptyView())
            }
            let vc = UIHostingController(rootView: MarkDownView(markdownString: string))
            vc.title = BWIL10n.bwiAccessibilityDeclarationButtonTitle
            vc.view.backgroundColor = ThemeService.shared().theme.backgroundColor
            vc.navigationItem.largeTitleDisplayMode = .never
            return vc
        } else {
            return UIHostingController(rootView: EmptyView())
        }
    }
}


