//
/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import SwiftUI

struct AuthenticationServerSelectionQRCodeScanner: View {
    @Environment(\.presentationMode) var presentationMode
    @Binding var qrCode: String
    @State var scanCompleted = false

    var body: some View {
        NavigationView {
            ScannerView(qrCode: $qrCode, scanCompleted: $scanCompleted)
                .navigationTitle(BWIL10n.authenticationServerSelectionScanCodeButtonTitle)
                .navigationBarTitleDisplayMode(.inline)
                .toolbar {
                    ToolbarItem(placement: .navigationBarLeading) {
                        Button {
                            presentationMode.wrappedValue.dismiss()
                        } label: {
                            Text(VectorL10n.close)
                                .foregroundColor(Color(ThemeService.shared().theme.tintColor))
                        }
                    }
                }
        }
        .onChange(of: scanCompleted) { newValue in
            if newValue {
                presentationMode.wrappedValue.dismiss()
            }
        }
    }
}
