//
/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import SwiftUI
import UIKit
import CoreImage.CIFilterBuiltins

class MyQRCodeViewController: NSObject {
    
    @objc static func createFromSwiftUIView(matrixID: String, displayName: String, avatarUIImage: UIImage) -> UIViewController {
        return UIHostingController(rootView: MyQRCodeView(matrixID: matrixID, displayName: displayName, avatarUIImage: avatarUIImage))
  }
    
    @objc static func createFromSwiftUIView(matrixID: String, displayName: String) -> UIViewController {
        return UIHostingController(rootView: MyQRCodeView(matrixID: matrixID, displayName: displayName))
  }

}

struct MyQRCodeView: View {
    let avatarSize = 80.0
    var matrixID: String
    var displayName: String
    var avatarUIImage: UIImage?

    var avatarImage: some View {
        if let avatarUIImage = avatarUIImage {
            return Image(uiImage: avatarUIImage)
                .resizable()
                .frame(width: avatarSize, height: avatarSize)
                .clipShape(Circle())
        } else {
            return Image(uiImage: generateAvatarUIImage())
                .resizable()
                .frame(width: avatarSize, height: avatarSize)
                .clipShape(Circle())
        }
    }
    
    var qrCode: some View {
        return Image(uiImage: generateQRCodeUIImage())
            .interpolation(.none)
            .resizable()
            .aspectRatio(1, contentMode: .fit)
            .padding(8)
            .frame(maxWidth: 200, maxHeight: 200)
            .background(Color.white)
            .cornerRadius(8)
    }
    
    var grayContainer: some View {
        qrCode
            .padding(.top, 120)
            .padding(.bottom, 30)
            .padding(.horizontal, 50)
            .background(Color(ThemeService.shared().theme.colors.system))
            .cornerRadius(8)
    }
    
    var content: some View {
        ZStack(alignment: .top) {
            grayContainer
                .padding(.top, avatarSize / 2)
            VStack {
                avatarImage
                    .padding(.bottom, 2)
                Text(displayName)
                    .lineLimit(1)
                    .font(.system(size: 17, weight: .bold))
                    .foregroundColor(Color(ThemeService.shared().theme.colors.primaryContent))
                    .padding(.bottom, 2)
                Text(matrixID)
                    .lineLimit(1)
                    .font(.system(size: 15))
                    .foregroundColor(Color(ThemeService.shared().theme.colors.secondaryContent))
                    .padding(.bottom, 20)
            }
            .padding(.horizontal, 10)
        }
    }
    
    var footer: some View {
        Text(BWIL10n.showMyQrScreenMessage(AppInfo.current.displayName))
            .font(.system(size: 15))
            .foregroundColor(BWIBuildSettings.shared.useNewBumColors ? Color(ThemeService.shared().theme.tintColor) : Color(ThemeService.shared().theme.colors.primaryContent))   // bwi: 4769
            .multilineTextAlignment(.center)
    }
    
    var body: some View {
        ScrollView {
            VStack(spacing: 20) {
                content
                footer
            }
            .padding(.vertical, 40)
            .padding(.horizontal, 40)
            .navigationBarTitleDisplayMode(.inline)
            .navigationTitle(BWIL10n.showMyQrScreenTitle)
        }
    }
               
    private func generateAvatarUIImage() -> UIImage {
        return AvatarGenerator.generateAvatar(forMatrixItem: matrixID, withDisplayName: displayName)
    }
    private func generateQRCodeUIImage() -> UIImage {
        let context = CIContext()
        let filter = CIFilter.qrCodeGenerator()

        if let permalink = MXTools.permalinkToUser(withUserId: matrixID) {
            filter.message = Data(permalink.utf8)
            
            if let outputImage = filter.outputImage {
                if let cgImage = context.createCGImage(outputImage, from: outputImage.extent) {
                    return UIImage(cgImage: cgImage)
                }
            }
        }
        return UIImage()
    }
                     
}

struct MyQRCodeView_Previews: PreviewProvider {
    static var previews: some View {
        MyQRCodeView(matrixID: "User ID", displayName: "Display Name")
    }
}
