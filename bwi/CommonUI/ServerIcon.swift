//
/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import SwiftUI

struct ServerIcon: View {
    @Environment(\.theme) private var theme
    
    let image: ImageAsset?
    let size: CGFloat
    
    var body: some View {
        Image((image ?? getDefaultServerIcon()).name)
            .resizable()
            .frame(width: size, height: size)
            .background(BWIBuildSettings.shared.bwiLoginFlowLayout ? Color.clear : Color.white)
            .cornerRadius(20)
            .accessibilityHidden(true)
    }
    
    // get app specific ServerIcon
    func getDefaultServerIcon() -> ImageAsset {
        return BWIBuildSettings.shared.bwiLoginFlowLayout ? Asset.Images.launchScreenLogo : Asset.SharedImages.loginFlowLogo
    }
}

struct ServerIcon_Previews: PreviewProvider {
    static var previews: some View {
        ServerIcon(image: Asset.Images.authenticationEmailIcon, size: 72)
    }
}
