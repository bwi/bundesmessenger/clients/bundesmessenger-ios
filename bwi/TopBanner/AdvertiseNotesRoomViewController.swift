/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation
import UIKit

class AdvertiseNotesRoomViewController: UIViewController {
    
    private weak var session: MXSession?
    
    @objc
    init(session: MXSession?) {
        self.session = session
        super.init(nibName: "AdvertiseNotesRoomViewController", bundle: .main)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(userTappedOnView))
        view.addGestureRecognizer(tap)
    }
    
    // MARK: - Gesture Recognizer Handler
    
    @objc
    private func userTappedOnView() {
        NotificationCenter.default.post(name: .bwiHideTopBanner, object: self, userInfo: ["type" : "notice_room"])
        
        // if the notes room is enabled in the app setting then lets show it
        if PersonalNotesSettings().personalNotesVisible {
            if let session = session {
                PersonalNotesDefaultService(mxSession: session).fetchRoomID { (roomID, error) in
                    if let roomID = roomID {
                        let legacyAppDelegate: LegacyAppDelegate = AppDelegate.theDelegate()
                        legacyAppDelegate.showRoom(roomID, andEventId: nil, withMatrixSession: session)
                    }
                }
            }
        }
    }
    
}
