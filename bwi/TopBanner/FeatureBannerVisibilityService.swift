//
/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation
/*
"should_show_android_release_notes" : [ {
    "version" : "1.17.0",
    "show_hint" : false
  } ]
 */

@objcMembers class FeatureBannerVisibilityService : NSObject {
    
    private enum AccountDataTypes {
        static let featureVisibility = "de.bwi.should_show_ios_release_notes"
    }
    
    private let session:MXSession
    
    init(mxSession: MXSession) {
        self.session = mxSession
    }
    
    func markAsRead( version: String ) -> MXHTTPOperation? {
        // Update only the "widgets" field in the account data
        var featureDict = self.session.accountData.accountData(forEventType: AccountDataTypes.featureVisibility) ?? [:]
        
        featureDict[version] = false

        return session.setAccountData(featureDict, forType: AccountDataTypes.featureVisibility, success: nil, failure: nil)
    }
    
    func markAsUnread( version: String ) -> MXHTTPOperation? {
        // Update only the "widgets" field in the account data
        var featureDict = self.session.accountData.accountData(forEventType: AccountDataTypes.featureVisibility) ?? [:]
        
        featureDict[version] = true

        return session.setAccountData(featureDict, forType: AccountDataTypes.featureVisibility, success: {
            // bwi: update tableview
            NotificationCenter.default.post(name: .bwiMarkTopBannerAsUnRead, object: nil, userInfo: ["type" : "feature_banner"])
        }, failure: nil)
    }
    
    func isUnread( version: String, completion: @escaping (_ unread : Bool) -> Void)  {
        session.matrixRestClient.getAccountData(forType: AccountDataTypes.featureVisibility ) { (jsonResponse, error) in
            
            guard let featureDict = jsonResponse as? [String:Any] else {
                completion(true)
                return
            }
            guard let unread = featureDict[version] as? Bool else {
                completion(true)
                return
            }
            completion(unread)
        }
    }
}
