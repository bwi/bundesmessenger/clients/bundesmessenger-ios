//
/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation
import UIKit

@objcMembers class FeatureBannerViewController: UIViewController {
    
    private let session: MXSession
    private let version: String
    
    @objc
    init(session: MXSession, version: String) {
        self.session = session
        self.version = version
        super.init(nibName: "FeatureBannerViewController", bundle: .main)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(userTappedOnView))
        view.addGestureRecognizer(tap)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        _ = FeatureBannerVisibilityService(mxSession: self.session).markAsRead(version: self.version)
        NotificationCenter.default.post(name: .bwiMarkTopBannerAsRead, object: self, userInfo: ["type" : "feature_banner"])
    }
    
    // MARK: - Gesture Recognizer Handler
    
    @objc
    private func userTappedOnView() {
        //NotificationCenter.default.post(name: .bwiHideTopBanner, object: self, userInfo: ["type" : "feature_banner"])
        
        let htmlFile = Bundle.main.path(forResource: BWIBuildSettings.shared.newFeaturesHTML, ofType: "html")
        if let webviewController = WebViewViewController(localHTMLFile: htmlFile) {
            webviewController.title = BWIL10n.bwiSettingsNewFeaturesHeader
            
            self.present(webviewController, animated: true, completion: { () -> Void in
                NotificationCenter.default.post(name: .bwiHideTopBanner, object: self, userInfo: ["type" : "feature_banner"])
            })
        }
    }
}
