/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation
import UIKit

class TopBannerViewController: UIPageViewController {
    
    @objc private(set) var advertiseViewControllers: [UIViewController] = []
    private var currentIndex = 0
    private var timer: Timer?

    @objc
    init(controllers: [UIViewController]) {
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        self.advertiseViewControllers = controllers
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setViewControllers([advertiseViewControllers[currentIndex]], direction: .forward, animated: false, completion: nil)
        
        // add a swipe up gesture recognizer to hide the banner
        let swipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(onSwipeUp))
        swipeGestureRecognizer.direction = .up
        self.view.addGestureRecognizer(swipeGestureRecognizer)
    }
    
    override func didMove(toParent parent: UIViewController?) {
        if parent == nil {
            timer?.invalidate()
            timer = nil
        } else {
            timer?.invalidate()
            timer = Timer.scheduledTimer(timeInterval: 7.0, target: self, selector: #selector(advertiseNextFeature), userInfo: nil, repeats: true)
        }
    }
    
    @objc func removeAdvertiseViewControllers() {
        self.advertiseViewControllers.removeAll()
    }
    
    // MARK: - Timer Callback
    
    @objc
    private func advertiseNextFeature() {
        guard advertiseViewControllers.count > 1 else {
            return
        }
        
        currentIndex += 1
        if currentIndex >= advertiseViewControllers.count {
            currentIndex = 0
        }
        setViewControllers([advertiseViewControllers[currentIndex]], direction: .forward, animated: true, completion: nil)
    }
    
    @objc
    private func onSwipeUp() {
        NotificationCenter.default.post(name: .bwiHideTopBanner, object: nil)
    }
}

extension TopBannerViewController: UIPageViewControllerDelegate {
}

extension NSNotification.Name {
    static let bwiHideTopBanner = Notification.Name("de.bwi.messenger.hide_top_banner")
    static let bwiMarkTopBannerAsRead = Notification.Name("de.bwi.messenger.mark_top_banner_as_read")
    static let bwiMarkTopBannerAsUnRead = Notification.Name("de.bwi.messenger.mark_top_banner_as_unread")
}
