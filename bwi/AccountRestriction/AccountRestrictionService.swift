//
/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation

@objcMembers class AccountRestrictionService : NSObject {
    
    private enum AccountDataTypes {
        static let restrictions = "m.bwi.account_restrictions"
    }
    
    let session: MXSession
    private lazy var serializationService: SerializationServiceType = SerializationService()
    
    init(mxSession: MXSession) {
        self.session = mxSession
    }
    
    func isRoomAccessRestriction() -> Bool {
        guard let restrictionsDict = session.accountData.accountData(forEventType: AccountDataTypes.restrictions) as? [String: Any] else {
            return false
        }
        do {
            let restriction: AccountRestriction = try serializationService.deserialize(restrictionsDict)
            
            return restriction.roomAccess
        } catch {
            
        }
        
        return false
    }
    
    func isAddressListRestriction() -> Bool {
        guard let restrictionsDict = session.accountData.accountData(forEventType: AccountDataTypes.restrictions) as? [String: Any] else {
            return false
        }
        do {
            let restriction: AccountRestriction = try serializationService.deserialize(restrictionsDict)
            
            return restriction.addressListAccess
        } catch {
            return false
        }
    }
    
    func updateRestriction(roomAccess: Bool, addressListAccess: Bool) {
        var restrictionDict = session.accountData.accountData(forEventType: AccountDataTypes.restrictions) ?? [:]
        
        restrictionDict[AccountRestriction.CodingKeys.roomAccess.rawValue] = roomAccess
        restrictionDict[AccountRestriction.CodingKeys.addressListAccess.rawValue] = addressListAccess

        session.setAccountData(restrictionDict, forType: AccountDataTypes.restrictions, success: nil, failure: nil)
    }
}
