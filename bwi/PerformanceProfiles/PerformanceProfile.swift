//
/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation

@objcMembers class PerformanceProfile : NSObject {
    private var startTime: DispatchTime?
    private var trackingThreshold: TimeInterval
    private var timeInterval: TimeInterval
    private var aborted = false
    
    init(threshold: TimeInterval) {
        self.trackingThreshold = threshold
        self.timeInterval = .zero
        super.init()
    }
    
    func startMeasurement() {
        startTime = DispatchTime.now()
    }
    
    func stopMeasurement() {
        guard let startTime = startTime else {
            return
        }
        let endTime = DispatchTime.now()
        
        let nanoTime = endTime.uptimeNanoseconds - startTime.uptimeNanoseconds
        timeInterval = Double(nanoTime) / 1_000_000_000
    }
    
    func abortMeasurement() {
        aborted = true
    }
    
    func isLogable() -> Bool {
        return ((timeInterval > trackingThreshold) && !aborted)
    }
    
    func log2Analytics(users: Int, devices: Int) {
        if isLogable() {
            BWIAnalytics.sharedTracker.trackSlowMessage(dimension: BWIAnalyticsHelper.shared.dimensionForDeviceCount(devices), value: Int(timeInterval*1000))
        }
    }
}
