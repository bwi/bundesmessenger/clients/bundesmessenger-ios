//
/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation

final class RoomRollsAndRightsService: RoomRollsAndRightsServiceType {
    
    typealias Completion = () -> Void
    
    // MARK: - Properties
    
    // MARK: Private
    
    private let room: MXRoom
    
//    private var notificationCenterDidUpdateObserver: NSObjectProtocol?
//    private var notificationCenterDidFailObserver: NSObjectProtocol?
//
//    private var observers: [ObjectIdentifier] = []
    
    // MARK: Public
    
//    var notificationState: RoomNotificationState {
//        room.notificationState
//    }

    // MARK: - Setup
    
    init(room: MXRoom) {
        self.room = room
    }
    
    func getRoomPowerLevels() -> RoomRollsAndRightsViewState? {
        var state: RoomRollsAndRightsViewState?
        room.state { roomState in
            if let roomPowerlevels = roomState?.powerLevels  {
                if let events = roomPowerlevels.events as? [String : Int] {
                    if let roomPowerLevelName = events["m.room.name"] {
                        state = RoomRollsAndRightsViewState(powerLevelGeneral: roomPowerLevelName,
                                                            powerLevelSend: roomPowerlevels.eventsDefault,
                                                            powerLevelRedact: roomPowerlevels.redact,
                                                            powerLevelInvite: roomPowerlevels.invite,
                                                            powerLevelBan: roomPowerlevels.ban)
                    }
                }
            }
        }
        return state
    }
    
    func setRoomPowerLevels(state: RoomRollsAndRightsViewState) {
        room.sendPowerLevelGeneral(state.powerLevelGeneral,
                                   powerLevelInvite: state.powerLevelInvite,
                                   powerLevelBan: state.powerLevelBan,
                                   powerLevelSend: state.powerLevelSend,
                                   powerLevelRedact: state.powerLevelRedact) {
            
        } failure: { _ in
        }
    }
    
    func isCurrentUserAdmin() -> Bool {
        var admin = false
        
        self.room.state { (state) in
            let userID = self.room.mxSession.myUser.userId
            let powerLevels = state?.powerLevels
            let userPowerLevel = powerLevels?.powerLevelOfUser(withUserID: userID)
            let roomPowerLevel = RoomPowerLevelHelper.roomPowerLevel(from: userPowerLevel ?? RoomPowerLevel.user.rawValue)
            
            if roomPowerLevel == RoomPowerLevel.admin {
                admin = true
            }
        }
        return admin
    }

}
