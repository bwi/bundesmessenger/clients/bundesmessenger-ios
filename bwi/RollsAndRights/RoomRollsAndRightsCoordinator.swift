//
/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation
import UIKit

final class RoomRollsAndRightsCoordinator: RoomRollsAndRightsCoordinatorType {
    
    // MARK: - Properties
    
    // MARK: Private
    private var roomRollsAndRightsViewModel: RoomRollsAndRightsViewModelType
    private let roomRollsAndRightsViewController: RoomRollsAndRightsViewController
    
    // MARK: Public

    // Must be used only internally
    var childCoordinators: [Coordinator] = []
    
    weak var delegate: RoomRollsAndRightsCoordinatorDelegate?
    
    // MARK: - Setup
    
    init(room: MXRoom) {
        let repository = RoomRollsAndRightsService(room: room)
        
        let roomRollsAndRightsViewModel = RoomRollsAndRightsViewModel(roomRollsAndRightsService: repository)
        let roomRollsAndRightsViewController = RoomRollsAndRightsViewController.instantiate(with: roomRollsAndRightsViewModel)
        self.roomRollsAndRightsViewModel = roomRollsAndRightsViewModel
        self.roomRollsAndRightsViewController = roomRollsAndRightsViewController
    }

    // MARK: - Public methods
    
    func start() {
        self.roomRollsAndRightsViewModel.coordinatorDelegate = self
    }
    
    func toPresentable() -> UIViewController {
        return self.roomRollsAndRightsViewController
    }
}

// MARK: - RoomNotificationSettingsViewModelCoordinatorDelegate
extension RoomRollsAndRightsCoordinator: RoomRollsAndRightsViewModelCoordinatorDelegate {
    
    func roomRollsAndRightsViewModelDidComplete(_ viewModel: RoomRollsAndRightsViewModelType) {
        self.delegate?.roomRollsAndRightsCoordinatorDidComplete(self)
    }
    
    func roomRollsAndRightsViewModelDidCancel(_ viewModel: RoomRollsAndRightsViewModelType) {
        self.delegate?.roomRollsAndRightsCoordinatorDidCancel(self)
    }
}
