//
/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation

class RoomRollsAndRightsViewModel: RoomRollsAndRightsViewModelType {
        
    // MARK: Private
    
    private let roomRollsAndRightsService: RoomRollsAndRightsServiceType
    private var state: RoomRollsAndRightsViewState {
        willSet {
            update(viewState: newValue)
        }
    }

    // MARK: Public

    weak var viewDelegate: RoomRollsAndRightsViewModelViewDelegate?
    weak var coordinatorDelegate: RoomRollsAndRightsViewModelCoordinatorDelegate?
    
    // MARK: - Setup
    
    init(roomRollsAndRightsService: RoomRollsAndRightsServiceType) {
        self.roomRollsAndRightsService = roomRollsAndRightsService
        self.state = RoomRollsAndRightsViewState(powerLevelGeneral: 0,
                                                 powerLevelSend: 0,
                                                 powerLevelRedact: 0,
                                                 powerLevelInvite: 0,
                                                 powerLevelBan: 0)
    }
    
    // MARK: - Public

    func process(viewAction: RoomRollsAndRightsViewAction) {
        switch viewAction {
        case .reset:
            reset()
        case .load:
            if let viewState = roomRollsAndRightsService.getRoomPowerLevels() {
                self.state = viewState
            }
        case .save:
            roomRollsAndRightsService.setRoomPowerLevels(state: self.state)
            coordinatorDelegate?.roomRollsAndRightsViewModelDidComplete(self)
            break
        case .cancel:
            coordinatorDelegate?.roomRollsAndRightsViewModelDidCancel(self)
        }
    }
    
    // MARK: - Private
    
    private func reset() {
        self.state.powerLevelGeneral = 50
        self.state.powerLevelInvite = 50
        self.state.powerLevelBan = 50
        self.state.powerLevelSend = 0
        self.state.powerLevelRedact = 50
        self.viewDelegate?.roomRollsAndRightsViewModel(self, didUpdateViewState: self.state)
    }
    
    private func update(viewState: RoomRollsAndRightsViewState) {
        self.viewDelegate?.roomRollsAndRightsViewModel(self, didUpdateViewState: viewState)
    }
    
    func isCurrentUserAdmin() -> Bool {
        return roomRollsAndRightsService.isCurrentUserAdmin()
    }
    
}

extension RoomRollsAndRightsViewModel: RoomRollsAndRightsPickerViewControllerDelegate {
    
    func powerLevelsDidChange(controller: RoomRollsAndRightsPickerViewController, viewState: RoomRollsAndRightsViewState) {
        self.state = viewState
    }
    
}
