//
/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation

protocol RoomRollsAndRightsViewModelViewDelegate: AnyObject {
    func roomRollsAndRightsViewModel(_ viewModel: RoomRollsAndRightsViewModelType, didUpdateViewState viewState: RoomRollsAndRightsViewState)
}

protocol RoomRollsAndRightsViewModelCoordinatorDelegate: AnyObject {
    func roomRollsAndRightsViewModelDidComplete(_ viewModel: RoomRollsAndRightsViewModelType)
    func roomRollsAndRightsViewModelDidCancel(_ viewModel: RoomRollsAndRightsViewModelType)
}

protocol RoomRollsAndRightsViewModelType {
        
    var viewDelegate: RoomRollsAndRightsViewModelViewDelegate? { get set }
    var coordinatorDelegate: RoomRollsAndRightsViewModelCoordinatorDelegate? { get set }
    
    func process(viewAction: RoomRollsAndRightsViewAction)
    func isCurrentUserAdmin() -> Bool
}
