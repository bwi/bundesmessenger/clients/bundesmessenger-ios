//
/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#import "MXRoom+sendRoomPowerLevels.h"

@implementation MXRoom (BWI)


- (MXHTTPOperation*)sendRoomPowerLevelGeneral:(NSInteger)powerLevelGeneral
                             powerLevelInvite:(NSInteger)powerLevelInvite
                                powerLevelBan:(NSInteger)powerLevelBan
                               powerLevelSend:(NSInteger)powerLevelSend
                             powerLevelRedact:(NSInteger)powerLevelRedact
                                      success:(void (^)(void))success
                                      failure:(void (^)(NSError *))failure
{
    // Create an empty operation that will be mutated later
    MXHTTPOperation *operation = [[MXHTTPOperation alloc] init];

    MXWeakify(self);
    [self state:^(MXRoomState *roomState) {
        MXStrongifyAndReturnIfNil(self);

        // To set this new value, we have to take the current powerLevels content,
        // Update it with expected values and send it to the home server.
        NSMutableDictionary *eventContent = [NSMutableDictionary dictionaryWithDictionary:roomState.powerLevels.JSONDictionary];

        NSMutableDictionary *eventPowerLevels = [NSMutableDictionary dictionaryWithDictionary:eventContent[@"events"]];
        
        // Element defined event power levels
        eventPowerLevels[@"m.room.name"] = [NSNumber numberWithInteger:powerLevelGeneral];
        eventPowerLevels[@"m.room.topic"] = [NSNumber numberWithInteger:powerLevelGeneral];
        eventPowerLevels[@"m.room.avatar"] = [NSNumber numberWithInteger:powerLevelGeneral];
        
        // bwi: 5035 - huddle power levels should stay at power level 0 (all users)
        eventPowerLevels[@"m.call"] = [NSNumber numberWithInteger:0];
        eventPowerLevels[@"m.call.member"] = [NSNumber numberWithInteger:0];
        eventPowerLevels[@"org.matrix.msc3401.call"] = [NSNumber numberWithInteger:0];
        eventPowerLevels[@"org.matrix.msc3401.call.member"] = [NSNumber numberWithInteger:0];

        eventContent[@"events"] = eventPowerLevels;

        eventContent[@"invite"] = [NSNumber numberWithInteger:powerLevelInvite];
        eventContent[@"kick"] = [NSNumber numberWithInteger:powerLevelBan];
        eventContent[@"ban"] = [NSNumber numberWithInteger:powerLevelBan];
        eventContent[@"redact"] = [NSNumber numberWithInteger:powerLevelRedact];
        eventContent[@"events_default"] = [NSNumber numberWithInteger:powerLevelSend];

        // Make the request to the HS
        MXHTTPOperation *_operation = [self sendStateEventOfType:kMXEventTypeStringRoomPowerLevels content:eventContent stateKey:nil success:^(NSString *eventId) {
            success();
        } failure:failure];
        
        [operation mutateTo:_operation];
    }];

    return operation;
}

@end
