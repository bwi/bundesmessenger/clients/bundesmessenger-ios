//
/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import XCTest
@testable import Element

class LoginProtectionTests: XCTestCase {

    let hashes = ["34f2dfdb69edeef64ae9f53cea21c7e27db19566d440174c5bc3949d87ae90f6",
                  "04feebbb6cc530f26db673ee7d781c57870cfc7c6d1814d63dc703a3da522619"
    ]

    func testValidURL() throws {
        let service = LoginProtectionService()
        service.hashes = hashes
        
        XCTAssertTrue(service.isValid("https://www.wellbehaved.de"))
    }

    func testInvalidURL() throws {
        let service = LoginProtectionService()
        service.hashes = hashes
         
        XCTAssertFalse(service.isValid("https://www.unknown.org"))
    }
    
    func testSimpleURL() throws {
        let service = LoginProtectionService()
        service.hashes = hashes
         
        XCTAssertTrue(service.isValid("www.simple.com"))
    }
    
    func testMalformatedURL() throws {
        let service = LoginProtectionService()
        service.hashes = hashes
         
        XCTAssertFalse(service.isValid("ur%%l@blalalal"))
    }

    func testNoHashlist() throws {
        let service = LoginProtectionService()
         
        XCTAssertFalse(service.isValid("https://www.wellbehaved.de"))
    }
}
