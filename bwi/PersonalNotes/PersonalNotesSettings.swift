/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation
import KeychainAccess

@objcMembers
final class  PersonalNotesSettings : NSObject {

    private enum StoreKeys {
        static let visibilityKey = "PersonalNotesVisible"
    }
    
    private struct BwiSettingsConstants {
        static let bwiSettingsKeychainService: String = BuildSettings.baseBundleIdentifier + ".bwi-personalnotes-service"
    }
    
    private let vault: KeyValueVault
    
    override init() {
        vault = KeychainVault(Keychain(service: BwiSettingsConstants.bwiSettingsKeychainService,
                                                     accessGroup: BuildSettings.keychainAccessGroup))
        super.init()
    }
    
    func reset() {
        do {
            try vault.removeObject(forKey: StoreKeys.visibilityKey)
        } catch let error {
            NSLog("[PersonalNotes] Error when removing objects: \(error)")
        }
        
    }
    
    var personalNotesVisible : Bool {
        get {
            do {
                return try vault.bool(forKey: StoreKeys.visibilityKey) ?? true
            } catch {
                return true
            }
        }
        set {
            do {
                try vault.set(newValue, forKey: StoreKeys.visibilityKey)
            } catch let error {
                NSLog("[PersonalNotes] Error when storing rest time enabled to the vault: \(error)")
            }
        }
    }
}
