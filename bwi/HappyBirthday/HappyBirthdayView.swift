//
/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation
import SwiftUI
import StoreKit

/// Helper  class for making our SwiftUI view available to ObjectiveC
@objcMembers class HappyBirthdayViewController: NSObject {

    class func makeViewController() -> UIViewController {
        return UIHostingController(rootView: HappyBirthdayView())
    }
}

struct HappyBirthdayView: View {
    var body: some View {
        ZStack(alignment: .center) {
            HappyBirthdayCloseButton()
            HappyBirthdayBody()
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(Color(ThemeService.shared().theme.backgroundColor))
        .ignoresSafeArea()
    }
}

struct HappyBirthdayCloseButton: View {
    @Environment(\.presentationMode) var presentationMode

    var body: some View {
        VStack {
            HStack() {
                Spacer()
                Button(action: { presentationMode.wrappedValue.dismiss() }) {
                    Text("HAPPY_BIRTHDAY_BUTTON_DISMISS")
                        .font(.headline)
                        .foregroundColor(Color(ThemeService.shared().theme.tintColor))
                        .padding(20)
                }
            }
            .padding(.top, 50)
            Spacer()
        }
    }
}

struct HappyBirthdayBody: View {
    @Environment(\.presentationMode) var presentationMode

    var body: some View {
        VStack(spacing: 40) {
            VStack {
                Text("HAPPY_BIRTHDAY")
            }
            .font(.title)
            .foregroundColor(Color(ThemeService.shared().theme.textPrimaryColor))

            Image("birthday_cake")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(height: 200)

            Text("HAPPY_BIRTHDAY_BODY")
                .multilineTextAlignment(.center)
                .font(.headline)
                .foregroundColor(Color(ThemeService.shared().theme.textPrimaryColor))
            
            Button(action: {
                presentationMode.wrappedValue.dismiss()
                SKStoreReviewController.requestReviewInCurrentScene()
            })
             {
                Text("HAPPY_BIRTHDAY_BUTTON_GIVE_STARS")
             }
             .buttonStyle(PrimaryActionButtonStyle())
        }
        .padding()
    }
}

struct HappyBirthdayView_Previews: PreviewProvider {
    static var previews: some View {
        HappyBirthdayView()
    }
}

// MARK: - SKStoreReviewController Extension for iOS 14+

extension SKStoreReviewController {
    public static func requestReviewInCurrentScene() {
        if let scene = UIApplication.shared.connectedScenes.first(where: { $0.activationState == .foregroundActive }) as? UIWindowScene {
            DispatchQueue.main.async {
                requestReview(in: scene)
            }
        }
    }
}
