//
/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation

struct LazyDevice {
    let timeStamp: Date
    let deviceCount: Int
}
class LazyRoomDevices : NSObject {
    static let shared = LazyRoomDevices()
    
    // reload devices older than 10 minutes
    let threshold = 10.0 * 60.0
    
    var devices: Dictionary<String, LazyDevice> = Dictionary()
    
    func deviceCount( room: MXRoom ) -> Int {
        guard let lazyDevice = devices[room.roomId] else {
            return 0
        }
        return lazyDevice.deviceCount
    }
    
    func areDevicesCurrent( room: MXRoom) -> Bool {
        guard let lazyDevice = devices[room.roomId] else {
            return false
        }
        return Date().timeIntervalSince(lazyDevice.timeStamp) < threshold
        
    }
    
    func setDeviceCount( room: MXRoom, deviceCount: Int) {
        devices[room.roomId] = LazyDevice(timeStamp: Date(), deviceCount: deviceCount)
    }
}
