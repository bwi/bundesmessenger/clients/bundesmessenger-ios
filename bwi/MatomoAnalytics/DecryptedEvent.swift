//
/*
 * Copyright (c) 2023 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import AnalyticsEvents

/// `DecryptedEvent` represents a decryption failure that was decrypted.
@objcMembers class DecryptedEvent: NSObject {
    /// The id of the event that was unabled to decrypt.
    let eventId: String
    /// The time the failure has been reported.
    let ts: TimeInterval
    // bwi: #5265 the time the failure has been decrypted
    var decryptedAt: TimeInterval = Date().timeIntervalSince1970

    init(eventId: String, ts: TimeInterval) {
        self.eventId = eventId
        self.ts = ts
    }
}
