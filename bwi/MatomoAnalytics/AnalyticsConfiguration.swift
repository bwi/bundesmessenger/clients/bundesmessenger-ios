//
/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation


struct AnalyticsConfiguration {
    
    private enum ConfigType: String {
        case productive, beta, test, userdefined, undefined
    }
    
    private struct Config {
        let baseUrl: String
        let siteID: String
        let sendMessageDimensionIndex: Int
    }
    
    private var currentConfigs: [String:Config] = [:]
    private var selectedConfigType: ConfigType = .undefined
    
    mutating func readConfig( jsonFile: String ) {
        let fileURL = URL(fileURLWithPath: jsonFile)

        do {
            let jsonData = try Data(contentsOf: fileURL)
            let json = try JSONSerialization.jsonObject(with: jsonData, options: [])

            if let dictionary = json as? [String: Any] {
                if let configArray = dictionary["config"] as? [[String: Any]] {
                    for configDict in configArray {
                        // Access the values in each dictionary
                        if let type = configDict["type"] as? String,
                           let baseURL = configDict["baseURL"] as? String,
                           let siteID = configDict["siteID"] as? String,
                           let sendMessageDimensionIndex = configDict["sendMessageDimensionIndex"] as? String {
                            currentConfigs[type] = Config(baseUrl: baseURL, siteID: siteID, sendMessageDimensionIndex: Int(sendMessageDimensionIndex)!)
                        }
                    }
                }
            }
        } catch {
        }
        
        selectedConfigType = self.computeConfigurationType()
    }
    
    func selectedBaseURL() -> String? {
        if let config = currentConfigs[selectedConfigType.rawValue] {
            return config.baseUrl
        }
        return nil
    }
    
    func selectedSiteID() -> String? {
        if let config = currentConfigs[selectedConfigType.rawValue] {
            return config.siteID
        }
        return nil
    }
    
    func selectedSendMessageDimensionIndex() -> Int? {
        if let config = currentConfigs[selectedConfigType.rawValue] {
            return config.sendMessageDimensionIndex
        }
        return nil
    }
    
    private func computeConfigurationType() -> ConfigType {
        if currentConfigs[ConfigType.userdefined.rawValue] != nil {
            return .userdefined
        }
        if AppInfo.current.displayName == BWIBuildSettings.shared.matomoNameProd {
            return .productive
        }
        if AppInfo.current.displayName == BWIBuildSettings.shared.matomoNameBeta {
            if AppConfigService.shared.serverUrl() == BWIBuildSettings.shared.matomoServerProd {
                return .beta
            } else {
                return .test
            }
        }
        return .undefined
    }
}
