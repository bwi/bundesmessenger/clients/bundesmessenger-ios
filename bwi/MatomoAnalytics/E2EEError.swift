//
/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation

/*
 typedef enum : NSUInteger
 {
     encryptionNotEnabledCode = 0,
     unableToEncryptCode,
     unableToDecryptCode,
     olmCode,
     unknownInboundSessionIdCode,
     inboundSessionMismatchRoomIdCode,
     missingFieldsCode,
     missingCiphertextCode,
     notIncludedInRecipientsCode,
     badRecipientCode,
     badRecipientKeyCode,
     forwardedMessageCode,
     badRoomCode,
     badEncryptedMessageCode,
     duplicateMessageIndexCode,
     missingPropertyCode,
 } Code;
 */

enum AnalyticsE2EEErrorCode: Int {
    case encryptionNotEnabledCode = 0
    case unableToEncryptCode = 1
    case unableToDecryptCode = 2
    case olmCode = 3
    case unknownInboundSessionIdCode = 4
    case inboundSessionMismatchRoomIdCode = 5
    case missingFieldsCode = 6
    case missingCiphertextCode = 7
    case notIncludedInRecipientsCode = 8
    case badRecipientCode = 9
    case badRecipientKeyCode = 10
    case forwardedMessageCode = 11
    case badRoomCode = 12
    case badEncryptedMessageCode = 13
    case duplicateMessageIndexCode = 14
    case missingPropertyCode = 15
    // BWI: #4956 add decryption error information
    case unspecifiedIdentifierCode = 16
    case unspecifiedSerializationCode = 17
    case unspecifiedStoreCode = 18
    // Add more error codes as needed

    var description: String {
        switch self {
        case .encryptionNotEnabledCode:
            return "Encryption_Not_Enabled"
        case .unableToEncryptCode:
            return "Unable_To_Encrypt"
        case .unableToDecryptCode:
            return "Unable_To_Decrypt"
        case .olmCode:
            return "Olm_Unknown_Message_Index"
        case .unknownInboundSessionIdCode:
            return "Unknown_Inbound_SessionId"
        case .inboundSessionMismatchRoomIdCode:
            return "Inbound_Session_Mismatch_RoomId"
        case .missingFieldsCode:
            return "Missing_Fields"
        case .missingCiphertextCode:
            return "Missing_Ciphertext"
        case .notIncludedInRecipientsCode:
            return "Not_Included_In_Recipients"
        case .badRecipientCode:
            return "Bad_Recipient"
        case .badRecipientKeyCode:
            return "Bad_Recipient_Key"
        case .forwardedMessageCode:
            return "Forwarded_Message"
        case .badRoomCode:
            return "Bad_Room"
        case .badEncryptedMessageCode:
            return "Bad_Encrypted_Message"
        case .duplicateMessageIndexCode:
            return "Duplicate_Message_Index"
        case .missingPropertyCode:
            return "Missing_Property"
        case .unspecifiedIdentifierCode:
            return "Unspecified_Identifier_Error"
        case .unspecifiedSerializationCode:
            return "Unspecified_Serialization_Error"
        case .unspecifiedStoreCode:
            return "Unspecified_StoreCode_Error"
        }
    }
    
    // You can also define additional methods or properties as needed
}
