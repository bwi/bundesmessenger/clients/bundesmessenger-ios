//
/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation

@objc class BWIAnalyticsAccountDataService: NSObject {
    
    private struct AccountAnalytics: Codable {
        
        enum CodingKeys: String, CodingKey {
            case consent
            case time
            case platform
            case appVersion = "app_version"
        }
        
        let consent: Bool
        let time: Int
        let platform: String
        let appVersion: String
        
        func toJsonDict() -> [String : Any] {
            return [CodingKeys.consent.rawValue:consent, CodingKeys.time.rawValue:time, CodingKeys.platform.rawValue:platform, CodingKeys.appVersion.rawValue:appVersion]
        }
        
    }
    
    private enum AccountDataTypes {
        static let analytics = "de.bwi.analytics"
    }
    
    let session: MXSession
    private lazy var serializationService: SerializationServiceType = SerializationService()
    
    init(mxSession: MXSession) {
        session = mxSession
    }
    
    func needsToShowPromt() -> Bool {
        guard let analyticsArray = session.accountData.accountData(forEventType: AccountDataTypes.analytics) as? [String: Any] else {
            return true
        }
        guard let deviceID = session.myDeviceId, analyticsArray[deviceID] is [String: Any] else {
            return true
        }
        return false
    }
    
    func isEnabled() -> Bool {
        guard let analyticsArray = session.accountData.accountData(forEventType: AccountDataTypes.analytics) as? [String: Any] else {
            return false
        }
        
        guard let deviceId = session.myDeviceId, let analyticsDict = analyticsArray[deviceId] as? [String: Any] else {
            return false
        }
        
        do {
            let analytics: AccountAnalytics = try serializationService.deserialize(analyticsDict)
            
            return analytics.consent
        } catch {
            
        }
        
        return false
    }
    
    func setEnabled(_ enabled: Bool) {
        var analyticsDict = session.accountData.accountData(forEventType: AccountDataTypes.analytics) ?? [:]
        
        let currentConsent = AccountAnalytics(consent: enabled, time: Int(Date().timeIntervalSince1970*1000), platform: "ios", appVersion: AppInfo.current.appVersion?.bundleShortVersion ?? "")
        
        analyticsDict[session.myDeviceId] = currentConsent.toJsonDict()
            
        session.setAccountData(analyticsDict, forType: AccountDataTypes.analytics, success: nil, failure: nil)
    }
}
