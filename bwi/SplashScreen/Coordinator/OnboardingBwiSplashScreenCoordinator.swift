/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import SwiftUI

protocol OnboardingBwiSplashScreenCoordinatorProtocol: Coordinator, Presentable {
    var completion: ((OnboardingBwiSplashScreenViewModelResult) -> Void)? { get set }
}

final class OnboardingBwiSplashScreenCoordinator: OnboardingBwiSplashScreenCoordinatorProtocol {
    
    // MARK: - Properties
    
    // MARK: Private
    
    private let onboardingSplashScreenHostingController: UIViewController
    private var onboardingSplashScreenViewModel: OnboardingBwiSplashScreenViewModelProtocol
    private var timer: Timer?
    
    // MARK: Public

    // Must be used only internally
    var childCoordinators: [Coordinator] = []
    var completion: ((OnboardingBwiSplashScreenViewModelResult) -> Void)?
    
    // MARK: - Setup
    
    @available(iOS 14.0, *)
    init() {
        let viewModel = OnboardingBwiSplashScreenViewModel()
        onboardingSplashScreenViewModel = viewModel

        if BWIBuildSettings.shared.showBUMLottieAnimation {
            onboardingSplashScreenHostingController = BUMLaunchLoadingViewController()
        } else {
            let view = OnboardingBwiSplashScreen(viewModel: viewModel.context)
            let hostingController = VectorHostingController(rootView: view)
            hostingController.vc_removeBackTitle()
            hostingController.isNavigationBarHidden = true
            onboardingSplashScreenHostingController = hostingController
        }

        NotificationCenter.default.addObserver(self, selector: #selector(cancelAnimation(_:)), name: .cancelLaunchLoadingAnimation, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Public
    func start() {
        if BWIBuildSettings.shared.showBUMLottieAnimation {
            timer?.invalidate()
            timer = Timer.scheduledTimer(withTimeInterval: 5, repeats: false) { timer in
                timer.invalidate()
                self.completion?(.login)
            }
        } else {
            onboardingSplashScreenViewModel.completion = { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .login:
                    self.completion?(result)
                }
            }
        }
    }
    
    func toPresentable() -> UIViewController {
        return self.onboardingSplashScreenHostingController
    }
    
    
    @objc func cancelAnimation(_ sender: UITapGestureRecognizer? = nil) {
        timer?.invalidate()
        timer = nil
        self.completion?(.login)
    }

}
