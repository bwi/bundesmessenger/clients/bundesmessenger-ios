/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import SwiftUI
import Combine

@available(iOS 14, *)
typealias OnboardingBwiSplashScreenViewModelType = StateStoreViewModel<OnboardingBwiSplashScreenViewState,
                                                                    OnboardingBwiSplashScreenViewAction>

protocol OnboardingBwiSplashScreenViewModelProtocol {
    var completion: ((OnboardingBwiSplashScreenViewModelResult) -> Void)? { get set }
    @available(iOS 14, *)
    var context: OnboardingBwiSplashScreenViewModelType.Context { get }
}


@available(iOS 14, *)
class OnboardingBwiSplashScreenViewModel: OnboardingBwiSplashScreenViewModelType, OnboardingBwiSplashScreenViewModelProtocol {

    // MARK: Public

    var completion: ((OnboardingBwiSplashScreenViewModelResult) -> Void)?

    // MARK: - Setup

    init() {
        super.init(initialViewState: OnboardingBwiSplashScreenViewState())
    }

    // MARK: - Public

    override func process(viewAction: OnboardingBwiSplashScreenViewAction) {
        switch viewAction {
        case .login:
            login()
        }
    }

    private func login() {
        completion?(.login)
    }
}
