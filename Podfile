source 'https://github.com/CocoaPods/Specs.git'

# Uncomment this line to define a global platform for your project
platform :ios, '15.0'

# By default, ignore all warnings from any pod
inhibit_all_warnings!

# Use frameworks to allow usage of pods written in Swift
use_frameworks!

# Different flavours of pods to MatrixSDK. Can be one of:
# - a String indicating an official MatrixSDK released version number
# - `:local` (to use Development Pods)
# - `{ :branch => 'sdk branch name'}` to depend on specific branch of MatrixSDK repo
# - `{ :specHash => {sdk spec hash}` to depend on specific pod options (:git => …, :podspec => …) for MatrixSDK repo. Used by Fastfile during CI
#
# Warning: our internal tooling depends on the name of this variable name, so be sure not to change it
$matrixSDKVersion = '= 0.27.6'
# $matrixSDKVersion = :local
# $matrixSDKVersion = { :branch => 'develop'}
# $matrixSDKVersion = { :specHash => { git: 'https://git.io/fork123', branch: 'fix' } }

########################################

case $matrixSDKVersion
when :local
$matrixSDKVersionSpec = { :path => '../matrix-ios-sdk/MatrixSDK.podspec' }
when Hash
spec_mode, sdk_spec = $matrixSDKVersion.first # extract first and only key/value pair; key is spec_mode, value is sdk_spec

  case spec_mode
  when :branch
  # :branch => sdk branch name
  sdk_spec = { :git => 'https://github.com/matrix-org/matrix-ios-sdk.git', :branch => sdk_spec.to_s } unless sdk_spec.is_a?(Hash)
  when :specHash
  # :specHash => {sdk spec Hash}
  sdk_spec = sdk_spec
  end

$matrixSDKVersionSpec = sdk_spec
when String # specific MatrixSDK released version
$matrixSDKVersionSpec = $matrixSDKVersion
end

$matrixSDKVersionSpec = { :git => 'https://dl-gitlab.example.com/bwmessenger/bundesmessenger/bundesmessenger-ios-sdk', :tag => 'v2.24.0' }

########################################

def import_MatrixKit_pods
  pod 'libPhoneNumber-iOS', '~> 0.9.13'  
  pod 'DTCoreText', '1.6.26'
  #pod 'DTCoreText/Extension', '~> 1.6.25'
  pod 'Down', '~> 0.11.0'
end

def import_SwiftUI_pods
    pod 'Introspect', '~> 0.1'
    pod 'ZXingObjC', '~> 3.6.9'
end

abstract_target 'RiotPods' do

  pod 'GBDeviceInfo', '~> 7.1.0'
  pod 'Reusable', '~> 4.1'
  pod 'KeychainAccess', '~> 4.2.2'
  pod 'WeakDictionary', '~> 2.0'
  
  # Piwik for analytics
  pod 'MatomoTracker', '~> 7.5.2'

  pod 'zxcvbn-ios'

  # Tools
  pod 'SwiftGen'
  pod 'SwiftLint'
  pod 'SwiftFormat/CLI'

  target "Riot" do
    pod 'MatrixSDK', $matrixSDKVersionSpec, :inhibit_warnings => false
    import_MatrixKit_pods

    import_SwiftUI_pods

    pod 'UICollectionViewRightAlignedLayout', '~> 0.0.3'
    pod 'UICollectionViewLeftAlignedLayout', '~> 1.0.2'
    pod 'KTCenterFlowLayout', '~> 1.3.1'
    pod 'FlowCommoniOS', '~> 1.12.0'
    pod 'DTTJailbreakDetection', '~> 0.4.0'
    pod 'ReadMoreTextView', '~> 3.0.1'
    pod 'SideMenu', '~> 6.5'
    pod 'DSWaveformImage', '~> 6.1.1'

   target 'RiotTests' do
      inherit! :search_paths
    end

#    target 'RiotUITests' do
#      inherit! :search_paths
#    end
  end

  target "BundesMessenger" do
      pod 'MatrixSDK', $matrixSDKVersionSpec, :inhibit_warnings => false
      import_MatrixKit_pods

      import_SwiftUI_pods

      pod 'UICollectionViewLeftAlignedLayout', '~> 1.0.2'
      pod 'UICollectionViewRightAlignedLayout', '~> 0.0.3'
      pod 'KTCenterFlowLayout', '~> 1.3.1'
      pod 'FlowCommoniOS', '~> 1.12.0'
      pod 'DTTJailbreakDetection', '~> 0.4.0'
      pod 'ReadMoreTextView', '~> 3.0.1'
      pod 'SideMenu', '~> 6.5'
      pod 'DSWaveformImage', '~> 6.1.1'
  end
  
    target "BuM-Beta" do
      pod 'MatrixSDK', $matrixSDKVersionSpec, :inhibit_warnings => false
      import_MatrixKit_pods

      import_SwiftUI_pods

      pod 'UICollectionViewLeftAlignedLayout', '~> 1.0.2'
      pod 'UICollectionViewRightAlignedLayout', '~> 0.0.3'
      pod 'KTCenterFlowLayout', '~> 1.3.1'
      pod 'FlowCommoniOS', '~> 1.12.0'
      pod 'DTTJailbreakDetection', '~> 0.4.0'
      pod 'ReadMoreTextView', '~> 3.0.1'
      pod 'SideMenu', '~> 6.5'
      pod 'DSWaveformImage', '~> 6.1.1'
  end
    
    target "BuM-Open" do
        pod 'MatrixSDK', $matrixSDKVersionSpec, :inhibit_warnings => false
        import_MatrixKit_pods

        import_SwiftUI_pods

        pod 'UICollectionViewLeftAlignedLayout', '~> 1.0.2'
        pod 'UICollectionViewRightAlignedLayout', '~> 0.0.3'
        pod 'KTCenterFlowLayout', '~> 1.3.1'
        pod 'FlowCommoniOS', '~> 1.12.0'
        pod 'DTTJailbreakDetection', '~> 0.4.0'
        pod 'ReadMoreTextView', '~> 3.0.1'
        pod 'SideMenu', '~> 6.5'
        pod 'DSWaveformImage', '~> 6.1.1'
    end
    
    target "BuM-BWI-MDM" do
      pod 'MatrixSDK', $matrixSDKVersionSpec, :inhibit_warnings => false
      import_MatrixKit_pods

      import_SwiftUI_pods

      pod 'UICollectionViewLeftAlignedLayout', '~> 1.0.2'
      pod 'UICollectionViewRightAlignedLayout', '~> 0.0.3'
      pod 'KTCenterFlowLayout', '~> 1.3.1'
      pod 'FlowCommoniOS', '~> 1.12.0'
      pod 'DTTJailbreakDetection', '~> 0.4.0'
      pod 'ReadMoreTextView', '~> 3.0.1'
      pod 'SideMenu', '~> 6.5'
      pod 'DSWaveformImage', '~> 6.1.1'
  end

  target "RiotSwiftUI" do
    import_SwiftUI_pods
  end

  target "RiotSwiftUITests" do
    import_SwiftUI_pods
  end

  target "RiotNSE" do
    pod 'MatrixSDK', $matrixSDKVersionSpec, :inhibit_warnings => false
    import_MatrixKit_pods
  end

  target "BroadcastUploadExtension" do
    pod 'MatrixSDK', $matrixSDKVersionSpec, :inhibit_warnings => false
  end

  # Disabled due to crypto corruption issues.
  # https://github.com/element-hq/element-ios/issues/7618
  # target "RiotShareExtension" do
  #   import_MatrixSDK
  #   import_MatrixKit_pods
  # end
  #
  # target "SiriIntents" do
  #   import_MatrixSDK
  #   import_MatrixKit_pods
  # end

end

post_install do |installer|
  installer.pods_project.targets.each do |target|

    target.build_configurations.each do |config|
      # Disable bitcode for each pod framework
      # Because the WebRTC pod (included by the JingleCallStack pod) does not support it.
      # Plus the app does not enable it
      config.build_settings['ENABLE_BITCODE'] = 'NO'

      # Force ReadMoreTextView to use Swift 5.2 version (as there is no code changes to perform)
      if target.name.include? 'ReadMoreTextView'
        config.build_settings['SWIFT_VERSION'] = '5.2'
      end

      # Stop Xcode 12 complaining about old IPHONEOS_DEPLOYMENT_TARGET from pods
      config.build_settings.delete 'IPHONEOS_DEPLOYMENT_TARGET'

      # Disable nullability checks
      config.build_settings['WARNING_CFLAGS'] ||= ['$(inherited)','-Wno-nullability-completeness']
      config.build_settings['OTHER_SWIFT_FLAGS'] ||= ['$(inherited)', '-Xcc', '-Wno-nullability-completeness']
    end

    # Fix Xcode 14 resource bundle signing issues
    # https://github.com/CocoaPods/CocoaPods/issues/11402#issuecomment-1259231655
    if target.respond_to?(:product_type) and target.product_type == "com.apple.product-type.bundle"
      target.build_configurations.each do |config|
        config.build_settings['CODE_SIGNING_ALLOWED'] = 'NO'
      end
    end

  end

  Dir.glob("#{installer.sandbox.target_support_files_root}/**/*.pch") do |item|
    open(item, "a") do |file|
      print "[post_install] Updating #{item}\n"
      file.puts <<EOF
// Disable logs
#ifndef DEBUG
    #define NSLog(...)
#endif
EOF
    end
  end
end
