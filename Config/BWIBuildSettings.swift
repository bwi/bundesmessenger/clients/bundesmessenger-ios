//
/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation
import KeychainAccess

@objcMembers
class BWIBuildSettings: NSObject {
    static let shared = BWIBuildSettings()
    
    private let vault: KeyValueVault

    override init() {
        vault = KeychainVault(Keychain(service: BwiSettingsConstants.bwiSettingsKeychainService,
                                                     accessGroup: BuildSettings.keychainAccessGroup))

        super.init()
        overrideTargetSpecificSettings()
    }

    // MARK: -
    
    private enum UserDefaultsKeys {
        static let additionalBwiHeaderKey = "bwiHeader"
        static let isWellknownFederationEnabled = "isWellknownFederationEnabled"
        static let isLabsFederationEnabled = "isLabsFederationEnabled"
    }
    
    private struct BwiSettingsConstants {
        static let bwiSettingsKeychainService: String = BuildSettings.baseBundleIdentifier + ".bwi-settings-service"
    }
            
    func reset() {
        additionalBwiHeader = UserAgentService().bwiUserAgentDict
        let sdkOptions = MXSDKOptions.sharedInstance()
        sdkOptions.httpAdditionalHeaders = UserAgentService().bwiUserAgentDict
    }
    
    // MARK: Servers
    
    var additionalBwiHeader: Dictionary<String, String> {
        get {
            do {
                guard let data = try vault.data(forKey: UserDefaultsKeys.additionalBwiHeaderKey) else {
                    return UserAgentService().bwiUserAgentDict
                }
                var dict = try JSONDecoder().decode(Dictionary<String,String>.self, from: data)
                dict["User-Agent"] = UserAgentService().bwiUserAgent
                return dict
            } catch {
                return UserAgentService().bwiUserAgentDict
            }
        } set {
            do {
                let data = try JSONEncoder().encode(newValue)
                try vault.set(data, forKey: UserDefaultsKeys.additionalBwiHeaderKey)
            } catch let error {
                NSLog("[PinCodePreferences] Error when storing addional header to the vault: \(error)")
            }
        }
    }

    // MARK: -
    
    var secondaryAppName = ""
    
    // Location Sharing
    // (this disables error messages when map laoding failed)
    var locationSharingSSLProblemWorkaround = true
    var locationSharingEnabled = false

    // Integration check
    var forcedPinProtection = true

    // Jailbreak check
    var forcedNoneJailbroken = true
    
    /// Default number of iterations for secure storage. Do not change this value after going live.
    var iterationsForSecureStorage: UInt = 100000
    
    /// Allow split view detail view stacking
    var allowSplitViewDetailsScreenStacking = true
    
    // direct rooms are for two persons only in bwi context
    var allowInviteOnDirectRooms = false
    
    // test setup for downtime: should be false for all builds
    var useTestDataForDowntime = false
    
    var flavor = ""
    
    var showBwiSplashScreen = false
    
    var bwiShowRoomSearch = false
    
    var bwiAllowRoomPermalink = false
    
    var bwiAllowUserPermalink = false

    var bwiCheckAppVersion = true
        
    var bwiNotificationTimes = false

    var bwiUserLabelsAdminSettingsVisible = true
    var bwiUserLabelsMemberDetailsVisible = true
    var bwiUserLabelsParticipantsVisible = true
    var bwiUserLabelsTimelineDisplayNameVisible = true
    var bwiUserLabelsTimelineEventVisible = true
    
    var bwiUserLabelEventTypeString = "de.bwi.room.user_function_labels"
    
    var bwiUserLabelParticipantSorting = true
    
    var bwiShowClosedPolls = true
    var bwiPollShowParticipantsToggle = true
    var bwiPollVisibleVotes = 5
    var bwiPollParticipantsInHistory = true
    
    
    var bwiShowThreads = false
    
    var bwiShowRoomCreationSectionFooter = false
    
    var bwiAutoCreateAliasOnRoomCreation = true
    
    var bwiLocationShareButtonVisible = false
    var bwiBetterIgnoredUsers = true
    var bwiSettingsShowInAppNotifications = false
    var bwiFilteredContextMenu = true
    
    var bwiShowPinnedNotificationSettings = false
    var bwiShowSessionSettingsFooter = false
    
    var bwiEnablePersonalState = false
    
    // In onboarding splash screen dis/enable register button
    var bwiOnboardingSplashScreenEnableRegister = false

    // make sure that the NSE extention always resets message body and title
    var bwiHideNotificationMessageBody = true
    
    // in Auth screen dis/enable register button even with onboarding splash screen enabled
    var bwiAuthentificationScreenEnableRegister = false

    // enable BuM style authentication UI (more Info text and a logo)
    var bwiEnableBuMAuthentificationUI = true
        
    // voicemessages should resignplaying when the app enters the background
    var bwiResignPlayingVoiceMessageInBackground = true
    
    // enable BUM style UI in pincode and maybe other places
    var bwiEnableBuMUI = true
        
    // clear media cache every time when leaving a room vc: no media should be saved on device for security purposes
    var bwiClearMediaCacheOnRoomExit = true
    
    var bwiEnableLoginProtection = true

    var bwiHashes = [ "a3f65e35a7476799afe8d80282fb3c45b39dab06d1d8c70dc98e45ab7d8e93a9",
                      "2fda1a831655c22a5e6096d7cfbff4429fbf27891141e191b46adbf168142a11",
                      "4f8cbb3fef885f7284d0477d797d7007f0e1ba76221834132752f4d645796e28",
                       "24c2ec541e61e8e68944b96dc45ed5df12f6bdbda283cb0b3a522742aa970256",
                       "1be0b314a6c915d4475290522baef5b642db1b6d68937792b8e0eb5b7b0d6666",
                      "3deb73db8cafcd1d5a59e25e251c35816162e1f6ee67b5d7d011da0e8d6ef931",
                      "42e57985d61202c2c7dd87d898cef9bdce020877a4c7a8c7cd699f6a28f58c0c",
                      "e1c3c7cac12bd65bd48de79a2677187d2e768d2769377627534023588b8d7a33",
                      "300f100961520d2909686f405bf97f53273f8ea82fa5359d981af8bf755f56ea",
                      "642e9a5b1276d65cd12f913b96a3d05fe022489f5481e0c888dfd0654b25177d",
                      "f7b8efdec2f424dbc912f4592d2489cc26232a621feecade73c33205a0a5cd8a",
                      "7cfd1c9b9405146681e43f6339ea487f083a3a12cea7cf669810ea160407781a",
                      "72d9a018893555073840bd90d80301417d2caa8b6ada7973d3365bcf929d6321",
                      "28e0940e355717de28a9b48add20ebb7ed178875937015033d394129d9356cb3",
                      "58077bffe53341e53ad18363dafc09498c314dd05a4fbaa2150c48dbd5d35e09",
                      "74c038bb4e26fb1d0fcc14474ec9ff6fe3ec158e13286a787b90a22ee638ac18",
                      "3740163f98aeda7dba285d2af1bfc351db395868268e2759ca701f926a6605a5",
                      "240b05d9a54999140d23f21d104109fbc5e5179366ba3a7e58c8fad763aa88bd",
                      "4d5b6dcf02396274be58a69c4bbeba175b529f6b19c504fc99a37892ee1cf0b5",
                      "0d157119821bd9d76ac4f24c7f14f56e6bb5b766a6d5ee7dad6634420e79271a",
                      "e3573fe09d518cce80cececedf80f8e0020cbc150f22db8b64827bff2e27abd9",
                      "b76a62ccd8ea70d01c3a35ec3839e49ed2c83c8e3276f40a1b2c2cdf7cd77d01",
                      "4a610a4d5fd3d8a1e1fd5669abdf1e0c5f7f5ff0c6b559e0f360cfa092ecb115",
                      "32752f6d21f3005587941415cd64812ee28c19e6e01ed307edf9ddf4f6a91583",
                      "704c6eaa107b13ef0694eb7ddd041bb6f595b53670a2e0c3c16e199947a9e013",
                      "6921f031357cf63fb8538d9a1d1971efae95899907fdbf05a05082b6d1a6d0fb",
                      "9f960fc663f5eaae67eecff75b131dea130b3ab1cf889c45fc74c688a48aea30",
                      "160c35279484a027031b131183f3f203b1166306bab214355b00cf28502bce11",
                      "d5a7298dde23aa0269c4cbd3b2a543e6ede94ce78fc20e4bfb888eb6057b5c52",
                      "00136d830dd2acd5047efcf8419e939ef7ef97a84bef1930df86aace3f855265",
                      "64cbbeea37237814445b35c941d010b9d5d024e4c584a476864b00c7c9909bce",
                      "e79f4ce0f3c2772b45fd492a9c11e4e10e869ca21af68f13ff48c9c3bbd446ea",
                      "2d582bed323f226a0e18b6b7104c0d28ccc36423833220a7b5fd2854262ab27e",
                      "c56904235e283557626c327f8013c3b1c654eae86a5e314531e3a6fcb200ff92",
                      "202bbbaa7c5cd665106d14012c29bcda8217a4b3606cce83e6e6ea0d30733229",
                      "cca10f6b4b583da69bbd3815ee0fccd193cf0cfd046aee1aeffaa7b5245e8f83",
                      "36a9ec7368bddedd9deb1e2d1c627bd7304865135c9be30b1979659e3ac9ad07",
                      "dbaf8618e8a2f8d681591dfbcc73243c921c10dec69a2e5ee50bc91ca7dedcda",
                      "ed1af0fd873ec749f17c3b61ce4e481ab1644c132003f97a9c4e36516325788a",
                      "081e6ef90ba86102d678756fd13b07ca744340ad4d58a340e1956dca992f18e3",
                      "40b22592f2417c8031a0c38098c83dd0bfd28dee4c77ed1e9a022556c6ec0ded",
                      "098d7b8e7487c2228e6848c1baf6b5fec716b8d94d0210c22bad6adba5a332bf",
                      "bce875bbf120c13246f2591af8681bbc554068d0b0cbf3837604607fdf99001e",
                      "8362cb3205fb58345f1cc43115023027ea420b589da099271e127b9e9addb06c",
                      "2ae47272786b03f790ffef1331dc92b114f65bc2fc321f82ca78a32ae471043e",
                      "224185dd537000a0f5be5e09be1bd39103363b38bb8e49719d14f680a4d5e5ee",
                      "8bab8d8d993259213d7ef2295e3494382b4611f2d68596a120e7cdfbb33485d2",
                      "b9aa60d0067f63aa81eb6521af2120f2405e4bde4963b060ac34890e41734937",
                      "2fd5548d873cdd2b48691593d3a3121b452e4f990d2b0eddfca2bc44255ccb46",
                      "05a294b2e2214e326d9dc55a5ae4b1d91d0bd0e95177e59159c42ebaf8dae243",
                      "983a96007faae2d5321aadf10198bf1a568d4166eec24f9c878de12ad5da8b85",
                      "09940562c6e5d1b4071873c1be36dfe526c33a9c87bce30935c43ed451a67d72",
                      "c58c1892ba63b2a482a2ad72d563d523eff08759e6026b8630d64d41b48e7ae0",
                      "db0c9012e0886da4cbbaf4fae3d4c8d345a95fcc004c0fa8132b5f718963750d",
                      "e4920edcf64870e0d86a8e511ad3ba0dc91f7208c891329d6ee9a64b4b7a07e6",
                      "9f60d10d6ee4d1be2a5f301c57aae3224a3d010564c302346395ab1a7e2aa35f",
                      "ddf38402e479dcfe29066efd81fde1fdd2e767b1780d1736bdb8def2753065d1",
                      "6e9020ced31422578601a91bc96474c1e36c1b0c2f4a4193c9c49f1bde6749fb"
    ]
    // bwi #6162 login protection with jwt tokens
    var bwiEnableTokenizedLoginProtection = false
    
    // use a different badge color if the user was mentioned in a room
    var showMentionsInRoom = true

    // replace feature history link variable with the appropiate build setting
    var bwiFeatureHistoryLink = "https://messenger.bwi.de/bwmessenger#c7385"
    var bwiReplaceFeatureLink = true
    
    // feature history file path
    var bwiFeatureHistoryFilePath: String {
        guard let bundleFileURL = Bundle.main.url(forResource: newFeaturesHTML, withExtension: "html") else {return ""}
        
        // replace feature link and copy file into document directory
        if bwiReplaceFeatureLink {
            if var newFileURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last {
                newFileURL = newFileURL.appendingPathComponent(newFeaturesHTML.appending(".html"))
                do {
                    var text = try String(contentsOf: bundleFileURL, encoding: .utf8)
                    text = text.replacingOccurrences(of: "$FEATURELINK", with: bwiFeatureHistoryLink)
                    try text.write(to: newFileURL, atomically: false, encoding: .utf8)
                    
                    // return new path
                    return newFileURL.path
                } catch {}
            }
        }
        
        return bundleFileURL.path
    }

    // login with matrix id should only be enabled in some configurations
    var bwiEnableLoginWithMatrixID = true
    
    // show app specific loading icons instead of the rotating element logo
    var showBUMLottieAnimation = true

    // DMs don't need all roomsettings (like changing avatar, name, topic)
    var showUnrelatedRoomSettingsForDirectMessages = false
    
    // create rooms without shared history
    var enableSharedHistoryOnRoomCreation = false
    
    // explicitly set sdk option for key sharing => We don't want to share even if the room setting allows it
    var allowKeySharingOnRoomInvite = true

    // DMs don't need all roomsettings (like changing avatar, name, topic)
    var allowDoubleTapOnImageAttachmentsForZoom = true

    // ------ End of BwiBuildSettings ---------
    
    // Element-Web instance for the app
    var applicationWebAppUrlString = ""
    
    // Default servers proposed on the authentication screen
    var serverConfigDefaultHomeserverUrlString = "https://bundesmessenger.isthebest.real"
    // (#4549) this is needed because a real URL is expected by Element, for all of our systems this should be overwritten by well-known
    var serverConfigDefaultMapstyleURLString = "https://default.mapstyle.url"
    var serverConfigDefaultIdentityServerUrlString = ""
    var serverConfigPreSelections = ["":""]
    var serverConfigSygnalAPIUrlString = "http://push-local/_matrix/push/v1/notify"
    
    // Note: Set empty strings to hide the related entry in application settings
    var applicationCopyrightUrlString = "https://messenger.bwi.de/copyright"
    var applicationPrivacyPolicyUrlString = "https://messenger.bwi.de/datenschutz"
    var applicationTermsConditionsUrlString = ""
    var applicationPrivacyPolicyWithMatomoSectionUrlString = "https://messenger.bwi.de/datenschutz#c6637"
    
    // (#4751) use privacy policy link of well known
    var bwiUseWellKnownPrivacyPolicyLink: Bool = false
    
    // MARk: - Matrix permalinks
    // Paths for URLs that will considered as Matrix permalinks. Those permalinks are opened within the app
    var permalinkSupportedHosts: [String: [String]] = [:]
    
    // MARK: - VoIP
    var allowVoIPUsage = false
    var stunServerFallbackUrlString: String? = ""
    
    // MARK: -  Public rooms Directory
    // List of homeservers for the public rooms directory
    var publicRoomsDirectoryServers = ["matrix.org"]

    // MARK: - Analytics
    
    /// BWI: set host and key to nil to disable PostHog tracking
    var analyticsHost: String? = nil
    var analyticsKey: String? = nil
    var bwiAnalyticsServerUrlString = ""
    var bwiAnalyticsAppId = "1"
    
    /// The configuration to use for analytics during development. Set `isEnabled` to false to disable analytics in debug builds.
    var analyticsConfiguration = BuildSettings.AnalyticsConfiguration(isEnabled: false,
                                                                                   host: "",
                                                                                   apiKey: "",
                                                                                   termsURL: URL(string: "https://element.io/cookie-policy")!)
    
    var sendMessageThreshold = 5.0
    
    // MARK: - Bug report
    var bugReportEndpointUrlString = ""

    // MARK: - Integrations
    // Widgets in those paths require a scalar token
    var integrationsScalarWidgetsPaths = [""]
    // Jitsi server used outside integrations to create conference calls from the call button in the timeline
    var jitsiServerUrl = URL(string: "https://enter.jitsi.url")!
    var enableJSInWebView = false
    
    // MARK: - Features

    /// Setting to force protection by pin code
    var forcePinProtection = true

    /// Max allowed time to continue using the app without prompting PIN
    var pinCodeGraceTimeInSeconds: TimeInterval = 180

    var allowLocalContactsAccess = false
    var allowInviteExernalUsers = false
    var enableSideMenu = false
    var sideMenuShowInviteFriends = false

    // MARK: - Feature Specifics
    
    /// Not allowed pin codes. User won't be able to select one of the pin in the list.
    var notAllowedPINs: [String] = [
        "1234",
        "1111",
        "0000",
        "1212",
        "7777",
        "1004",
        "2000",
        "4444",
        "2222",
        "6969",
        "9999",
        "3333",
        "5555",
        "6666",
        "1122",
        "1313",
        "8888",
        "4321",
        "2001",
        "1010",
        "2580",
        "4711",
        "0815",
        "0852"
    ]

    var allowLocalContactPresence = false
    
    /// Indicates should the app log out the user when number of PIN failures reaches `maxAllowedNumberOfPinFailures`. Defaults to `false`
    var logOutUserWhenPINFailuresExceeded = true

    /// Indicates should the app log out the user when number of biometrics failures reaches `maxAllowedNumberOfBiometricsFailures`. Defaults to `false`
    var logOutUserWhenBiometricsFailuresExceeded = true
    
    /// If force is enabled the dialog for asking if reseting the key backup is not shown.. already asked before in mandatory verfication
    var forceResetBackupIfLost = true
    
    /// if skip is enabled the key backup dialogs are passed through
    var skipKeyBackupStep = true

    // MARK: - Main Tabs

    var homeScreenShowHomeTab = false

    // MARK: - General Settings Screen
    
    var settingsScreenAllowAddingLinkedEmails = false
    var settingsScreenAllowAddingPhoneNumbers = false
    var settingsScreenAllowAddingEmailThreepids = false
    var settingsScreenAllowAddingPhoneThreepids = false
    var settingsScreenShowThreepidExplanatory = false
    var settingsScreenShowDiscoverySettings = false
    var settingsScreenAllowIdentityServerConfig = false
    var settingsScreenShowConfirmMediaSize = false
    var settingsScreenShowAdvancedSettings = false
    var settingsScreenShowLabSettings = false
    var settingsScreenAllowChangingRageshakeSettings = false
    var settingsScreenAllowChangingCrashUsageDataSettings = false
    var settingsScreenAllowBugReportingManually = false
    var settingsScreenAllowDeactivatingAccount = false

    var settingsScreenShowLinkPreviews = false
    var settingsScreenShowInviteFriends = false
    var settingsScreenShowSettings = true
    var settingsScreenShowFeedback = false
    var settingsScreenShowHelp = false

    var settingsScreenShowNotificationDecodedContentOption = false
    var settingsScreenShowSystemSettingsOption = false
    var settingsScreenShowSupportSetting = true
    var settingsScreenSupportSettingHTML = "support"

    var settingsSecurityScreenShowCryptographyInfo:Bool = false
    var settingsSecurityScreenShowCryptographyExport:Bool = false
    var settingsSecurityScreenShowAdvancedUnverifiedDevices:Bool = false
    
    // MARK: - Notification Settings
    var settingsNotificationsBWIDefaultSet = true
    var settingsNotificationsShowDefault = true
    var settingsNotificationsShowMentions = false
    var settingsNotificationsShowAdvanced = false
    var notificationSettingsLikeAndroidAndWeb = true

    // MARK: - Timeline settings
    var roomInputToolbarCompressionMode: BuildSettings.MediaCompressionMode = .none

    // MARK: - Room Creation Screen
    
    var roomCreationScreenAllowEncryptionConfiguration = false

    // MARK: - Room Screen
    
    var roomScreenAllowStickerAction = false
    
    // MARK: - Room Info Screen
    
    var roomInfoScreenShowIntegrations = false

    // MARK: - Room Settings Screen
    
    var roomSettingsScreenShowLowPriorityOption = false
    var roomSettingsScreenShowDirectChatOption = false
    var roomSettingsScreenAllowChangingAccessSettings = false
    var roomSettingsScreenAllowChangingHistorySettings = false
    var roomSettingsScreenShowAddressSettings = false
    var roomSettingsScreenShowAdvancedSettings = false
    var roomSettingsScreenShowAccessSettingsBW = true
    var roomSettingsScreenRemoveLeave = true
    var roomSettingsScreenShowNotificationsV2 = false

    // MARK: - Message
    
    var messageDetailsAllowShare = false
    var messageDetailsAllowPermalink = false
    var messageDetailsAllowViewSource = false
    var messageDetailsAllowSave = false
    var messageDetailsAllowCopyMedia = false
    var messageDetailsAllowPasteMedia = false
    
    // MARK: - Authentication Screen
    
    var authScreenShowRegister = false
    var authScreenShowPhoneNumber = false
    var authScreenShowForgotPassword = false
    var authScreenShowCustomServerOptions = true
    var authScreenShowTestServerOptions = true
    var authScreenShowSocialLoginSection = false

    // MARK: - Self Verification not crosssigning  (bwi=true)
    var disableSelfUserVerification = true
    var disableCrosssigning = false
    var additionalSelfVerfificationAlert = false
    var showNoOtherDeviceError = false
    
    // MARK: - Antivirus scan  (bwi=true)
    
    var enableAntivirusScan = true
    
    // MARK: Verification screen (bwi=false)
    
    var showRecoverWithKey = false
    
    // MARK: Unified search screen (bwi=false)
    
    var showUnifiedSearchViewMessagesTab = false
    var showUnifiedSearchViewFilesTab = false

    // MARK: - Onboarding
    
    var onboardingShowAccountPersonalization = false
    var onboardingEnableNewAuthenticationFlow = true
    // show app specific welcoming screens
    var onboardingShowWelcomeScreens = true
    var disableLegacyAuthentication = true
    
    // ---
    
    // MARK: Last message timestamp support (bwi=false)
    var enableLastMessageTimestamp = false
    
    // MARK: Invite friends in Direct Chat (bwi=false)
    var directChatShowInviteFriends  = false
    
    // MARK: Last admin is not allowed to leave the room  (bwi=true)
    var lastAdminIsNotAllowedToLeaveRoom = true
    
    // MARK: Room Member Details Screen  (bwi=true)
    var roomMemberDetailsHideLeaveButton = true
    
    // MARK: Room create options (bwi=false)
    var enableShowInRoomDirectory = false

    // Mark: Unified Search  (bwi=true)
    var unifiedSearchScreenShowPublicDirectory = true
    
    // MARK: Allows removal of uploaded avatar photos (bwi=true)
    var enableRemoveAvatarImage = true
    
    // MARK: Add a toggle button to the login screen to make the password visible
    var passwordIndicatorOnLogin = true
    
    // MARK: Displays the element base version on the settings screen
    var elementBaseVersion = "1.11.19"
    
    var showElementBaseVersion = true

    // MARK: Bypasses the normal forgot password process by presenting the user an information alert
    //       (requires authScreenShowForgotPassword set to true)
    var forgotPasswordInformationAlert = true
    
    // MARK: Promote new feature within a banner below the navigation view
    var showTopBanner = true

    var showCustomServerDisplayName = true
    var customServerDisplayName = ""

    // MARK BWI show/hide developer menu
    var bwiShowDeveloperSettings = false

    // MARK BWI personal notes room
    var bwiPersonalNotesRoom = false
    var bwiPersonalNotesRoomLeavable = false
    var bwiResetPersonalNotesAccountData = false
    var bwiUseCustomPersonalNotesAvatar = true
    var bwiPersonalNotesVisibilityInSettings = false
    
    var bwiShowTimelineSettings = false
    
    // #5506 alway use the latest avatar for data privacy
    var enableRoomScreenUseOnlyLatestUserAvatarAndNameByDefault = true
    
    // MARK BWI personal state
    var bwiPersonalState = false

    // MARK BWI personal notes room
    var bwiRollsAndRights = true

    // MARK: Timeline
    var settingsScreenShowSimpleTimeLineOptions = false
    var settingsScreenShowTimeStampOption = true
    var settingsScreenShowDeletedMessagesOption = false
    var settingsScreenShowNameChangeOption = false
    var settingsScreenShowChatEffectsOption = false
    var settingsScreenShowRoomAvatarChangeOption = false
    var settingsScreenShowUserAvatarChangeOption = true
    var settingsScreenShowEnterRoomOption = true
    
    var bwiLastAdminCanDowngradeHimself = false

    var bwiEnableRegisterInfo = false
    
    var bwiShowHappyBirthdayCampaign = false
    var bwiHappyBirthdayCampaignIdentifier: String = "2025"
    var bwiDisableSecuritySettingsUntrustedDevices = true
    
    
    // MARK: - analytics with matomo
    var bwiMatomoTrackingDefaultState = false
    var bwiMatomoEnabled = false
    var matomoNameProd = ""
    var matomoNameBeta = ""
    var matomoServerProd = ""
    
    var bwiShowNewFeatures = true
            
    // MARK: - Message Bubbles bwi show in our menu and only for beta builds
    
    var bwiShowMessageBubbleSettings = false

    // bwi disable encrypted option in message context menu
    var roomContextualMenuShowEncryptionOption = false
    
    // bwi show new session manager
    var showSessionManager = false

    // bwi disable side menu coach message
    var showSideMenuCoachMessage = false
    
    var roomMembersAllowUserVerification = false
    
    // one flag for all layout changes to the element login flow
    var bumLoginFlowLayout = true
    
    // one flag for app specific layout changes
    var bwiLoginFlowLayout = true
    
    // website for users in public service that want a backend
    var bumAdvertizementURLString = "https://messenger.bwi.de/ich-will-bum";
    
    // internal html page for a listing of new features in the latest versions
    var newFeaturesHTML = "new_features"
    
    // handling of new layout
    var enableSpaces = false
    var filtersActiveByDefault = true
    var shouldShowMayorUpdate = false
    var shouldShowAllChatsOnboarding = false


    // MARK: - Netiguette
    // bwi flag for showing netiquette in settings
    var bwiShowNetiquetteInfos = false
    // internal html page for netiquette in en and de
    var netiquetteEnHTML = "netiquette_en"
    var netiquetteDeHTML = "netiquette_de"

    // MARK: - Scan server qr code
    var allowScanServerQRCode = true
    
    // MARK: - Login with qr code
    var allowLoginWithQR = true   // should be set by the server but we disable it with false also in the app

    // MARK: - Scan permalink qr code
    var clientPermalinkBaseUrl = ""
    var allowScanPermalinkQRCode = false
    var showMyQRCode = false
    
    // MARK: - Maintenance
    var enableMaintenanceInfoOnLogin = true
    var showMaintenanceInfoMessageType = true
    var ignoreBlockingMaintenance = false
    
    // MARK: User Search
    var sortUserSearchResultsAlphabetically = true
    
    // MARK: Permalinks
    var permalinkPrefixSettings = false
    var permalinkPrefixes: [String] = []

    // MARK: Client error search term
    var clientErrorSearchTerm = "Request failed: client error"
    
    // MARK: Device Manager
    var deviceManagerShowIPAddress = false
    
    // MARK: Rust Encryption
    var useRustEncryption = false
    
    // MARK: Color Theme
    var useNewBumColors = false

    // MARK: Sessions Manager
    var enableNewSessionManagerByDefault = false
    
    // MARK: Accessibility declaration
    // bwi flag for showing accessibility declaration on login screen and in settings
    var bwiShowAccessibilityDeclaration = false
    // internal markdown file for accessibility declaration in en and de.
    var accessibilityDeclarationFileDe = ""
    var accessibilityDeclarationFileEn = ""

    // MARK: Voice Broadcast
    var enableFeatureVoiceBroadcastsByDefault = false
    
    // MARK: WYSIWYG
    var enableFeatureWYSIWYGByDefault = false
    var bwiWYSIWYGVisibilityInSettings = true 
    var enableWYSIWYGCommands = false
    
    // MARK: itunes
    var itunesAppLink = ""
    
    // MARK: itunes
    var showAllChatsFilterMenu = false
    
    // MARK: itunes
    var roomFiltersToggle = true

    // MARK: OIDC
    var isOIDCEnabled = true
    
    // MARK: Change Password
    var showPasswordChangedConfirmation = true

    // MARK: Create Room Menu
    var enableAllChatsToolbar = false
    
    // MARK: App Config
    var avoidServerSelectionOnAppConfig = false
    
    // MARK: Content Scanner Status Thumbnail
    var showContentScannerStatusWithFilename = true
    
    // MARK: Federation
    // Enables federation functions and views
    var isFederationEnabled: Bool {
        return (isWellknownFederationEnabled || isLabsFederationEnabled)
    }
    
    // Federation status from wellknown config
    @UserDefault(key: UserDefaultsKeys.isWellknownFederationEnabled, defaultValue: false, storage: RiotSettings.defaults)
    var isWellknownFederationEnabled
    
    // Federation status from labs setting
    @UserDefault(key: UserDefaultsKeys.isLabsFederationEnabled, defaultValue: false, storage: RiotSettings.defaults)
    var isLabsFederationEnabled
    
    // shows the grey/green/red shield for the room avatar / user avatar
    var showEncryptionStatusBadgeOnAvatar = false
    
    // MARK: Backup restore
    var enableRestoreKeysFromBackup = true
    
    // MARK: Backup restore
    var showMatrixIDinRoomInfoScreen = false

    
    // MARK: New photos picker API
    var useNewPhotosPickerAPI = true

    // MARK: Contacts List
    var showContactIdentifierInDetailRow = true
    
    // MARK: Room Participants
    // activates swipe gesture action to remove participants from room (RoomParticipantsViewController)
    var bwiCanEditRoomParticipants = false
    
    // MARK: Profile picture hint
    // (only when federation is enabled)
    var showChangeProfilePictureHint = false
    
    // MARK: Federation Introduction
    // (only when federation is enabled in the client and well-known flag is set)
    var showFederationIntroduction = false
    
    // MARK: Change Password
    var showPasswordRequirements = false
    
    // MARK: Report Room
    var showReportRoomButton = false
    
    // MARK: Enable NSFW filter
    var enableNSFWFilter = false
    
    // MARK: Enable Room Retention
    var enableRoomRetention = false
}
