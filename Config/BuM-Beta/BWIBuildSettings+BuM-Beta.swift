//
/*
 * Copyright (c) 2022 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation

extension BWIBuildSettings {
    
    func overrideTargetSpecificSettings() {
        useNewBumColors = true
        secondaryAppName = "BundesMessenger"
        settingsScreenShowLabSettings = true
        authScreenShowRegister = true
        bwiShowDeveloperSettings = true
        bwiPersonalState = true
        bwiMatomoTrackingDefaultState = false
        showSessionManager = false
        locationSharingEnabled = true
        bwiLocationShareButtonVisible = false
        bwiLoginFlowLayout = false
        useRustEncryption = true
        bwiNotificationTimes = true
        enableNewSessionManagerByDefault = true
        showMaintenanceInfoMessageType = true
        ignoreBlockingMaintenance = true
        bwiUseWellKnownPrivacyPolicyLink = true
        showChangeProfilePictureHint = true
        showFederationIntroduction = false
        
        itunesAppLink = "https://apps.apple.com/de/app/bundesmessenger/id1616866351"
        avoidServerSelectionOnAppConfig = true
        enableFeatureWYSIWYGByDefault = true
        bwiEnableTokenizedLoginProtection = true
        bwiShowHappyBirthdayCampaign = true
    }
}
