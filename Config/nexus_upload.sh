#!/usr/bin/env bash
NEXUS_BASE_URL=$1
VARIANT=$2
EXT=$3
VERSION=$4
DATE_STR=$5
GITHASH=$6
VERSION_TAG=$7
SUBDIR=$8

echo "params: $NEXUS_BASE_URL $VARIANT $EXT $VERSION $DATE_STR $GITHASH $SUBDIR $VERSION_TAG"
echo "dir: $PWD"

check_last_exit_code () {
    if [ $1 -ne 0 ]; then
        echo "Error: exit code != 0"
        exit $1
    fi
}

echo "checking if file is found"
find ../out$SUBDIR -name "*$VARIANT*.$EXT" | grep .
check_last_exit_code $?
echo "find end"

find ../out$SUBDIR -name "*$VARIANT*.$EXT" -print0 | while read -d $'\0' file
do
  echo "Source: $file"
  zip -r $file.zip $file
  BASENAME=$(basename $file .zip)-$VERSION_TAG-$DATE_STR-$GITHASH.$EXT$SUBDIR.zip
  echo "Destination $NEXUS_BASE_URL/$VERSION/$BASENAME"
  curl --fail -v -u $S_BWMESSENGER_ID:$S_BWMESSENGER_PASSWORD  --upload-file $file.zip $NEXUS_BASE_URL/$VERSION/$BASENAME
  check_last_exit_code $?
done
check_last_exit_code $?
