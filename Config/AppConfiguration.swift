// 
// Copyright 2024 New Vector Ltd.
// Copyright 2020 Vector Creations Ltd
// Copyright (c) 2021 BWI GmbH
//
// SPDX-License-Identifier: AGPL-3.0-only
// Please see LICENSE in the repository root for full details.
//

import Foundation

/// AppConfiguration is CommonConfiguration plus configurations dedicated to the app
class AppConfiguration: CommonConfiguration {
    
    // MARK: - Global settings
    
    override func setupSettings() {
        super.setupSettings()
        setupAppSettings()
    }
    
    private func setupAppSettings() {
        // Enable CallKit for app
        MXKAppSettings.standard()?.isCallKitEnabled = true
        
        // Get modular widget events in rooms histories
        // bwi: add additional event for nicknames
        MXKAppSettings.standard()?.addSupportedEventTypes([kWidgetMatrixEventTypeString,
                                                           kWidgetModularEventTypeString,
                                                           BWIBuildSettings.shared.bwiUserLabelEventTypeString,
                                                          VoiceBroadcastSettings.voiceBroadcastInfoContentKeyType])
        
        // Hide undecryptable messages that were sent while the user was not in the room
        MXKAppSettings.standard()?.hidePreJoinedUndecryptableEvents = true
        
        // Enable long press on event in bubble cells
        MXKRoomBubbleTableViewCell.disableLongPressGesture(onEvent: false)
        
        // Each room member will be considered as a potential contact.
        MXKContactManager.shared().contactManagerMXRoomSource = MXKContactManagerMXRoomSource.all
        
        // Use UIKit BackgroundTask for handling background tasks in the SDK
        MXSDKOptions.sharedInstance().backgroundModeHandler = MXUIKitBackgroundModeHandler()
        
        // Enable key backup on app
        MXSDKOptions.sharedInstance().enableKeyBackupWhenStartingMXCrypto = true
        
        // bwi: explicitly set option for key sharing
        MXSDKOptions.sharedInstance().enableRoomSharedHistoryOnInvite = BWIBuildSettings.shared.allowKeySharingOnRoomInvite
        
        // bwi: #4941 deactivate voice broadcast
        RiotSettings.shared.enableVoiceBroadcast = BWIBuildSettings.shared.enableFeatureVoiceBroadcastsByDefault
        
        // bwi: #5506 activate use latest avatar
        RiotSettings.shared.roomScreenUseOnlyLatestUserAvatarAndName = BWIBuildSettings.shared.enableRoomScreenUseOnlyLatestUserAvatarAndNameByDefault
    }
    
    
    // MARK: - Per matrix session settings
    
    override func setupSettings(for matrixSession: MXSession) {
        super.setupSettings(for: matrixSession)
        setupWidgetReadReceipts(for: matrixSession)
        
        if BWIBuildSettings.shared.enableAntivirusScan, let homeServerStringURL = matrixSession.credentials?.homeServer {
            matrixSession.antivirusServerURL = homeServerStringURL
        }
    }
  
    private func setupWidgetReadReceipts(for matrixSession: MXSession) {
        var acknowledgableEventTypes = matrixSession.acknowledgableEventTypes ?? []
        acknowledgableEventTypes.append(kWidgetMatrixEventTypeString)
        acknowledgableEventTypes.append(kWidgetModularEventTypeString)

        matrixSession.acknowledgableEventTypes = acknowledgableEventTypes
    }
    
}
