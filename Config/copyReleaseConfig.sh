#!/bin/sh

#  setConfig.sh
#  Riot
#
#  Created by Frank Rotermund on 25.03.21.
#  Copyright © 2021 matrix.org. All rights reserved.

cp -vf ../Config/BuM/AppIdentifiers-bum.xcconfig ../Config/AppIdentifiers.xcconfig
