/*
Copyright 2024 New Vector Ltd.
Copyright 2016 OpenMarket Ltd
Copyright (c) 2021 BWI GmbH

SPDX-License-Identifier: AGPL-3.0-only
Please see LICENSE in the repository root for full details.
 */

#import "Tools.h"
#import "GeneratedInterface-Swift.h"
#import "MXKSwiftHeader.h"

#define MAX_BWI_IMAGE_SIZE 1600

@implementation Tools

+ (NSString *)presenceText:(MXUser *)user
{
    NSString* presenceText = [VectorL10n roomParticipantsUnknown];

    if (user)
    {
        switch (user.presence)
        {
            case MXPresenceOnline:
                presenceText = [VectorL10n roomParticipantsOnline];
                break;

            case MXPresenceUnavailable:
                presenceText = [VectorL10n roomParticipantsIdle];
                break;
                
            case MXPresenceUnknown:
                // Fix https://github.com/vector-im/element-ios/issues/6597
                // Return nil because we don't want to display anything if the status is unknown
                return nil;
                
            case MXPresenceOffline:
                presenceText = [VectorL10n roomParticipantsOffline];
                break;
                
            default:
                break;
        }
        
        if (user.currentlyActive)
        {
            presenceText = [presenceText stringByAppendingString:[NSString stringWithFormat:@" %@",[VectorL10n roomParticipantsNow]]];
        }
        else if (-1 != user.lastActiveAgo && 0 < user.lastActiveAgo)
        {
            presenceText = [presenceText stringByAppendingString:[NSString stringWithFormat:@" %@ %@",
                                                                  [MXKTools formatSecondsIntervalFloored:(user.lastActiveAgo / 1000)],
                                                                  [VectorL10n roomParticipantsAgo]]];
        }
    }

    return presenceText;
}

#pragma mark - Universal link

+ (BOOL)isUniversalLink:(NSURL*)url
{
    BOOL isUniversalLink = NO;
    
    for (NSString *matrixPermalinkHost in BWIBuildSettings.shared.permalinkSupportedHosts)
    {
        if ([url.host isEqualToString:matrixPermalinkHost])
        {
            NSArray<NSString*> *hostPaths = BWIBuildSettings.shared.permalinkSupportedHosts[matrixPermalinkHost];
            if (hostPaths.count)
            {
                // iOS Patch: fix urls before using it
                NSURL *fixedURL = [Tools fixURLWithSeveralHashKeys:url];
                
                if (NSNotFound != [hostPaths indexOfObject:fixedURL.path])
                {
                    isUniversalLink = YES;
                    break;
                }
            }
            else
            {
                isUniversalLink = YES;
                break;
            }
        }
    }
    
    // bwi: 5603 fix link in reply to - handle in app
    if ([url.host isEqualToString:[AppConfigService.shared permalinkHost]]) {
        isUniversalLink = YES;
    } 
    else
    {
        NSURLComponents *components = [[NSURLComponents alloc] initWithString:[MXSDKOptions sharedInstance].clientPermalinkBaseUrl];
        if (components)
        {
            if (components.host) 
            {
                if ([url.host isEqualToString:components.host]) {
                    isUniversalLink = YES;
                }
            }
        }
    }

    return isUniversalLink;
}

+ (NSURL *)fixURLWithSeveralHashKeys:(NSURL *)url
{
    NSURL *fixedURL = url;

    // The NSURL may have no fragment because it contains more that '%23' occurence
    if (!url.fragment)
    {
        // Replacing the first '%23' occurence into a '#' makes NSURL works correctly
        NSString *urlString = url.absoluteString;
        NSRange range = [urlString rangeOfString:@"%23"];
        if (NSNotFound != range.location)
        {
            urlString = [urlString stringByReplacingCharactersInRange:range withString:@"#"];
            fixedURL = [NSURL URLWithString:urlString];
        }
    }

    return fixedURL;
}

#pragma mark - String utilities

+ (NSAttributedString *)setTextColorAlpha:(CGFloat)alpha inAttributedString:(NSAttributedString*)attributedString
{
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithAttributedString:attributedString];

    // Check all attributes one by one
    [string enumerateAttributesInRange:NSMakeRange(0, attributedString.length) options:0 usingBlock:^(NSDictionary *attrs, NSRange range, BOOL *stop)
     {
         // Replace only colored texts
         if (attrs[NSForegroundColorAttributeName])
         {
             UIColor *color = attrs[NSForegroundColorAttributeName];
             color = [color colorWithAlphaComponent:alpha];

             NSMutableDictionary *newAttrs = [NSMutableDictionary dictionaryWithDictionary:attrs];
             newAttrs[NSForegroundColorAttributeName] = color;

             [string setAttributes:newAttrs range:range];
         }
     }];

    return string;
}

#pragma mark - NSData utilities

+ (NSData*)mediaConvertMaxImageData:(NSData*) imageData withUTI:(MXKUTI *)uti
{
    NSData *convertedData;
    
    /**
      * Reduce the image size to MAX_BWI_IMAGE_SIZE to be conform
      * with the BWI back-end server system.
      */
     CGSize newImageSize = CGSizeMake(MAX_BWI_IMAGE_SIZE, MAX_BWI_IMAGE_SIZE);
     UIImage* convertedImage = [MXKTools resizeImageWithData:imageData toFitInSize:newImageSize];
     
     if (convertedImage)
     {
         if ([uti.mimeType isEqualToString:@"image/png"])
         {
             convertedData = UIImagePNGRepresentation(convertedImage);
         }
         else if ([uti.mimeType isEqualToString:@"image/jpeg"])
         {
             convertedData = UIImageJPEGRepresentation(convertedImage, 0.9);
         }
         else
         {
             // Return the original image data pointer
             return imageData;
         }
     }
    
    return convertedData;
}

#pragma mark - Time utilities

+ (uint64_t)durationInMsFromDays:(uint)days
{
    return days * (uint64_t)(86400000);
}

@end
