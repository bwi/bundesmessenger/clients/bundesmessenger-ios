//
// Copyright 2020-2024 New Vector Ltd.
// Copyright (c) 2021 BWI GmbH
//
// SPDX-License-Identifier: AGPL-3.0-only
// Please see LICENSE in the repository root for full details.
//

import Foundation

enum DiscussionType {
    case directMessage
    case multipleDirectMessage
    case room(topic: String?, canInvitePeople: Bool)
    case personalNotes
}

struct RoomCreationIntroViewData {
    let dicussionType: DiscussionType
    let roomDisplayName: String
    let avatarViewData: RoomAvatarViewData
}
