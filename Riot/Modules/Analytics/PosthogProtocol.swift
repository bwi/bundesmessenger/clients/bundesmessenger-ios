// 
// Copyright 2024 New Vector Ltd.
//
// SPDX-License-Identifier: AGPL-3.0-only
// Please see LICENSE in the repository root for full details.
//

import Foundation
#if POSTHOG
import PostHog
#endif

protocol PostHogProtocol {
    func optIn()
    
    func optOut()
    
    func reset()
    
    func flush()
    
    func capture(_ event: String, properties: [String: Any]?, userProperties: [String: Any]?)
    
    func screen(_ screenTitle: String, properties: [String: Any]?)
    
    func isFeatureEnabled(_ feature: String) -> Bool
    
    func identify(_ distinctId: String)
    
    func identify(_ distinctId: String, userProperties: [String: Any]?)
    
    func isOptOut() -> Bool
}

protocol PostHogFactory {
#if POSTHOG
    func createPostHog(config: PostHogConfig) -> PostHogProtocol
#endif
}

class DefaultPostHogFactory: PostHogFactory {
#if POSTHOG
    func createPostHog(config: PostHogConfig) -> PostHogProtocol {
        PostHogSDK.shared.setup(config)
        return PostHogSDK.shared
    }
#endif
}

#if POSTHOG
extension PostHogSDK: PostHogProtocol { }
#endif
