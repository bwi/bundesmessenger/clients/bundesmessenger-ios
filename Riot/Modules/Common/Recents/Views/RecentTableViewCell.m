/*
Copyright 2024 New Vector Ltd.
Copyright 2017 Vector Creations Ltd
Copyright 2015 OpenMarket Ltd

SPDX-License-Identifier: AGPL-3.0-only
Please see LICENSE in the repository root for full details.
 */

#import "RecentTableViewCell.h"

#import "AvatarGenerator.h"

#import "MXEvent.h"
#import "MXRoom+Riot.h"

#import "ThemeService.h"
#import "GeneratedInterface-Swift.h"

#import "MXRoomSummary+Riot.h"

@implementation RecentTableViewCell

#pragma mark - Class methods

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    // Initialize unread count badge
    [_missedNotifAndUnreadBadgeBgView.layer setCornerRadius:10];
    _missedNotifAndUnreadBadgeBgViewWidthConstraint.constant = 0;
    
    // bwi: 5203 add federation icon
    self.federationImage = FederationIconHelper.shared.federationBadgeImage;
    self.federationImageView = [[UIImageView alloc] initWithImage:self.federationImage];
    self.federationImageView.translatesAutoresizingMaskIntoConstraints = NO;
    self.federationImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.contentView addSubview:self.federationImageView];
    
    // scale and new with federation icon
    float scaleFactor = ((self.roomAvatar.frame.size.height * 0.34) / self.federationImage.size.height);
    float imageWidth = scaleFactor * self.federationImage.size.width;
    // offset federation icon    
    float offsetTrailing = 3 * scaleFactor;
    float offsetBotom = 1;

    
    [self.federationImageView.bottomAnchor constraintEqualToAnchor:self.roomAvatar.bottomAnchor constant:offsetBotom].active = YES;
    [self.federationImageView.trailingAnchor constraintEqualToAnchor:self.roomAvatar.trailingAnchor constant:offsetTrailing].active = YES;
    [self.federationImageView.heightAnchor constraintEqualToConstant:(self.roomAvatar.frame.size.height * 0.34)].active = YES;
    [self.federationImageView.widthAnchor constraintEqualToConstant:imageWidth].active = YES;
}

- (void)customizeTableViewCellRendering
{
    [super customizeTableViewCellRendering];
    
    self.contentView.backgroundColor = ThemeService.shared.theme.backgroundColor;
    self.roomTitle.textColor = ThemeService.shared.theme.textPrimaryColor;
    self.lastEventDescription.textColor = ThemeService.shared.theme.textSecondaryColor;
    self.lastEventDate.textColor = ThemeService.shared.theme.textSecondaryColor;
    self.missedNotifAndUnreadBadgeLabel.textColor = ThemeService.shared.theme.baseTextPrimaryColor;
    self.presenceIndicatorView.borderColor = ThemeService.shared.theme.backgroundColor;
    
    self.roomAvatar.defaultBackgroundColor = [UIColor clearColor];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    // Round image view
    [_roomAvatar.layer setCornerRadius:_roomAvatar.frame.size.width / 2];
    _roomAvatar.clipsToBounds = YES;
    
    // bwi: 5203 add federation icon
    self.federationImage = FederationIconHelper.shared.federationBadgeImage;
    self.federationImageView.image = self.federationImage;
}

- (void)render:(MXKCellData *)cellData
{
    // Hide by default missed notifications and unread widgets
    self.missedNotifAndUnreadIndicator.hidden = YES;
    self.missedNotifAndUnreadBadgeBgView.hidden = YES;
    self.missedNotifAndUnreadBadgeBgViewWidthConstraint.constant = 0;
    self.missedNotifAndUnreadBadgeLabel.text = @"";
    
    roomCellData = (id<MXKRecentCellDataStoring>)cellData;
    if (roomCellData)
    {
        
        // bwi: 5203 add federation icon
        if (BWIBuildSettings.shared.isFederationEnabled)
        {
            MXRoom *room = [roomCellData.mxSession roomWithRoomId:roomCellData.roomSummary.roomId];
            if (room.isPersonalNotesRoom)
            {
                self.federationImageView.hidden = YES;
            }
            else
            {
                if (room.isDirect)
                {
                    if (room.isDMFederated)
                    {
                        self.federationImageView.hidden = NO;
                    }
                    else
                    {
                        self.federationImageView.hidden = YES;
                    }
                }
                else
                {
                    [room getFederationStatusWithCompletion:^(BOOL isFederated) {
                        self.federationImageView.hidden = !isFederated;
                    }];
                    
                }
            }
        }
        else
        {
            self.federationImageView.hidden = YES;
        }
        
        // Report computed values as is
        self.roomTitle.text = roomCellData.roomDisplayname;
        self.lastEventDate.text = roomCellData.lastEventDate;
        
        // Manage lastEventAttributedTextMessage optional property
        if (!roomCellData.roomSummary.spaceChildInfo && [roomCellData respondsToSelector:@selector(lastEventAttributedTextMessage)])
        {
            // Attempt to correct the attributed string colors to match the current theme
            self.lastEventDescription.attributedText = [roomCellData.lastEventAttributedTextMessage fixForegroundColor];
        }
        else
        {
            self.lastEventDescription.text = roomCellData.lastEventTextMessage;
        }

        self.unsentImageView.hidden = roomCellData.roomSummary.sentStatus == MXRoomSummarySentStatusOk;
        self.lastEventDecriptionLabelTrailingConstraint.constant = self.unsentImageView.hidden ? 10 : 30;

        // Notify unreads and bing
        if (roomCellData.hasUnread)
        {
            self.missedNotifAndUnreadIndicator.hidden = NO;
            if (0 < roomCellData.notificationCount)
            {
                if( [BWIBuildSettings.shared showMentionsInRoom] ) {
                    self.missedNotifAndUnreadIndicator.backgroundColor = [Mentions countMentionsWithRoomID:roomCellData.roomIdentifier] > 0 ? ThemeService.shared.theme.noticeColor : ThemeService.shared.theme.noticeSecondaryColor;
                } else {
                    self.missedNotifAndUnreadIndicator.backgroundColor = roomCellData.highlightCount ? ThemeService.shared.theme.noticeColor : ThemeService.shared.theme.noticeSecondaryColor;
                }

                self.missedNotifAndUnreadBadgeBgView.hidden = NO;
                self.missedNotifAndUnreadBadgeBgView.backgroundColor = self.missedNotifAndUnreadIndicator.backgroundColor;

                self.missedNotifAndUnreadBadgeLabel.text = roomCellData.notificationCountStringValue;
                [self.missedNotifAndUnreadBadgeLabel sizeToFit];

                self.missedNotifAndUnreadBadgeBgViewWidthConstraint.constant = self.missedNotifAndUnreadBadgeLabel.frame.size.width + 18;
            }
            else
            {
                self.missedNotifAndUnreadIndicator.backgroundColor = ThemeService.shared.theme.unreadRoomIndentColor;
            }

            // Use bold font for the room title
            self.roomTitle.font = [UIFont systemFontOfSize:17 weight:UIFontWeightBold];
        }
        else
        {
            self.lastEventDate.textColor = ThemeService.shared.theme.textSecondaryColor;

            // The room title is not bold anymore
            self.roomTitle.font = [UIFont systemFontOfSize:17 weight:UIFontWeightMedium];
        }

        // bwi: if the room is a personal notes room a local image is used as room avatar
        PersonalNotesDefaultService *service = [[PersonalNotesDefaultService alloc] initWithMxSession:roomCellData.mxSession];
        if (BWIBuildSettings.shared.bwiUseCustomPersonalNotesAvatar && [roomCellData.roomIdentifier isEqualToString:[service personalNotesRoomId]]) {
            self.roomAvatar.image = [UIImage imageNamed:[service avatarImageUrl]];
        } else {
            [self.roomAvatar vc_setRoomAvatarImageWith:roomCellData.avatarUrl
                                                roomId:roomCellData.roomIdentifier
                                           displayName:roomCellData.roomDisplayname
                                          mediaManager:roomCellData.mxSession.mediaManager];
        }

        if (roomCellData.directUserId)
        {
            [self.presenceIndicatorView configureWithUserId:roomCellData.directUserId presence:roomCellData.presence];
        }
        else
        {
            [self.presenceIndicatorView stopListeningPresenceUpdates];
        }
    }
    else
    {
        self.lastEventDescription.text = @"";
    }
}

- (void)prepareForReuse
{
    [super prepareForReuse];

    [self.presenceIndicatorView stopListeningPresenceUpdates];
}

+ (CGFloat)heightForCellData:(MXKCellData *)cellData withMaximumWidth:(CGFloat)maxWidth
{
    // The height is fixed
    return 74;
}

@end
