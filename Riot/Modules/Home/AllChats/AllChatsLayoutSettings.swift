// 
// Copyright 2022-2024 New Vector Ltd.
//
// SPDX-License-Identifier: AGPL-3.0-only
// Please see LICENSE in the repository root for full details.
//

import Foundation

// bwi: we use an additional section and have specific settings we want to apply on first start of the app

@objcMembers
class AllChatsLayoutSettings: NSObject, NSCoding {
    
    fileprivate enum Constants {
        static let sectionsKey = "sections"
        static let filtersKey = "filters"
        static let sortingKey = "sorting"
    }
    
    let sections: AllChatsLayoutSectionType
    let filters: AllChatsLayoutFilterType
    let sorting: AllChatsLayoutSortingType
    
    init(sections: AllChatsLayoutSectionType = [],
         filters: AllChatsLayoutFilterType = [],
         sorting: AllChatsLayoutSortingType = .activity) {        
        if !UserDefaults.standard.bool(forKey: "FirstStartWithNewSettings") {
            if PersonalNotesSettings().personalNotesVisible && BWIBuildSettings.shared.bwiPersonalNotesRoom {
                self.sections = [.bwiPersonalNotes]
            } else {
                self.sections = []
            }
            self.filters = [.unreads, .favourites, .people]
            self.sorting = .activity
            UserDefaults.standard.set(true, forKey: "FirstStartWithNewSettings")
        } else {
            self.sections = sections
            self.filters = filters
            self.sorting = sorting
        }
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(Int(sections.rawValue), forKey: Constants.sectionsKey)
        coder.encode(Int(filters.rawValue), forKey: Constants.filtersKey)
        coder.encode(Int(sorting.rawValue), forKey: Constants.sortingKey)
    }
    
    required init?(coder: NSCoder) {
        // bwi: there are cases when only init with coder is called, so use our first start settings here, too
        if !UserDefaults.standard.bool(forKey: "FirstStartWithNewSettings") {
            if PersonalNotesSettings().personalNotesVisible && BWIBuildSettings.shared.bwiPersonalNotesRoom {
                self.sections = [.bwiPersonalNotes]
            } else {
                self.sections = []
            }
            self.filters = [.unreads, .favourites, .people]
            self.sorting = .activity
            UserDefaults.standard.set(true, forKey: "FirstStartWithNewSettings")
        } else {
            self.sections = AllChatsLayoutSectionType(rawValue: UInt(coder.decodeInteger(forKey: Constants.sectionsKey)))
            self.filters = AllChatsLayoutFilterType(rawValue: UInt(coder.decodeInteger(forKey: Constants.filtersKey)))
            self.sorting = AllChatsLayoutSortingType(rawValue: UInt(coder.decodeInteger(forKey: Constants.sortingKey))) ?? .activity
        }
    }
}
