// swiftlint:disable all
// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Strings

// swiftlint:disable function_parameter_count identifier_name line_length type_body_length
@objcMembers
public class BWIL10n: NSObject {
  /// Notizenraum anzeigen
  public static var allChatsEditLayoutShowPersonalNotes: String { 
    return BWIL10n.tr("Bwi", "all_chats_edit_layout_show_personal_notes") 
  }
  /// Suche
  public static var allChatsSearchbarPrompt: String { 
    return BWIL10n.tr("Bwi", "all_chats_searchbar_prompt") 
  }
  /// Passwort vergessen?
  public static var authForgotPassword: String { 
    return BWIL10n.tr("Bwi", "auth_forgot_password") 
  }
  /// Verbinde mit BwMessenger
  public static var authLoginHeadlineText: String { 
    return BWIL10n.tr("Bwi", "auth_login_headline_text") 
  }
  /// Dein Messenger für unser Land
  public static var authLoginSubheadlineText: String { 
    return BWIL10n.tr("Bwi", "auth_login_subheadline_text") 
  }
  /// Benutzername
  public static var authUserIdPlaceholder: String { 
    return BWIL10n.tr("Bwi", "auth_user_id_placeholder") 
  }
  /// Datenschutzhinweisen
  public static var authenticationDataprivacyLink: String { 
    return BWIL10n.tr("Bwi", "authentication_dataprivacy_link") 
  }
  /// Hinweise zur Verarbeitung Ihrer Daten und Ihrer Rechte erhalten Sie in unseren 
  public static var authenticationDataprivacyText: String { 
    return BWIL10n.tr("Bwi", "authentication_dataprivacy_text") 
  }
  /// Mit deinem Konto anmelden
  public static var authenticationLoginDescription: String { 
    return BWIL10n.tr("Bwi", "authentication_login_description") 
  }
  /// Nutzername
  public static var authenticationLoginUsername: String { 
    return BWIL10n.tr("Bwi", "authentication_login_username") 
  }
  /// Verbindung fehlgeschlagen
  public static var authenticationQrLoginFailureTitle: String { 
    return BWIL10n.tr("Bwi", "authentication_qr_login_failure_title") 
  }
  /// Anmelden mit QR-Code
  public static var authenticationQrLoginScanTitle: String { 
    return BWIL10n.tr("Bwi", "authentication_qr_login_scan_title") 
  }
  /// QR-Code scannen
  public static var authenticationQrLoginStartButtonTitle: String { 
    return BWIL10n.tr("Bwi", "authentication_qr_login_start_button_title") 
  }
  /// Anmelden mit QR-Code
  public static var authenticationQrLoginStartTitle: String { 
    return BWIL10n.tr("Bwi", "authentication_qr_login_start_title") 
  }
  /// Willkommen!
  public static var authenticationServerSelectionLoginTitle: String { 
    return BWIL10n.tr("Bwi", "authentication_server_selection_login_title") 
  }
  /// Gehe in die Einstellungen deines iPhones, um der App den Zugriff auf die Kamera zu erlauben.
  public static var authenticationServerSelectionQrMissingAuthorizationMessage: String { 
    return BWIL10n.tr("Bwi", "authentication_server_selection_qr_missing_authorization_message") 
  }
  /// Scannen nicht möglich
  public static var authenticationServerSelectionQrMissingAuthorizationTitle: String { 
    return BWIL10n.tr("Bwi", "authentication_server_selection_qr_missing_authorization_title") 
  }
  /// QR-Code einlesen
  public static var authenticationServerSelectionScanCodeButtonTitle: String { 
    return BWIL10n.tr("Bwi", "authentication_server_selection_scan_code_button_title") 
  }
  /// Website
  public static var authenticationServerSelectionServerDeniedAdvertizementWebsiteButton: String { 
    return BWIL10n.tr("Bwi", "authentication_server_selection_server_denied_advertizement_website_button") 
  }
  /// Dein Server ist leider noch nicht für den BundesMessenger eingerichtet. Wenn Du aus der Öffentlichen Verwaltung bist und Fragen hast, wie du den BundesMessenger nutzen kannst, besuche unsere Webseite.
  public static var authenticationServerSelectionServerDeniedMessage: String { 
    return BWIL10n.tr("Bwi", "authentication_server_selection_server_denied_message") 
  }
  /// Unbekannte URL
  public static var authenticationServerSelectionServerDeniedTitle: String { 
    return BWIL10n.tr("Bwi", "authentication_server_selection_server_denied_title") 
  }
  /// Server-URL
  public static var authenticationServerSelectionServerUrl: String { 
    return BWIL10n.tr("Bwi", "authentication_server_selection_server_url") 
  }
  /// Anmelden
  public static var authenticationServerSelectionSubmitButtonTitle: String { 
    return BWIL10n.tr("Bwi", "authentication_server_selection_submit_button_title") 
  }
  /// Die Verbindung konnte nicht in der erforderlichen Zeit hergestellt werden. Versuche es erneut.
  public static var authenticationQrLoginFailureRequestTimedOut: String { 
    return BWIL10n.tr("Bwi", "authenticationQrLoginFailureRequestTimedOut") 
  }
  /// PIN eingeben
  public static var biometricsModeCantUnlockButtonTitle: String { 
    return BWIL10n.tr("Bwi", "biometrics_mode_cant_unlock_button_title") 
  }
  /// Zurück
  public static var blockingDowntimeAlertDismissButton: String { 
    return BWIL10n.tr("Bwi", "blocking_downtime_alert_dismiss_button") 
  }
  /// Ignorieren
  public static var blockingDowntimeAlertIgnoreButton: String { 
    return BWIL10n.tr("Bwi", "blocking_downtime_alert_ignore_button") 
  }
  /// Der angegebene Server ist nicht für die Nutzung mit %@ zugelassen.
  public static func bumAuthenticationProtectionError(_ p1: String) -> String {
    return BWIL10n.tr("Bwi", "bum_authentication_protection_error", p1)
  }
  /// Wenn du einen Account auf einem Homeserver eingerichtet hast, benutze deine Matrix-ID (z.B. @benutzer:domain.com) und Passwort
  public static var bumAutheticationDescription: String { 
    return BWIL10n.tr("Bwi", "bum_authetication_description") 
  }
  /// Einloggen mit Matrix-ID
  public static var bumAutheticationTitle: String { 
    return BWIL10n.tr("Bwi", "bum_authetication_title") 
  }
  /// Barrierefreiheitserklärung
  public static var bwiAccessibilityDeclarationButtonTitle: String { 
    return BWIL10n.tr("Bwi", "bwi_accessibility_declaration_button_title") 
  }
  /// Labor ist jetzt verfügbar
  public static var bwiActivateLabsAlertTitle: String { 
    return BWIL10n.tr("Bwi", "bwi_activate_labs_alert_title") 
  }
  /// Wir brauchen Deine Hilfe, um Fehler im %@ besser analysieren zu können. Dazu würden wir gerne anonymisierte Diagnosedaten erfassen. Es werden keine Daten an Dritte übermittelt. Details findest Du in der Datenschutzerklärung.\n\nFalls Du nicht mehr mithelfen möchtest, kannst Du dies in den Einstellungen jederzeit wieder deaktivieren.\n\nMöchtest du bei der Fehler-Analyse unterstützen?
  public static func bwiAnalyticsAlertBody(_ p1: String) -> String {
    return BWIL10n.tr("Bwi", "bwi_analytics_alert_body", p1)
  }
  /// Ablehnen
  public static var bwiAnalyticsAlertCancelButton: String { 
    return BWIL10n.tr("Bwi", "bwi_analytics_alert_cancel_button") 
  }
  /// Datenschutzerklärung
  public static var bwiAnalyticsAlertInfoButton: String { 
    return BWIL10n.tr("Bwi", "bwi_analytics_alert_info_button") 
  }
  /// Zustimmen
  public static var bwiAnalyticsAlertOkButton: String { 
    return BWIL10n.tr("Bwi", "bwi_analytics_alert_ok_button") 
  }
  /// Fehleranalyse
  public static var bwiAnalyticsAlertTitle: String { 
    return BWIL10n.tr("Bwi", "bwi_analytics_alert_title") 
  }
  /// Vorauswahl
  public static var bwiAuthBetaSelectionButtonTitle: String { 
    return BWIL10n.tr("Bwi", "bwi_auth_beta_selection_button_title") 
  }
  /// Serverauswahl
  public static var bwiAuthBetaSelectionTitle: String { 
    return BWIL10n.tr("Bwi", "bwi_auth_beta_selection_title") 
  }
  /// Falls Du dein Passwort vergessen hast, wende dich an deinen Poolverwalter oder gehe auf das SelfServicePortal und setze dein Passwort zurück.
  public static var bwiAuthForgotPasswordAlertText: String { 
    return BWIL10n.tr("Bwi", "bwi_auth_forgot_password_alert_text") 
  }
  /// Du kannst Dich im IntranetBw für den BwMessenger registrieren. Hierfür benötigst Du eine aktive Personalnummer bzw. einen IAMBw-Eintrag oder die Unterstützung deines Poolverwalters.
  public static var bwiAuthRegisterAlertText: String { 
    return BWIL10n.tr("Bwi", "bwi_auth_register_alert_text") 
  }
  /// Konto erstellen
  public static var bwiAuthRegisterAlertTitle: String { 
    return BWIL10n.tr("Bwi", "bwi_auth_register_alert_title") 
  }
  /// Konto erstellen
  public static var bwiAuthRegisterButtonTitle: String { 
    return BWIL10n.tr("Bwi", "bwi_auth_register_button_title") 
  }
  /// Zum App Store
  public static var bwiDeprecatedVersionAppstoreButton: String { 
    return BWIL10n.tr("Bwi", "bwi_deprecated_version_appstore_button") 
  }
  /// Abmelden
  public static var bwiDeprecatedVersionLogoutButton: String { 
    return BWIL10n.tr("Bwi", "bwi_deprecated_version_logout_button") 
  }
  /// Du nutzt eine App-Version, die nicht mehr unterstützt wird. Bitte führe ein Update durch.
  public static var bwiDeprecatedVersionWarningMessage: String { 
    return BWIL10n.tr("Bwi", "bwi_deprecated_version_warning_message") 
  }
  /// App aktualisieren
  public static var bwiDeprecatedVersionWarningTitle: String { 
    return BWIL10n.tr("Bwi", "bwi_deprecated_version_warning_title") 
  }
  /// Du kannst eine eigene Statusnachricht setzen, wenn du dich zum Beispiel auf einer Dienstreise oder im Urlaub befindest. Der Status ist für andere Nutzer sichtbar.
  public static var bwiEditPersonalStateDescription: String { 
    return BWIL10n.tr("Bwi", "bwi_edit_personal_state_description") 
  }
  /// Mein Status
  public static var bwiEditPersonalStateMyState: String { 
    return BWIL10n.tr("Bwi", "bwi_edit_personal_state_my_state") 
  }
  /// Im Homeoffice
  public static var bwiEditPersonalStateOptionHomeoffice: String { 
    return BWIL10n.tr("Bwi", "bwi_edit_personal_state_option_homeoffice") 
  }
  /// Abwesend
  public static var bwiEditPersonalStateOptionNotAvailable: String { 
    return BWIL10n.tr("Bwi", "bwi_edit_personal_state_option_not_available") 
  }
  /// Nachricht entfernen
  public static var bwiEditPersonalStateOptionReset: String { 
    return BWIL10n.tr("Bwi", "bwi_edit_personal_state_option_reset") 
  }
  /// Auf der Arbeit
  public static var bwiEditPersonalStateOptionWork: String { 
    return BWIL10n.tr("Bwi", "bwi_edit_personal_state_option_work") 
  }
  /// Statusmeldung erstellen
  public static var bwiEditPersonalStatePlaceholder: String { 
    return BWIL10n.tr("Bwi", "bwi_edit_personal_state_placeholder") 
  }
  /// max 160 Zeichen
  public static var bwiEditPersonalStateTextLimit: String { 
    return BWIL10n.tr("Bwi", "bwi_edit_personal_state_text_limit") 
  }
  /// Statusmeldungen bearbeiten
  public static var bwiEditPersonalStateTitle: String { 
    return BWIL10n.tr("Bwi", "bwi_edit_personal_state_title") 
  }
  /// Falsches Passwort
  public static var bwiErrorInvalidPassword: String { 
    return BWIL10n.tr("Bwi", "bwi_error_invalid_password") 
  }
  /// %@ ist bereits im Raum.
  public static func bwiErrorInviteAlreadyInRoom(_ p1: String) -> String {
    return BWIL10n.tr("Bwi", "bwi_error_invite_already_in_room", p1)
  }
  /// %@ ist vom Raum gebannt.
  public static func bwiErrorInviteBannedInRoom(_ p1: String) -> String {
    return BWIL10n.tr("Bwi", "bwi_error_invite_banned_in_room", p1)
  }
  /// %@ konnte nicht eingeladen werden.
  public static func bwiErrorInviteGeneral(_ p1: String) -> String {
    return BWIL10n.tr("Bwi", "bwi_error_invite_general", p1)
  }
  /// Abmelden ist ohne Internetverbindung nicht möglich.
  public static var bwiErrorLogoutOffline: String { 
    return BWIL10n.tr("Bwi", "bwi_error_logout_offline") 
  }
  /// Nachrichten können erst nach Einladung gelesen werden.
  public static var bwiErrorRoomHistoryNotAvailable: String { 
    return BWIL10n.tr("Bwi", "bwi_error_room_history_not_available") 
  }
  /// Der Raum wurde bereits geschlossen, daher kannst Du nicht mehr beitreten.
  public static var bwiErrorRoomNotAvailableMessage: String { 
    return BWIL10n.tr("Bwi", "bwi_error_room_not_available_message") 
  }
  /// Link ungültig
  public static var bwiErrorRoomNotAvailableTitle: String { 
    return BWIL10n.tr("Bwi", "bwi_error_room_not_available_title") 
  }
  /// Übergreifende sichere Kommunikation zwischen verschiedenen Organisationen
  public static var bwiFeatureBannerAdvertisementText: String { 
    return BWIL10n.tr("Bwi", "bwi_feature_banner_advertisement_text") 
  }
  /// Föderation jetzt möglich
  public static var bwiFeatureBannerHeader: String { 
    return BWIL10n.tr("Bwi", "bwi_feature_banner_header") 
  }
  /// 
  public static var bwiFeatureBannerShowMoreButton: String { 
    return BWIL10n.tr("Bwi", "bwi_feature_banner_show_more_button") 
  }
  /// Die Ver-/Entschlüsselung von Nachrichten wird verbessert, dies kann ein paar Minuten dauern, bitte schließe die App nicht. Verbesserung läuft.
  public static var bwiLaunchLoadingCryptoStoreMigrationInfo: String { 
    return BWIL10n.tr("Bwi", "bwi_launch_loading_crypto_store_migration_info") 
  }
  /// Der angegebene Server ist nicht für die Nutzung mit dem %@ vorgesehen
  public static func bwiLoginProtectionErrorMessage(_ p1: String) -> String {
    return BWIL10n.tr("Bwi", "bwi_login_protection_error_message", p1)
  }
  /// Website
  public static var bwiLoginProtectionInfoButton: String { 
    return BWIL10n.tr("Bwi", "bwi_login_protection_info_button") 
  }
  /// Dein Server ist leider noch nicht für den %@ eingerichtet. Wenn du aus der Öffentlichen Verwaltung bist und Fragen hast, wie Du den %@ nutzen kannst, besuche unsere Website.
  public static func bwiLoginProtectionInfoMessage(_ p1: String, _ p2: String) -> String {
    return BWIL10n.tr("Bwi", "bwi_login_protection_info_message", p1, p2)
  }
  /// Die Konfiguration hat sich geändert. Bitte melde dich neu an.
  public static var bwiMdmLogoutMessage: String { 
    return BWIL10n.tr("Bwi", "bwi_mdm_logout_message") 
  }
  /// Meine Notizen
  public static var bwiNotesRoomTitle: String { 
    return BWIL10n.tr("Bwi", "bwi_notes_room_title") 
  }
  /// Benachrichtigungszeiten
  public static var bwiNotificationTimes: String { 
    return BWIL10n.tr("Bwi", "bwi_notification_times") 
  }
  /// Am %@ benachrichtigen
  public static func bwiNotificationTimesButtonActivate(_ p1: String) -> String {
    return BWIL10n.tr("Bwi", "bwi_notification_times_button_activate", p1)
  }
  /// Am %@ nicht benachrichtigen
  public static func bwiNotificationTimesButtonDeactivate(_ p1: String) -> String {
    return BWIL10n.tr("Bwi", "bwi_notification_times_button_deactivate", p1)
  }
  /// Fertig
  public static var bwiNotificationTimesDoneAction: String { 
    return BWIL10n.tr("Bwi", "bwi_notification_times_done_action") 
  }
  /// bis
  public static var bwiNotificationTimesEndTime: String { 
    return BWIL10n.tr("Bwi", "bwi_notification_times_end_time") 
  }
  /// Freitag
  public static var bwiNotificationTimesFriday: String { 
    return BWIL10n.tr("Bwi", "bwi_notification_times_friday") 
  }
  /// Fr
  public static var bwiNotificationTimesFridayShort: String { 
    return BWIL10n.tr("Bwi", "bwi_notification_times_friday_short") 
  }
  /// Montag
  public static var bwiNotificationTimesMonday: String { 
    return BWIL10n.tr("Bwi", "bwi_notification_times_monday") 
  }
  /// Mo
  public static var bwiNotificationTimesMondayShort: String { 
    return BWIL10n.tr("Bwi", "bwi_notification_times_monday_short") 
  }
  /// Samstag
  public static var bwiNotificationTimesSaturday: String { 
    return BWIL10n.tr("Bwi", "bwi_notification_times_saturday") 
  }
  /// Sa
  public static var bwiNotificationTimesSaturdayShort: String { 
    return BWIL10n.tr("Bwi", "bwi_notification_times_saturday_short") 
  }
  /// von
  public static var bwiNotificationTimesStartTime: String { 
    return BWIL10n.tr("Bwi", "bwi_notification_times_start_time") 
  }
  /// Am %@ bekommst du Benachrichtigungen
  public static func bwiNotificationTimesStatusActivated(_ p1: String) -> String {
    return BWIL10n.tr("Bwi", "bwi_notification_times_status_activated", p1)
  }
  /// Am %@ bekommst du keine Benachrichtigungen
  public static func bwiNotificationTimesStatusDeactivated(_ p1: String) -> String {
    return BWIL10n.tr("Bwi", "bwi_notification_times_status_deactivated", p1)
  }
  /// Sonntag
  public static var bwiNotificationTimesSunday: String { 
    return BWIL10n.tr("Bwi", "bwi_notification_times_sunday") 
  }
  /// So
  public static var bwiNotificationTimesSundayShort: String { 
    return BWIL10n.tr("Bwi", "bwi_notification_times_sunday_short") 
  }
  /// Donnerstag
  public static var bwiNotificationTimesThursday: String { 
    return BWIL10n.tr("Bwi", "bwi_notification_times_thursday") 
  }
  /// Do
  public static var bwiNotificationTimesThursdayShort: String { 
    return BWIL10n.tr("Bwi", "bwi_notification_times_thursday_short") 
  }
  /// Außerhalb der eingestellten Benachrichtigungszeiten erhälst du keine Benachrichtigungen. Diese Regel kannst du pro Raum und Chat ein- und ausschalten.
  public static var bwiNotificationTimesToggleDescription: String { 
    return BWIL10n.tr("Bwi", "bwi_notification_times_toggle_description") 
  }
  /// Benachrichtigungszeiten
  public static var bwiNotificationTimesToggleTitle: String { 
    return BWIL10n.tr("Bwi", "bwi_notification_times_toggle_title") 
  }
  /// Dienstag
  public static var bwiNotificationTimesTuesday: String { 
    return BWIL10n.tr("Bwi", "bwi_notification_times_tuesday") 
  }
  /// Di
  public static var bwiNotificationTimesTuesdayShort: String { 
    return BWIL10n.tr("Bwi", "bwi_notification_times_tuesday_short") 
  }
  /// Mittwoch
  public static var bwiNotificationTimesWednesday: String { 
    return BWIL10n.tr("Bwi", "bwi_notification_times_wednesday") 
  }
  /// Mi
  public static var bwiNotificationTimesWednesdayShort: String { 
    return BWIL10n.tr("Bwi", "bwi_notification_times_wednesday_short") 
  }
  /// App Update
  public static var bwiOutdatedSettingsHeader: String { 
    return BWIL10n.tr("Bwi", "bwi_outdated_settings_header") 
  }
  /// Wir empfehlen, die neuste Version des %@ herunterzuladen.
  public static func bwiOutdatedSettingsMessage(_ p1: String) -> String {
    return BWIL10n.tr("Bwi", "bwi_outdated_settings_message", p1)
  }
  /// Zum App Store
  public static var bwiOutdatedVersionAppstoreButton: String { 
    return BWIL10n.tr("Bwi", "bwi_outdated_version_appstore_button") 
  }
  /// Später
  public static var bwiOutdatedVersionLogoutButton: String { 
    return BWIL10n.tr("Bwi", "bwi_outdated_version_logout_button") 
  }
  /// Wir empfehlen die neuste Version des %@ herunterzuladen.
  public static func bwiOutdatedVersionWarningMessage(_ p1: String) -> String {
    return BWIL10n.tr("Bwi", "bwi_outdated_version_warning_message", p1)
  }
  /// App aktualisieren?
  public static var bwiOutdatedVersionWarningTitle: String { 
    return BWIL10n.tr("Bwi", "bwi_outdated_version_warning_title") 
  }
  /// Das ist dein privater Notizen-Raum. Du bist der einzige, der auf die Inhalte in diesem Raum zugreifen kann.
  public static var bwiPersonalNotesBeginningNotesRoom: String { 
    return BWIL10n.tr("Bwi", "bwi_personal_notes_beginning_notes_room") 
  }
  /// Dir steht jetzt ein privater Raum für deine Notizen und Dateien zur Verfügung. Klicke, um den Raum zu öffnen.
  public static var bwiPersonalNotesHintMessage: String { 
    return BWIL10n.tr("Bwi", "bwi_personal_notes_hint_message") 
  }
  /// Nachrichten in diesem Raum sind Ende-zu-Ende-verschlüsselt.
  public static var bwiPersonalNotesRoomProfileEncryptedSubtitle: String { 
    return BWIL10n.tr("Bwi", "bwi_personal_notes_room_profile_encrypted_subtitle") 
  }
  /// Berechtigungen
  public static var bwiRollsAndRights: String { 
    return BWIL10n.tr("Bwi", "bwi_rolls_and_rights") 
  }
  /// Admin
  public static var bwiRollsAndRightsAdmin: String { 
    return BWIL10n.tr("Bwi", "bwi_rolls_and_rights_admin") 
  }
  /// Nutzer entfernen
  public static var bwiRollsAndRightsBan: String { 
    return BWIL10n.tr("Bwi", "bwi_rolls_and_rights_ban") 
  }
  /// Raumeigenschaften (Name, Thema, Raumbild) anpassen
  public static var bwiRollsAndRightsGeneral: String { 
    return BWIL10n.tr("Bwi", "bwi_rolls_and_rights_general") 
  }
  /// Nutzer einladen
  public static var bwiRollsAndRightsInvite: String { 
    return BWIL10n.tr("Bwi", "bwi_rolls_and_rights_invite") 
  }
  /// Rollen, die zum Ändern verschiedener Teile des Raums erforderlich sind, auswählen
  public static var bwiRollsAndRightsMainHeaderAdmin: String { 
    return BWIL10n.tr("Bwi", "bwi_rolls_and_rights_main_header_admin") 
  }
  /// Rollen, die zum Ändern verschiedener Teile des Raums erforderlich sind, auswählen
  public static var bwiRollsAndRightsMainHeaderNoAdmin: String { 
    return BWIL10n.tr("Bwi", "bwi_rolls_and_rights_main_header_no_admin") 
  }
  /// Berechtigungen
  public static var bwiRollsAndRightsMainTitle: String { 
    return BWIL10n.tr("Bwi", "bwi_rolls_and_rights_main_title") 
  }
  /// Moderator
  public static var bwiRollsAndRightsModerator: String { 
    return BWIL10n.tr("Bwi", "bwi_rolls_and_rights_moderator") 
  }
  /// Nachrichten löschen
  public static var bwiRollsAndRightsRedact: String { 
    return BWIL10n.tr("Bwi", "bwi_rolls_and_rights_redact") 
  }
  /// Werte zurücksetzen
  public static var bwiRollsAndRightsResetButton: String { 
    return BWIL10n.tr("Bwi", "bwi_rolls_and_rights_reset_button") 
  }
  /// Abbruch
  public static var bwiRollsAndRightsResetCancel: String { 
    return BWIL10n.tr("Bwi", "bwi_rolls_and_rights_reset_cancel") 
  }
  /// Zurücksetzen
  public static var bwiRollsAndRightsResetOk: String { 
    return BWIL10n.tr("Bwi", "bwi_rolls_and_rights_reset_ok") 
  }
  /// Bist du sicher, dass die Rechteeinstellungen dieses Raumes zurück gesetzt werden sollen?
  public static var bwiRollsAndRightsResetTitle: String { 
    return BWIL10n.tr("Bwi", "bwi_rolls_and_rights_reset_title") 
  }
  /// Nachrichten senden
  public static var bwiRollsAndRightsSend: String { 
    return BWIL10n.tr("Bwi", "bwi_rolls_and_rights_send") 
  }
  /// Mitglied
  public static var bwiRollsAndRightsUser: String { 
    return BWIL10n.tr("Bwi", "bwi_rolls_and_rights_user") 
  }
  /// Funktion in %@: %@
  public static func bwiRoomMemberDetailsUserlabel(_ p1: String, _ p2: String) -> String {
    return BWIL10n.tr("Bwi", "bwi_room_member_details_userlabel", p1, p2)
  }
  /// Funktion in %@
  public static func bwiRoomMemberSectionUserlabels(_ p1: String) -> String {
    return BWIL10n.tr("Bwi", "bwi_room_member_section_userlabels", p1)
  }
  /// Löschen
  public static var bwiRoomMemberUserlabelsButtonDelete: String { 
    return BWIL10n.tr("Bwi", "bwi_room_member_userlabels_button_delete") 
  }
  /// Sichern
  public static var bwiRoomMemberUserlabelsButtonSave: String { 
    return BWIL10n.tr("Bwi", "bwi_room_member_userlabels_button_save") 
  }
  /// Als Administrator kannst du für Personen im Raum eine Funktion hinzufügen und entfernen.\nDiese ist für alle Raummitglieder sichtbar.
  public static var bwiRoomMemberUserlabelsDescription: String { 
    return BWIL10n.tr("Bwi", "bwi_room_member_userlabels_description") 
  }
  /// Funktion hinzufügen
  public static var bwiRoomMemberUserlabelsTextfieldPlaceholder: String { 
    return BWIL10n.tr("Bwi", "bwi_room_member_userlabels_textfield_placeholder") 
  }
  /// Admin
  public static var bwiRoomParticipantsSectionAdmin: String { 
    return BWIL10n.tr("Bwi", "bwi_room_participants_section_admin") 
  }
  /// Eingeladen
  public static var bwiRoomParticipantsSectionInvite: String { 
    return BWIL10n.tr("Bwi", "bwi_room_participants_section_invite") 
  }
  /// Mitglied
  public static var bwiRoomParticipantsSectionMember: String { 
    return BWIL10n.tr("Bwi", "bwi_room_participants_section_member") 
  }
  /// Moderator
  public static var bwiRoomParticipantsSectionModerator: String { 
    return BWIL10n.tr("Bwi", "bwi_room_participants_section_moderator") 
  }
  /// Durch das Aufheben der Föderation werden alle Mitglieder der anderen Organisation unwiderruflich entfernt.\n\nFöderation trotzdem aufheben?
  public static var bwiRoomSettingsFederationAlertMessage: String { 
    return BWIL10n.tr("Bwi", "bwi_room_settings_federation_alert_message") 
  }
  /// Achtung!
  public static var bwiRoomSettingsFederationAlertTitle: String { 
    return BWIL10n.tr("Bwi", "bwi_room_settings_federation_alert_title") 
  }
  /// Ja, aufheben
  public static var bwiRoomSettingsFederationAlertWithdrawButton: String { 
    return BWIL10n.tr("Bwi", "bwi_room_settings_federation_alert_withdraw_button") 
  }
  /// Details dazu findest du in der Datenschutzerklärung
  public static var bwiSettingsAnalyticsSectionFooter: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_analytics_section_footer") 
  }
  /// Fehleranalyse
  public static var bwiSettingsAnalyticsSectionHeader: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_analytics_section_header") 
  }
  /// Fehleranalyse unterstützen
  public static var bwiSettingsAnalyticsSwitchText: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_analytics_switch_text") 
  }
  /// Entwickler
  public static var bwiSettingsDeveloper: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_developer") 
  }
  /// Announcement zeigen
  public static var bwiSettingsDeveloperAnnouncementFederationPreview: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_developer_announcement_federation_preview") 
  }
  /// Account Data Flag für Announcement zurücksetzen
  public static var bwiSettingsDeveloperAnnouncementFederationReset: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_developer_announcement_federation_reset") 
  }
  /// MDM Config: MSG Demo
  public static var bwiSettingsDeveloperApplyAppConfig: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_developer_apply_app_config") 
  }
  /// MDM Config: BW
  public static var bwiSettingsDeveloperApplyOrigAppConfig: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_developer_apply_orig_app_config") 
  }
  /// Capabilities
  public static var bwiSettingsDeveloperCapabilities: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_developer_capabilities") 
  }
  /// Neuen Notizen-Raum Raum erstellen
  public static var bwiSettingsDeveloperCreateNewPersonalNotesRoom: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_developer_create_new_personal_notes_room") 
  }
  /// Föderation
  public static var bwiSettingsDeveloperIntroduceFederation: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_developer_introduce_federation") 
  }
  /// Introduction zeigen
  public static var bwiSettingsDeveloperIntroduceFederationPreview: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_developer_introduce_federation_preview") 
  }
  /// Account Data Flag für Introduction zurücksetzen
  public static var bwiSettingsDeveloperIntroduceFederationReset: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_developer_introduce_federation_reset") 
  }
  /// Key Backup
  public static var bwiSettingsDeveloperKeyBackup: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_developer_key_backup") 
  }
  /// Algorithmus
  public static var bwiSettingsDeveloperKeyBackupAlgorithm: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_developer_key_backup_algorithm") 
  }
  /// Gespeicherte Schlüssel
  public static var bwiSettingsDeveloperKeyBackupCount: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_developer_key_backup_count") 
  }
  /// ETag
  public static var bwiSettingsDeveloperKeyBackupEtag: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_developer_key_backup_etag") 
  }
  /// Version
  public static var bwiSettingsDeveloperKeyBackupVersion: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_developer_key_backup_version") 
  }
  /// Maintenance
  public static var bwiSettingsDeveloperMaintenance: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_developer_maintenance") 
  }
  /// UserDefaults / lokale Daten
  public static var bwiSettingsDeveloperMaintenanceLocalData: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_developer_maintenance_local_data") 
  }
  /// Aktualisieren
  public static var bwiSettingsDeveloperMaintenanceReload: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_developer_maintenance_reload") 
  }
  /// Daten vom Server
  public static var bwiSettingsDeveloperMaintenanceServerData: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_developer_maintenance_server_data") 
  }
  /// Der alte Notizen-Raum Raum wurde durch einen neuen ersetzt.
  public static var bwiSettingsDeveloperNewPersonalNotesRoomCreated: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_developer_new_personal_notes_room_created") 
  }
  /// MDM Config: Reset
  public static var bwiSettingsDeveloperResetAppConfig: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_developer_reset_app_config") 
  }
  /// Matomo-Info-Screen zurücksetzen
  public static var bwiSettingsDeveloperResetMatomoInfo: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_developer_reset_matomo_info") 
  }
  /// Nutzer einschränken
  public static var bwiSettingsDeveloperRestrictUser: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_developer_restrict_user") 
  }
  /// Der Geburtstagsbanner wird beim nächsten Starten der App wieder angezeigt.
  public static var bwiSettingsDeveloperSettingsBirthdayBannerResettetMessage: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_developer_settings_birthday_banner_resettet_message") 
  }
  /// Zurückgesetzt
  public static var bwiSettingsDeveloperSettingsBirthdayBannerResettetTitle: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_developer_settings_birthday_banner_resettet_title") 
  }
  /// Geburtstagsbanner zurücksetzen
  public static var bwiSettingsDeveloperSettingsResetBirthdayBanner: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_developer_settings_reset_birthday_banner") 
  }
  /// Der Matomo-Info-Screen wird wieder angezeigt.
  public static var bwiSettingsDeveloperShowMatomoPrivacyNotesResetted: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_developer_show_matomo_privacy_notes_resetted") 
  }
  /// Banner als nicht gelesen markieren
  public static var bwiSettingsDeveloperUnmarkBanner: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_developer_unmark_banner") 
  }
  /// Nutzereinschränkung aufheben
  public static var bwiSettingsDeveloperUnrestrictUser: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_developer_unrestrict_user") 
  }
  /// Account Data
  public static var bwiSettingsDeveloperUserAccountData: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_developer_user_account_data") 
  }
  /// Type
  public static var bwiSettingsDeveloperUserAccountDataType: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_developer_user_account_data_type") 
  }
  /// Well-Known
  public static var bwiSettingsDeveloperWellKnown: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_developer_well_known") 
  }
  /// Einen Raum für private Notizen anzeigen
  public static var bwiSettingsEnableNotesRoomSummary: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_enable_notes_room_summary") 
  }
  /// Zeige meine Notizen
  public static var bwiSettingsEnableNotesRoomTitle: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_enable_notes_room_title") 
  }
  /// Neuer Editor (umfangreiche Formatierung)
  public static var bwiSettingsEnableWysiwygComposer: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_enable_wysiwyg_composer") 
  }
  /// Ignorierte Nutzer
  public static var bwiSettingsIgnoredUsersText: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_ignored_users_text") 
  }
  /// Föderation
  public static var bwiSettingsLabsFederationFeature: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_labs_federation_feature") 
  }
  /// Netiquette
  public static var bwiSettingsNetiquette: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_netiquette") 
  }
  /// Neue Funktionen
  public static var bwiSettingsNewFeaturesHeader: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_new_features_header") 
  }
  /// Neue Funktionen anzeigen
  public static var bwiSettingsNewFeaturesShowFeatures: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_new_features_show_features") 
  }
  /// Einstellungen
  public static var bwiSettingsShowDeveloperSettings: String { 
    return BWIL10n.tr("Bwi", "bwi_settings_show_developer_settings") 
  }
  /// %@ zur Funktion ernannt: %@
  public static func bwiTimelineUserlabelAddedForUser(_ p1: String, _ p2: String) -> String {
    return BWIL10n.tr("Bwi", "bwi_timeline_userlabel_added_for_user", p1, p2)
  }
  /// %@ hat %@
  public static func bwiTimelineUserlabelPrefixOtherUser(_ p1: String, _ p2: String) -> String {
    return BWIL10n.tr("Bwi", "bwi_timeline_userlabel_prefix_other_user", p1, p2)
  }
  /// Du hast %@
  public static func bwiTimelineUserlabelPrefixYou(_ p1: String) -> String {
    return BWIL10n.tr("Bwi", "bwi_timeline_userlabel_prefix_you", p1)
  }
  /// für %@ die Funktion entfernt: %@
  public static func bwiTimelineUserlabelRemovedForUser(_ p1: String, _ p2: String) -> String {
    return BWIL10n.tr("Bwi", "bwi_timeline_userlabel_removed_for_user", p1, p2)
  }
  /// Abbruch
  public static var cancel: String { 
    return BWIL10n.tr("Bwi", "cancel") 
  }
  /// BundesMessenger Android
  public static var clientAndroidName: String { 
    return BWIL10n.tr("Bwi", "client_android_name") 
  }
  /// BundesMessenger Desktop
  public static var clientDesktopName: String { 
    return BWIL10n.tr("Bwi", "client_desktop_name") 
  }
  /// BundesMessenger iOS
  public static var clientIosName: String { 
    return BWIL10n.tr("Bwi", "client_ios_name") 
  }
  /// BundesMessenger Web
  public static var clientWebName: String { 
    return BWIL10n.tr("Bwi", "client_web_name") 
  }
  /// Beenden
  public static var close: String { 
    return BWIL10n.tr("Bwi", "close") 
  }
  /// Einklappen
  public static var collapse: String { 
    return BWIL10n.tr("Bwi", "collapse") 
  }
  /// Datei steht nicht mehr zur Verfügung
  public static var contentScanStatusFailed: String { 
    return BWIL10n.tr("Bwi", "content_scan_status_failed") 
  }
  /// Scanne...
  public static var contentScanStatusInProgress: String { 
    return BWIL10n.tr("Bwi", "content_scan_status_in_progress") 
  }
  /// Blockiert
  public static var contentScanStatusInfected: String { 
    return BWIL10n.tr("Bwi", "content_scan_status_infected") 
  }
  /// ✓ Sicher
  public static var contentScanStatusTrusted: String { 
    return BWIL10n.tr("Bwi", "content_scan_status_trusted") 
  }
  /// Weiter
  public static var `continue`: String { 
    return BWIL10n.tr("Bwi", "continue") 
  }
  /// Erstelle
  public static var create: String { 
    return BWIL10n.tr("Bwi", "create") 
  }
  /// Ok
  public static var createRoomFailedToDeactivateFederationAlertDismissButton: String { 
    return BWIL10n.tr("Bwi", "create_room_failed_to_deactivate_federation_alert_dismiss_button") 
  }
  /// Die Föderation konnte nicht deaktiviert werden, bitte versuche es später erneut.
  public static var createRoomFailedToDeactivateFederationForRoomErrorText: String { 
    return BWIL10n.tr("Bwi", "create_room_failed_to_deactivate_federation_for_room_error_text") 
  }
  /// Föderation aktiv
  public static var createRoomFailedToDeactivateFederationForRoomErrorTitle: String { 
    return BWIL10n.tr("Bwi", "create_room_failed_to_deactivate_federation_for_room_error_title") 
  }
  /// Thema
  public static var createRoomPlaceholderTopic: String { 
    return BWIL10n.tr("Bwi", "create_room_placeholder_topic") 
  }
  /// Für eine Föderation zulassen
  public static var createRoomTypeFederated: String { 
    return BWIL10n.tr("Bwi", "create_room_type_federated") 
  }
  /// (Raum kann von externen Organisationen mitgenutzt werden)
  public static var createRoomTypeFederatedSubtext: String { 
    return BWIL10n.tr("Bwi", "create_room_type_federated_subtext") 
  }
  /// Verifizierung abgebrochen. Du kannst sie erneut starten.
  public static var deviceVerificationCancelled: String { 
    return BWIL10n.tr("Bwi", "device_verification_cancelled") 
  }
  /// Mit anderem Gerät verifizieren
  public static var deviceVerificationCrosssigningWaitRecoverSecrets: String { 
    return BWIL10n.tr("Bwi", "device_verification_crosssigning_wait_recover_secrets") 
  }
  /// Anmeldung verifizieren
  public static var deviceVerificationOtherLoginVerifyWaitTitle: String { 
    return BWIL10n.tr("Bwi", "device_verification_other_login_verify_wait_title") 
  }
  /// Bestätige die neue Anmeldung auf Deinem Gerät: %@
  public static func deviceVerificationSelfVerifyAlertMessage(_ p1: String) -> String {
    return BWIL10n.tr("Bwi", "device_verification_self_verify_alert_message", p1)
  }
  /// Verifiziere diese Anmeldung mit einem Gerät, auf dem du bereits angemeldet bist, um Zugriff auf die verschlüsselten Nachrichten zu erhalten.
  public static var deviceVerificationSelfVerifyWaitInformation: String { 
    return BWIL10n.tr("Bwi", "device_verification_self_verify_wait_information") 
  }
  /// 
  public static var deviceVerificationSelfVerifyWaitInformationMore: String { 
    return BWIL10n.tr("Bwi", "device_verification_self_verify_wait_information_more") 
  }
  /// Nutze Deinen Wiederherstellungsschlüssel, um alle Deine Nachrichten zu entschlüsseln.
  public static var deviceVerificationSelfVerifyWaitRecoverSecretsAdditionalInformation: String { 
    return BWIL10n.tr("Bwi", "device_verification_self_verify_wait_recover_secrets_additional_information") 
  }
  /// Wiederherstellungsschlüssel
  public static var deviceVerificationSelfVerifyWaitRecoverSecretsWithPassphrase: String { 
    return BWIL10n.tr("Bwi", "device_verification_self_verify_wait_recover_secrets_with_passphrase") 
  }
  /// Wiederherstellungsschlüssel
  public static var deviceVerificationSelfVerifyWaitRecoverSecretsWithoutPassphrase: String { 
    return BWIL10n.tr("Bwi", "device_verification_self_verify_wait_recover_secrets_without_passphrase") 
  }
  /// Diese Anmeldung verifizieren
  public static var deviceVerificationSelfVerifyWaitTitle: String { 
    return BWIL10n.tr("Bwi", "device_verification_self_verify_wait_title") 
  }
  /// Fertig
  public static var deviceVerificationVerifiedGotItButton: String { 
    return BWIL10n.tr("Bwi", "device_verification_verified_got_it_button") 
  }
  /// Geschafft!
  public static var deviceVerificationVerifiedTitle: String { 
    return BWIL10n.tr("Bwi", "device_verification_verified_title") 
  }
  /// %tu Treffer gefunden für %@
  public static func directorySearchResults(_ p1: Int, _ p2: String) -> String {
    return BWIL10n.tr("Bwi", "directory_search_results", p1, p2)
  }
  /// >%tu Treffer gefunden für %@
  public static func directorySearchResultsMoreThan(_ p1: Int, _ p2: String) -> String {
    return BWIL10n.tr("Bwi", "directory_search_results_more_than", p1, p2)
  }
  /// Ok
  public static var downtimeAlertDismissButton: String { 
    return BWIL10n.tr("Bwi", "downtime_alert_dismiss_button") 
  }
  /// Wir führen gerade Wartungsarbeiten durch. Bitte versuche es später erneut.
  public static var downtimeDefaultMessage: String { 
    return BWIL10n.tr("Bwi", "downtime_default_message") 
  }
  /// ⚠ Server nicht erreichbar
  public static var downtimeTitle: String { 
    return BWIL10n.tr("Bwi", "downtime_title") 
  }
  /// Eine neues Schlüsselbackup wurde erkannt.\n\nWenn dies nicht der Fall war, lege in den Einstellungen ein neues Verschlüsselungskennwort fest.
  public static var e2eKeyBackupWrongVersion: String { 
    return BWIL10n.tr("Bwi", "e2e_key_backup_wrong_version") 
  }
  /// Dieser Raum wurde vom Administrator für eine Föderation zugelassen.
  public static var eventFormatterAclAllowFederation: String { 
    return BWIL10n.tr("Bwi", "event_formatter_acl_allow_federation") 
  }
  /// %tu Föderations und weitere Einstellungen geändert.
  public static func eventFormatterAclAndOtherCollapsed(_ p1: Int) -> String {
    return BWIL10n.tr("Bwi", "event_formatter_acl_and_other_collapsed", p1)
  }
  /// %tu Föderationseinstellungen wurden geändert.
  public static func eventFormatterAclCollapsed(_ p1: Int) -> String {
    return BWIL10n.tr("Bwi", "event_formatter_acl_collapsed", p1)
  }
  /// Die Föderation für diesen Raum wurde nicht zugelassen.
  public static var eventFormatterAclDisallowAtStart: String { 
    return BWIL10n.tr("Bwi", "event_formatter_acl_disallow_at_start") 
  }
  /// Die Föderation für diesen Raum wurde aufgehoben. Alle föderierten Mitglieder werden automatisch entfernt.
  public static var eventFormatterAclDisallowFederation: String { 
    return BWIL10n.tr("Bwi", "event_formatter_acl_disallow_federation") 
  }
  /// Verstanden
  public static var federationAnnouncementButton: String { 
    return BWIL10n.tr("Bwi", "federation_announcement_button") 
  }
  /// Wenn du dein Profilbild vorweg ändern möchtest, gehe zu den Einstellungen.
  public static var federationAnnouncementSubText: String { 
    return BWIL10n.tr("Bwi", "federation_announcement_sub_text") 
  }
  /// Ab Mai 2024 ist die **Föderation** zwischen verschiedenen Organisationen möglich. Dies bietet eine übergreifende sichere Kommunikation.\n\nDetails findest du in der Datenschutzerklärung.
  public static var federationAnnouncementText: String { 
    return BWIL10n.tr("Bwi", "federation_announcement_text") 
  }
  /// Ankündigung
  public static var federationAnnouncementTitle: String { 
    return BWIL10n.tr("Bwi", "federation_announcement_title") 
  }
  /// Notizen ausblenden
  public static var homeContextMenuPersonalNotes: String { 
    return BWIL10n.tr("Bwi", "home_context_menu_personal_notes") 
  }
  /// Die Verbindung zum Heimserver ist fehlgeschlagen.
  public static var homeserverConnectionLost: String { 
    return BWIL10n.tr("Bwi", "homeserver_connection_lost") 
  }
  /// Foto entfernen
  public static var imagePickerActionRemovePhoto: String { 
    return BWIL10n.tr("Bwi", "image_picker_action_remove_photo") 
  }
  /// Deine Zugangsdaten sind leider nicht mehr in der App gespeichert. Bitte beende den Messenger und logge dich neu ein. Dabei musst du deinen Pin Code neu setzen und dich durch dein Verschlüsselungskennwort verifizieren.
  public static var integrityAlertMessage: String { 
    return BWIL10n.tr("Bwi", "integrity_alert_message") 
  }
  /// Fehler beim Appstart
  public static var integrityAlertTitle: String { 
    return BWIL10n.tr("Bwi", "integrity_alert_title") 
  }
  /// Übergreifende sichere Kommunikation zwischen verschiedenen Organisationen
  public static var introduceFederationScreen1Description: String { 
    return BWIL10n.tr("Bwi", "introduce_federation_screen1_description") 
  }
  /// Föderation
  public static var introduceFederationScreen1Title: String { 
    return BWIL10n.tr("Bwi", "introduce_federation_screen1_title") 
  }
  /// Föderierte Personen und Räume erkennst du am Symbol mit den zwei sich überschneidenden Kreisen.
  public static var introduceFederationScreen2Description: String { 
    return BWIL10n.tr("Bwi", "introduce_federation_screen2_description") 
  }
  /// Kennzeichnung
  public static var introduceFederationScreen2Title: String { 
    return BWIL10n.tr("Bwi", "introduce_federation_screen2_title") 
  }
  /// Bestimme als Admin, welche Räume für eine Föderation zugelassen sind.
  public static var introduceFederationScreen3Description: String { 
    return BWIL10n.tr("Bwi", "introduce_federation_screen3_description") 
  }
  /// Individuelle Einstellung
  public static var introduceFederationScreen3Title: String { 
    return BWIL10n.tr("Bwi", "introduce_federation_screen3_title") 
  }
  /// Verstanden
  public static var introduceFederationStart: String { 
    return BWIL10n.tr("Bwi", "introduce_federation_start") 
  }
  /// Einladen
  public static var invite: String { 
    return BWIL10n.tr("Bwi", "invite") 
  }
  /// %@ unterstützt nur nicht modifizierte iOS-Versionen.
  public static func jailbreakAlertMessage(_ p1: String) -> String {
    return BWIL10n.tr("Bwi", "jailbreak_alert_message", p1)
  }
  /// Systemanforderungen
  public static var jailbreakAlertTitle: String { 
    return BWIL10n.tr("Bwi", "jailbreak_alert_title") 
  }
  /// Betreten
  public static var join: String { 
    return BWIL10n.tr("Bwi", "join") 
  }
  /// Beigetreten
  public static var joined: String { 
    return BWIL10n.tr("Bwi", "joined") 
  }
  /// Nutze dein Verschlüsselungskennwort um deinen sicheren Chatverlauf zu entschlüsseln
  public static var keyBackupRecoverFromPassphraseInfo: String { 
    return BWIL10n.tr("Bwi", "key_backup_recover_from_passphrase_info") 
  }
  /// Wiederherstellungsschlüssel vergessen? Du kannst 
  public static var keyBackupRecoverFromPassphraseLostPassphraseActionPart1: String { 
    return BWIL10n.tr("Bwi", "key_backup_recover_from_passphrase_lost_passphrase_action_part1") 
  }
  /// nutze dein Wiederherstellungsschlüssel
  public static var keyBackupRecoverFromPassphraseLostPassphraseActionPart2: String { 
    return BWIL10n.tr("Bwi", "key_backup_recover_from_passphrase_lost_passphrase_action_part2") 
  }
  /// Wiederherstellungsschlüssel eingeben
  public static var keyBackupRecoverFromPassphrasePassphrasePlaceholder: String { 
    return BWIL10n.tr("Bwi", "key_backup_recover_from_passphrase_passphrase_placeholder") 
  }
  /// Gebe
  public static var keyBackupRecoverFromPassphrasePassphraseTitle: String { 
    return BWIL10n.tr("Bwi", "key_backup_recover_from_passphrase_passphrase_title") 
  }
  /// Nutze dein Verschlüsselungskennwort um deinen sicheren Chatverlauf zu entschlüsseln
  public static var keyBackupRecoverFromRecoveryKeyInfo: String { 
    return BWIL10n.tr("Bwi", "key_backup_recover_from_recovery_key_info") 
  }
  /// Hast du dein Verschlüsselungskennwort verloren? Du kannst in den Einstellungen ein neues einrichten.
  public static var keyBackupRecoverFromRecoveryKeyLostRecoveryKeyAction: String { 
    return BWIL10n.tr("Bwi", "key_backup_recover_from_recovery_key_lost_recovery_key_action") 
  }
  /// Gebe Verschlüsselungskennwort ein
  public static var keyBackupRecoverFromRecoveryKeyRecoveryKeyPlaceholder: String { 
    return BWIL10n.tr("Bwi", "key_backup_recover_from_recovery_key_recovery_key_placeholder") 
  }
  /// Sicherung konnte nicht mit diesem Wiederherstellungsschlüssel entschlüsselt werden: Bitte stelle sicher, dass du den richtigen Wiederherstellungsschlüssel eingegeben hast.
  public static var keyBackupRecoverInvalidPassphrase: String { 
    return BWIL10n.tr("Bwi", "key_backup_recover_invalid_passphrase") 
  }
  /// Falscher Wiederherstellungsschlüssel
  public static var keyBackupRecoverInvalidPassphraseTitle: String { 
    return BWIL10n.tr("Bwi", "key_backup_recover_invalid_passphrase_title") 
  }
  /// Sicherung konnte mit diesem Schlüssel nicht entschlüsselt werden: Bitte stelle sicher, dass du das richtige Verschlüsselungskennwort eingegeben hast.
  public static var keyBackupRecoverInvalidRecoveryKey: String { 
    return BWIL10n.tr("Bwi", "key_backup_recover_invalid_recovery_key") 
  }
  /// Verschlüsselungskennwort passt nicht
  public static var keyBackupRecoverInvalidRecoveryKeyTitle: String { 
    return BWIL10n.tr("Bwi", "key_backup_recover_invalid_recovery_key_title") 
  }
  /// Sichere Nachrichten
  public static var keyBackupRecoverTitle: String { 
    return BWIL10n.tr("Bwi", "key_backup_recover_title") 
  }
  /// Keine Übereinstimmung
  public static var keyBackupSetupPassphraseConfirmPassphraseInvalid: String { 
    return BWIL10n.tr("Bwi", "key_backup_setup_passphrase_confirm_passphrase_invalid") 
  }
  /// Wiederherstellungsschlüssel
  public static var keyBackupSetupPassphraseConfirmPassphrasePlaceholder: String { 
    return BWIL10n.tr("Bwi", "key_backup_setup_passphrase_confirm_passphrase_placeholder") 
  }
  /// 
  public static var keyBackupSetupPassphraseConfirmPassphraseTitle: String { 
    return BWIL10n.tr("Bwi", "key_backup_setup_passphrase_confirm_passphrase_title") 
  }
  /// Wir speichern eine verschlüsselte Kopie deiner Schlüssel auf unserem Server. Schütze deine Sicherung mit einem Wiederherstellungsschlüssel, um sie sicher zu halten.\n\nFür maximale Sicherheit sollte sich dieser von deinem Passwort unterscheiden.
  public static var keyBackupSetupPassphraseInfo: String { 
    return BWIL10n.tr("Bwi", "key_backup_setup_passphrase_info") 
  }
  /// Versuche ein weiteres Zeichen hinzuzufügen
  public static var keyBackupSetupPassphrasePassphraseInvalid: String { 
    return BWIL10n.tr("Bwi", "key_backup_setup_passphrase_passphrase_invalid") 
  }
  /// Wiederherstellungsschlüssel
  public static var keyBackupSetupPassphrasePassphrasePlaceholder: String { 
    return BWIL10n.tr("Bwi", "key_backup_setup_passphrase_passphrase_placeholder") 
  }
  /// 
  public static var keyBackupSetupPassphrasePassphraseTitle: String { 
    return BWIL10n.tr("Bwi", "key_backup_setup_passphrase_passphrase_title") 
  }
  /// Setze Wiederherstellungsschlüssel
  public static var keyBackupSetupPassphraseSetPassphraseAction: String { 
    return BWIL10n.tr("Bwi", "key_backup_setup_passphrase_set_passphrase_action") 
  }
  /// (Erweitert) Mit Wiederherstellungsschlüssel einrichten
  public static var keyBackupSetupPassphraseSetupRecoveryKeyAction: String { 
    return BWIL10n.tr("Bwi", "key_backup_setup_passphrase_setup_recovery_key_action") 
  }
  /// Oder sichere deine Sicherung mit einem Wiederherstellungsschlüssel, und sichere sie an einem sicheren Ort.
  public static var keyBackupSetupPassphraseSetupRecoveryKeyInfo: String { 
    return BWIL10n.tr("Bwi", "key_backup_setup_passphrase_setup_recovery_key_info") 
  }
  /// Sichere dein Backup mit einem Wiederherstellungsschlüssel
  public static var keyBackupSetupPassphraseTitle: String { 
    return BWIL10n.tr("Bwi", "key_backup_setup_passphrase_title") 
  }
  /// Deine Schlüssel werden gesichert.\n\nDein Wiederherstellungsschlüssel ist ein Sicherheitsnetzwerk - Du kannst damit den Zugriff auf deine verschlüsselten Nachrichten wiederherstellen, wenn du deine Verschlüsselungskennwort vergisst.\n\nBewahre den Wiederherstellungsschlüssel an einem sehr sicheren Ort auf, beispielsweise einem Kennwortmanager (oder einem Tresor).
  public static var keyBackupSetupSuccessFromPassphraseInfo: String { 
    return BWIL10n.tr("Bwi", "key_backup_setup_success_from_passphrase_info") 
  }
  /// Wiederherstellungsschlüssel speichern
  public static var keyBackupSetupSuccessFromPassphraseSaveRecoveryKeyAction: String { 
    return BWIL10n.tr("Bwi", "key_backup_setup_success_from_passphrase_save_recovery_key_action") 
  }
  /// Deine Schlüssel werden gesichert.\n\nErstelle eine Kopie dieses Wiederherstellungsschlüssels und bewahre ihn auf.
  public static var keyBackupSetupSuccessFromRecoveryKeyInfo: String { 
    return BWIL10n.tr("Bwi", "key_backup_setup_success_from_recovery_key_info") 
  }
  /// Verschlüsselungskennwort
  public static var keyBackupSetupSuccessFromRecoveryKeyRecoveryKeyTitle: String { 
    return BWIL10n.tr("Bwi", "key_backup_setup_success_from_recovery_key_recovery_key_title") 
  }
  /// Diese Anmeldung verifizieren
  public static var keyVerificationThisSessionTitle: String { 
    return BWIL10n.tr("Bwi", "key_verification_this_session_title") 
  }
  /// Verifizierung angefragt
  public static var keyVerificationTileRequestAlertTitle: String { 
    return BWIL10n.tr("Bwi", "key_verification_tile_request_alert_title") 
  }
  /// Du kannst nun sichere Nachrichten auf deinem neuen Gerät lesen. Andere Benutzer wissen, dass sie es vertrauen können.
  public static var keyVerificationVerifiedNewSessionInformation: String { 
    return BWIL10n.tr("Bwi", "key_verification_verified_new_session_information") 
  }
  /// Deine Anmeldung wurde erfolgreich verifiziert.
  public static var keyVerificationVerifiedOtherSessionInformation: String { 
    return BWIL10n.tr("Bwi", "key_verification_verified_other_session_information") 
  }
  /// Deine Anmeldung wurde erfolgreich verifiziert.
  public static var keyVerificationVerifiedThisSessionInformation: String { 
    return BWIL10n.tr("Bwi", "key_verification_verified_this_session_information") 
  }
  /// Stattdessen mit Emojis vergleichen
  public static var keyVerificationVerifyQrCodeCannotScanAction: String { 
    return BWIL10n.tr("Bwi", "key_verification_verify_qr_code_cannot_scan_action") 
  }
  /// Scanne den Code mit einem anderen Gerät oder umgekehrt.
  public static var keyVerificationVerifyQrCodeInformationOtherDevice: String { 
    return BWIL10n.tr("Bwi", "key_verification_verify_qr_code_information_other_device") 
  }
  /// Diese Anmeldung verifizieren
  public static var keyVerificationVerifyQrCodeTitle: String { 
    return BWIL10n.tr("Bwi", "key_verification_verify_qr_code_title") 
  }
  /// Mapbox Maps SDK für iOS
  public static var locationSharingMapCreditsTitle: String { 
    return BWIL10n.tr("Bwi", "location_sharing_map_credits_title") 
  }
  /// mehr
  public static var more: String { 
    return BWIL10n.tr("Bwi", "more") 
  }
  /// Du bist offline. Überprüfe deine Netzwerkverbindung!
  public static var networkOfflineMessage: String { 
    return BWIL10n.tr("Bwi", "network_offline_message") 
  }
  /// Du bist offline
  public static var networkOfflineTitle: String { 
    return BWIL10n.tr("Bwi", "network_offline_title") 
  }
  /// Nächster
  public static var next: String { 
    return BWIL10n.tr("Bwi", "next") 
  }
  /// %@ verließ den Raum
  public static func noticeRoomLeave(_ p1: String) -> String {
    return BWIL10n.tr("Bwi", "notice_room_leave", p1)
  }
  /// Dein Account %@ wurde erstellt.
  public static func onboardingCongratulationsMessage(_ p1: String) -> String {
    return BWIL10n.tr("Bwi", "onboarding_congratulations_message", p1)
  }
  /// Loslegen
  public static var onboardingSplashLoginButtonTitle: String { 
    return BWIL10n.tr("Bwi", "onboarding_splash_login_button_title") 
  }
  /// Wir helfen dir, dich zu verbinden.
  public static var onboardingUseCaseMessage: String { 
    return BWIL10n.tr("Bwi", "onboarding_use_case_message") 
  }
  /// diese Frage überspringen
  public static var onboardingUseCaseSkipButton: String { 
    return BWIL10n.tr("Bwi", "onboarding_use_case_skip_button") 
  }
  /// Willkommen!
  public static var pinProtectionChoosePinWelcomeAfterLogin: String { 
    return BWIL10n.tr("Bwi", "pin_protection_choose_pin_welcome_after_login") 
  }
  /// Willkommen!
  public static var pinProtectionChoosePinWelcomeAfterRegister: String { 
    return BWIL10n.tr("Bwi", "pin_protection_choose_pin_welcome_after_register") 
  }
  /// Bestätige PIN
  public static var pinProtectionConfirmPin: String { 
    return BWIL10n.tr("Bwi", "pin_protection_confirm_pin") 
  }
  /// Aktuelle PIN eingeben
  public static var pinProtectionConfirmPinToChange: String { 
    return BWIL10n.tr("Bwi", "pin_protection_confirm_pin_to_change") 
  }
  /// Löschen
  public static var pinProtectionResetButtonAccessibilityHint: String { 
    return BWIL10n.tr("Bwi", "pin_protection_reset_button_accessibility_hint") 
  }
  /// Löschen
  public static var pinProtectionResetButtonAccessibilityLabel: String { 
    return BWIL10n.tr("Bwi", "pin_protection_reset_button_accessibility_label") 
  }
  /// PIN
  public static var pinProtectionSettingsSectionHeaderX: String { 
    return BWIL10n.tr("Bwi", "pin_protection_settings_section_header_x") 
  }
  /// Umfrage erstellen
  public static var pollEditFormCreatePoll: String { 
    return BWIL10n.tr("Bwi", "poll_edit_form_create_poll") 
  }
  /// Anzeigen, wer für welche Option gestimmt hat.
  public static var pollEditFormParticipantToggle: String { 
    return BWIL10n.tr("Bwi", "poll_edit_form_participant_toggle") 
  }
  /// Umfragetyp
  public static var pollEditFormPollType: String { 
    return BWIL10n.tr("Bwi", "poll_edit_form_poll_type") 
  }
  /// Versteckte Umfrage
  public static var pollEditFormPollTypeClosed: String { 
    return BWIL10n.tr("Bwi", "poll_edit_form_poll_type_closed") 
  }
  /// Offene Umfrage
  public static var pollEditFormPollTypeOpen: String { 
    return BWIL10n.tr("Bwi", "poll_edit_form_poll_type_open") 
  }
  ///  Uhr
  public static var pollParticipantDetailsClockString: String { 
    return BWIL10n.tr("Bwi", "poll_participant_details_clock_string") 
  }
  /// Heute, 
  public static var pollParticipantDetailsDateStringToday: String { 
    return BWIL10n.tr("Bwi", "poll_participant_details_date_string_today") 
  }
  /// Alle ansehen (%lu weitere)
  public static func pollParticipantDetailsShowMore(_ p1: Int) -> String {
    return BWIL10n.tr("Bwi", "poll_participant_details_show_more", p1)
  }
  /// Umfragedetails
  public static var pollParticipantDetailsTitle: String { 
    return BWIL10n.tr("Bwi", "poll_participant_details_title") 
  }
  /// Stimmen ansehen
  public static var pollTimelineShowParticipantsButton: String { 
    return BWIL10n.tr("Bwi", "poll_timeline_show_participants_button") 
  }
  /// Wiederholen
  public static var retry: String { 
    return BWIL10n.tr("Bwi", "retry") 
  }
  /// Ok
  public static var roomAdminFederationDecisionSetFederationAlertOkButton: String { 
    return BWIL10n.tr("Bwi", "room_admin_federation_decision_set_federation_alert_ok_button") 
  }
  /// Der Server ist aktuell nicht erreichbar. Versuche es später erneut.
  public static var roomAdminFederationDecisionSetFederationErrorAlertText: String { 
    return BWIL10n.tr("Bwi", "room_admin_federation_decision_set_federation_error_alert_text") 
  }
  /// Aktion fehlgeschlagen
  public static var roomAdminFederationDecisionSetFederationErrorAlertTitle: String { 
    return BWIL10n.tr("Bwi", "room_admin_federation_decision_set_federation_error_alert_title") 
  }
  /// Dieser Raum ist jetzt föderiert.
  public static var roomAdminFederationDecisionSetFederationSuccessAlertTitle: String { 
    return BWIL10n.tr("Bwi", "room_admin_federation_decision_set_federation_success_alert_title") 
  }
  /// Raum föderieren
  public static var roomAdminFederationDecisionSheetActivateFederationButton: String { 
    return BWIL10n.tr("Bwi", "room_admin_federation_decision_sheet_activate_federation_button") 
  }
  /// Raum intern behalten
  public static var roomAdminFederationDecisionSheetDeactivateFederationButton: String { 
    return BWIL10n.tr("Bwi", "room_admin_federation_decision_sheet_deactivate_federation_button") 
  }
  /// Hierdurch kann der Raum von externen Organisationen mitgenutzt werden. Dies kann nachträglich in den Einstellungen geändert werden.
  public static var roomAdminFederationDecisionSheetText: String { 
    return BWIL10n.tr("Bwi", "room_admin_federation_decision_sheet_text") 
  }
  /// "%@" für eine Föderation zulassen?
  public static func roomAdminFederationDecisionSheetTitle(_ p1: String) -> String {
    return BWIL10n.tr("Bwi", "room_admin_federation_decision_sheet_title", p1)
  }
  /// Raumbild ändern
  public static var roomAvatarViewAccessibilityHint: String { 
    return BWIL10n.tr("Bwi", "room_avatar_view_accessibility_hint") 
  }
  /// Profilbild
  public static var roomAvatarViewAccessibilityLabel: String { 
    return BWIL10n.tr("Bwi", "room_avatar_view_accessibility_label") 
  }
  /// Name von Kontakt eingeben
  public static var roomCreationInviteAnotherUser: String { 
    return BWIL10n.tr("Bwi", "room_creation_invite_another_user") 
  }
  /// Neue Direktnachricht
  public static var roomCreationTitle: String { 
    return BWIL10n.tr("Bwi", "room_creation_title") 
  }
  /// Öffentlicher Raum
  public static var roomDetailsAccessSectionAccessToggleBw: String { 
    return BWIL10n.tr("Bwi", "room_details_access_section_access_toggle_bw") 
  }
  /// Jeder, der den Raumlink kennt - außer Gäste
  public static var roomDetailsAccessSectionAnyoneApartFromGuest: String { 
    return BWIL10n.tr("Bwi", "room_details_access_section_anyone_apart_from_guest") 
  }
  /// Zeige alle Räume in diesem Verzeichnis
  public static var roomDetailsAccessSectionDirectoryToggle: String { 
    return BWIL10n.tr("Bwi", "room_details_access_section_directory_toggle") 
  }
  /// Im Raum-Verzeichnis auflisten
  public static var roomDetailsAccessSectionDirectoryToggleForDm: String { 
    return BWIL10n.tr("Bwi", "room_details_access_section_directory_toggle_for_dm") 
  }
  /// Wer hat Zugriff hierauf?
  public static var roomDetailsAccessSectionForDm: String { 
    return BWIL10n.tr("Bwi", "room_details_access_section_for_dm") 
  }
  /// Verschlüsselung ist hier nicht aktiviert.
  public static var roomDetailsAdvancedE2eEncryptionDisabledForDm: String { 
    return BWIL10n.tr("Bwi", "room_details_advanced_e2e_encryption_disabled_for_dm") 
  }
  /// Verschlüsselung ist hier aktiviert
  public static var roomDetailsAdvancedE2eEncryptionEnabledForDm: String { 
    return BWIL10n.tr("Bwi", "room_details_advanced_e2e_encryption_enabled_for_dm") 
  }
  /// ID:
  public static var roomDetailsAdvancedRoomIdForDm: String { 
    return BWIL10n.tr("Bwi", "room_details_advanced_room_id_for_dm") 
  }
  /// Ok
  public static var roomDetailsFailedToChangeFederationAlertDismissButton: String { 
    return BWIL10n.tr("Bwi", "room_details_failed_to_change_federation_alert_dismiss_button") 
  }
  /// Die Föderation konnte nicht geändert werden, bitte versuche es später erneut.
  public static var roomDetailsFailedToChangeFederationForRoomErrorText: String { 
    return BWIL10n.tr("Bwi", "room_details_failed_to_change_federation_for_room_error_text") 
  }
  /// Föderation aktiv
  public static var roomDetailsFailedToChangeFederationForRoomErrorTitle: String { 
    return BWIL10n.tr("Bwi", "room_details_failed_to_change_federation_for_room_error_title") 
  }
  /// Aktualisierung der Föderations-Einstellung fehlgeschlagen
  public static var roomDetailsFailedToUpdateRoomServerAclRule: String { 
    return BWIL10n.tr("Bwi", "room_details_failed_to_update_room_server_acl_rule") 
  }
  /// Dateien
  public static var roomDetailsFiles: String { 
    return BWIL10n.tr("Bwi", "room_details_files") 
  }
  /// Keine lokalen Adressen vorhanden
  public static var roomDetailsNoLocalAddressesForDm: String { 
    return BWIL10n.tr("Bwi", "room_details_no_local_addresses_for_dm") 
  }
  /// Link zum Raum kopieren
  public static var roomDetailsPermalink: String { 
    return BWIL10n.tr("Bwi", "room_details_permalink") 
  }
  /// Suche
  public static var roomDetailsSearch: String { 
    return BWIL10n.tr("Bwi", "room_details_search") 
  }
  /// Raumdetails
  public static var roomDetailsTitle: String { 
    return BWIL10n.tr("Bwi", "room_details_title") 
  }
  /// Details
  public static var roomDetailsTitleForDm: String { 
    return BWIL10n.tr("Bwi", "room_details_title_for_dm") 
  }
  /// Du kannst den Raum nicht beitreten. Grund: Die Föderation wurde für diesen Raum aufgehoben.
  public static var roomErrorJoinFailedFederationDisabledMessage: String { 
    return BWIL10n.tr("Bwi", "room_error_join_failed_federation_disabled_message") 
  }
  /// Umfrage beenden
  public static var roomEventActionEndPoll: String { 
    return BWIL10n.tr("Bwi", "room_event_action_end_poll") 
  }
  /// Link zur Nachricht kopieren
  public static var roomEventActionPermalink: String { 
    return BWIL10n.tr("Bwi", "room_event_action_permalink") 
  }
  /// Umfrage entfernen
  public static var roomEventActionRemovePoll: String { 
    return BWIL10n.tr("Bwi", "room_event_action_remove_poll") 
  }
  /// Dies ist der Beginn deiner Direktnachrichten mit 
  public static var roomIntroCellInformationDmSentence1Part1: String { 
    return BWIL10n.tr("Bwi", "room_intro_cell_information_dm_sentence1_part1") 
  }
  /// Verstanden
  public static var roomMemberDetailsChangeFederatedMemberPowerLvlToAdminErrorButton: String { 
    return BWIL10n.tr("Bwi", "room_member_details_change_federated_member_power_lvl_to_admin_error_button") 
  }
  /// Diese Person kann keine Adminrechte erhalten, da sie nicht aus deiner Organisation stammt. Föderierte Personen können nur Mitglieder oder Moderatoren sein.
  public static var roomMemberDetailsChangeFederatedMemberPowerLvlToAdminErrorText: String { 
    return BWIL10n.tr("Bwi", "room_member_details_change_federated_member_power_lvl_to_admin_error_text") 
  }
  /// Du kannst dich nur zurückstufen, wenn du vorher einen weiteren Admin benennst.
  public static var roomMemberDetailsDowngradeLastAdmin: String { 
    return BWIL10n.tr("Bwi", "room_member_details_downgrade_last_admin") 
  }
  /// Administrator in %@
  public static func roomMemberPowerLevelAdminIn(_ p1: String) -> String {
    return BWIL10n.tr("Bwi", "room_member_power_level_admin_in", p1)
  }
  /// Moderator in %@
  public static func roomMemberPowerLevelModeratorIn(_ p1: String) -> String {
    return BWIL10n.tr("Bwi", "room_member_power_level_moderator_in", p1)
  }
  /// Es gelten globale Benachrichtigungseinstellungen
  public static var roomNotifsSettingsAllDescription: String { 
    return BWIL10n.tr("Bwi", "room_notifs_settings_all_description") 
  }
  /// Standardeinstellungen
  public static var roomNotifsSettingsAllMessages: String { 
    return BWIL10n.tr("Bwi", "room_notifs_settings_all_messages") 
  }
  /// 
  public static var roomNotifsSettingsEncryptedRoomNotice: String { 
    return BWIL10n.tr("Bwi", "room_notifs_settings_encrypted_room_notice") 
  }
  /// Benachrichtigungen aus
  public static var roomNotifsSettingsNone: String { 
    return BWIL10n.tr("Bwi", "room_notifs_settings_none") 
  }
  /// Einstellungen für diesen Raum
  public static var roomNotifsSettingsNotifyMeFor: String { 
    return BWIL10n.tr("Bwi", "room_notifs_settings_notify_me_for") 
  }
  /// Aus diesem Raum bannen
  public static var roomParticipantsActionBan: String { 
    return BWIL10n.tr("Bwi", "room_participants_action_ban") 
  }
  /// Alle Nachrichten von diesem Nutzer verbergen
  public static var roomParticipantsActionIgnore: String { 
    return BWIL10n.tr("Bwi", "room_participants_action_ignore") 
  }
  /// Link zum Nutzer kopieren
  public static var roomParticipantsActionPermalink: String { 
    return BWIL10n.tr("Bwi", "room_participants_action_permalink") 
  }
  /// Mache zum Administrator
  public static var roomParticipantsActionSetAdmin: String { 
    return BWIL10n.tr("Bwi", "room_participants_action_set_admin") 
  }
  /// Gib Moderationsrechte
  public static var roomParticipantsActionSetModerator: String { 
    return BWIL10n.tr("Bwi", "room_participants_action_set_moderator") 
  }
  /// Zeige alle Nachrichten von diesem Nutzer
  public static var roomParticipantsActionUnignore: String { 
    return BWIL10n.tr("Bwi", "room_participants_action_unignore") 
  }
  /// Als letzter Admin kannst du diesen Raum nicht verlassen, da entweder noch Mitglieder vorhanden sind oder es noch ausstehende Einladungen gibt.
  public static var roomParticipantsCantLeavePromptMessage: String { 
    return BWIL10n.tr("Bwi", "room_participants_cant_leave_prompt_message") 
  }
  /// Filtere Raum-Mitglieder
  public static var roomParticipantsFilterRoomMembers: String { 
    return BWIL10n.tr("Bwi", "room_participants_filter_room_members") 
  }
  /// Mitglieder filtern
  public static var roomParticipantsFilterRoomMembersForDm: String { 
    return BWIL10n.tr("Bwi", "room_participants_filter_room_members_for_dm") 
  }
  /// Name von Kontakt eingeben
  public static var roomParticipantsInviteAnotherUser: String { 
    return BWIL10n.tr("Bwi", "room_participants_invite_another_user") 
  }
  /// Du kannst diese Person nicht einladen, da die Föderation für diesen Raum durch den Admin nicht gewünscht ist.
  public static var roomParticipantsInvitePromptFederationForRoomNotAllowedText: String { 
    return BWIL10n.tr("Bwi", "room_participants_invite_prompt_federation_for_room_not_allowed_text") 
  }
  /// Du kannst noch keine Personen aus einer föderierten Organisation einladen, da die Freigabe hierfür noch nicht erteilt wurde. Gib dem Admin Bescheid, dass die Einstellung getroffen werden muss.
  public static var roomParticipantsInvitePromptServerAclForRoomNotConfiguredText: String { 
    return BWIL10n.tr("Bwi", "room_participants_invite_prompt_server_acl_for_room_not_configured_text") 
  }
  /// Die Person kann aktuell nicht eingeladen werden, bitte versuche es später erneut.
  public static var roomParticipantsInvitePromptServerAclLoadingErrorText: String { 
    return BWIL10n.tr("Bwi", "room_participants_invite_prompt_server_acl_loading_error_text") 
  }
  /// Bist du sicher, dass du %@ zu %@ einladen möchtest
  public static func roomParticipantsInvitePromptToMsg(_ p1: String, _ p2: String) -> String {
    return BWIL10n.tr("Bwi", "room_participants_invite_prompt_to_msg", p1, p2)
  }
  /// Raum wirklich verlassen und endgültig löschen?
  public static var roomParticipantsLeaveAndDeletePromptMsg: String { 
    return BWIL10n.tr("Bwi", "room_participants_leave_and_delete_prompt_msg") 
  }
  /// Bist du sicher, dass du die Konversation verlassen möchtest?
  public static var roomParticipantsLeavePromptMsgForDm: String { 
    return BWIL10n.tr("Bwi", "room_participants_leave_prompt_msg_for_dm") 
  }
  /// Verlassen
  public static var roomParticipantsLeavePromptTitleForDm: String { 
    return BWIL10n.tr("Bwi", "room_participants_leave_prompt_title_for_dm") 
  }
  /// Nachrichten in diesem Raum sind Ende-zu-Ende verschlüsselt.\n\nDeine Nachrichten sind mit digitalen Schlüsseln gesichert, nur du und der/die Empfänger!n haben die einzigen Schlüssel, um jene zu entschlüsseln.
  public static var roomParticipantsSecurityInformationRoomEncrypted: String { 
    return BWIL10n.tr("Bwi", "room_participants_security_information_room_encrypted") 
  }
  /// Nachrichten hier sind Ende-zu-Ende verschlüsselt.\n\nDeine Nachrichten sind mit digitalen Schlüsseln gesichert, nur du und der/die Empfänger!n haben die einzigen Schlüssel, um jene zu entsperren.
  public static var roomParticipantsSecurityInformationRoomEncryptedForDm: String { 
    return BWIL10n.tr("Bwi", "room_participants_security_information_room_encrypted_for_dm") 
  }
  /// Die Nachrichten hier sind nicht Ende-zu-Ende verschlüsselt.
  public static var roomParticipantsSecurityInformationRoomNotEncryptedForDm: String { 
    return BWIL10n.tr("Bwi", "room_participants_security_information_room_not_encrypted_for_dm") 
  }
  /// Neuer Raum
  public static var roomRecentsCreateEmptyRoom: String { 
    return BWIL10n.tr("Bwi", "room_recents_create_empty_room") 
  }
  /// Räume erkunden
  public static var roomRecentsExploreRooms: String { 
    return BWIL10n.tr("Bwi", "room_recents_explore_rooms") 
  }
  /// Dieser QR Code entspricht keinem gültigen Permalink.
  public static var roomRecentsScanFailedMessage: String { 
    return BWIL10n.tr("Bwi", "room_recents_scan_failed_message") 
  }
  /// Scan fehlgeschlagen
  public static var roomRecentsScanFailedTitle: String { 
    return BWIL10n.tr("Bwi", "room_recents_scan_failed_title") 
  }
  /// QR Code scannen
  public static var roomRecentsScanQrCode: String { 
    return BWIL10n.tr("Bwi", "room_recents_scan_qr_code") 
  }
  /// Neue Direktnachricht
  public static var roomRecentsStartChatWith: String { 
    return BWIL10n.tr("Bwi", "room_recents_start_chat_with") 
  }
  /// Deine Profilbild-URL
  public static var roomWidgetPermissionAvatarUrlPermission: String { 
    return BWIL10n.tr("Bwi", "room_widget_permission_avatar_url_permission") 
  }
  /// Speichern
  public static var save: String { 
    return BWIL10n.tr("Bwi", "save") 
  }
  /// Name von Kontakt eingeben
  public static var searchPeoplePlaceholder: String { 
    return BWIL10n.tr("Bwi", "search_people_placeholder") 
  }
  /// Erstelle einen neuen Raum
  public static var searchableDirectoryCreateNewRoom: String { 
    return BWIL10n.tr("Bwi", "searchable_directory_create_new_room") 
  }
  /// Raumverzeichnis
  public static var searchableDirectoryRoomTitle: String { 
    return BWIL10n.tr("Bwi", "searchable_directory_room_title") 
  }
  /// Raumname suchen
  public static var searchableDirectorySearchPlaceholder: String { 
    return BWIL10n.tr("Bwi", "searchable_directory_search_placeholder") 
  }
  /// %@ Netzwerk
  public static func searchableDirectoryXNetwork(_ p1: String) -> String {
    return BWIL10n.tr("Bwi", "searchable_directory_x_network", p1)
  }
  /// Wiederherstellungsschlüssel vergessen?
  public static var secretsRecoveryResetActionPart1: String { 
    return BWIL10n.tr("Bwi", "secrets_recovery_reset_action_part_1") 
  }
  /// Verschlüsselungskennwort zurücksetzen
  public static var secretsRecoveryResetActionPart2: String { 
    return BWIL10n.tr("Bwi", "secrets_recovery_reset_action_part_2") 
  }
  /// Ok
  public static var secretsRecoveryVerificationAlertCancel: String { 
    return BWIL10n.tr("Bwi", "secrets_recovery_verification_alert_cancel") 
  }
  /// Eine Verifizierungsanfrage wurde gesendet. Öffne eine deiner anderen Sitzungen, um sie zu akzeptieren und mit der Verifizierung zu beginnen.
  public static var secretsRecoveryVerificationAlertMessage: String { 
    return BWIL10n.tr("Bwi", "secrets_recovery_verification_alert_message") 
  }
  /// Anmeldung verifizieren
  public static var secretsRecoveryVerificationAlertTitle: String { 
    return BWIL10n.tr("Bwi", "secrets_recovery_verification_alert_title") 
  }
  /// Gib dein Verschlüsselungskennwort ein, um auf deine verschlüsselten Nachrichten und deine Cross-Signing-Identität zuzugreifen. Mit der Cross-Signing-Identität kannst du andere Sitzungen verifizieren.
  public static var secretsRecoveryWithKeyInformationDefault: String { 
    return BWIL10n.tr("Bwi", "secrets_recovery_with_key_information_default") 
  }
  /// Zum Forfahren gib deinen Wiederherstellungsschlüssel ein.
  public static var secretsRecoveryWithKeyInformationUnlockSecureBackupWithPhrase: String { 
    return BWIL10n.tr("Bwi", "secrets_recovery_with_key_information_unlock_secure_backup_with_phrase") 
  }
  /// Nutze dein Verschlüsselungskennwort um dieses Gerät zu verifizieren.
  public static var secretsRecoveryWithKeyInformationVerifyDevice: String { 
    return BWIL10n.tr("Bwi", "secrets_recovery_with_key_information_verify_device") 
  }
  /// Bitte stell sicher, dass du das korrekte Verschlüsselungskennwort benutzt hast.
  public static var secretsRecoveryWithKeyInvalidRecoveryKeyMessage: String { 
    return BWIL10n.tr("Bwi", "secrets_recovery_with_key_invalid_recovery_key_message") 
  }
  /// Verschlüsselungskennwort eingeben
  public static var secretsRecoveryWithKeyRecoveryKeyPlaceholder: String { 
    return BWIL10n.tr("Bwi", "secrets_recovery_with_key_recovery_key_placeholder") 
  }
  /// 
  public static var secretsRecoveryWithKeyRecoveryKeyTitle: String { 
    return BWIL10n.tr("Bwi", "secrets_recovery_with_key_recovery_key_title") 
  }
  /// Verschlüsselungskennwort
  public static var secretsRecoveryWithKeyTitle: String { 
    return BWIL10n.tr("Bwi", "secrets_recovery_with_key_title") 
  }
  /// Gib dein Wiederherstellungsschlüssel ein, um auf deine verschlüsselten Nachrichten und deine Cross-Signing-Identität zuzugreifen. Mit der Cross-Signing-Identität kannst du andere Sitzungen verifizieren.
  public static var secretsRecoveryWithPassphraseInformationDefault: String { 
    return BWIL10n.tr("Bwi", "secrets_recovery_with_passphrase_information_default") 
  }
  /// Nutze deinen Wiederherstellungsschlüssel, um alle deine Nachrichten zu entschlüsseln.
  public static var secretsRecoveryWithPassphraseInformationVerifyDevice: String { 
    return BWIL10n.tr("Bwi", "secrets_recovery_with_passphrase_information_verify_device") 
  }
  /// Bitte stell sicher, dass du den korrekten Wiederherstellungsschlüssel eingegeben hast.
  public static var secretsRecoveryWithPassphraseInvalidPassphraseMessage: String { 
    return BWIL10n.tr("Bwi", "secrets_recovery_with_passphrase_invalid_passphrase_message") 
  }
  /// Wenn du deinen Wiederherstellungsschlüssel nicht weißt, kannst du 
  public static var secretsRecoveryWithPassphraseLostPassphraseActionPart1: String { 
    return BWIL10n.tr("Bwi", "secrets_recovery_with_passphrase_lost_passphrase_action_part1") 
  }
  /// nutze deinen Wiederherstellungsschlüssel
  public static var secretsRecoveryWithPassphraseLostPassphraseActionPart2: String { 
    return BWIL10n.tr("Bwi", "secrets_recovery_with_passphrase_lost_passphrase_action_part2") 
  }
  /// Wiederherstellungsschlüssel
  public static var secretsRecoveryWithPassphrasePassphrasePlaceholder: String { 
    return BWIL10n.tr("Bwi", "secrets_recovery_with_passphrase_passphrase_placeholder") 
  }
  /// 
  public static var secretsRecoveryWithPassphrasePassphraseTitle: String { 
    return BWIL10n.tr("Bwi", "secrets_recovery_with_passphrase_passphrase_title") 
  }
  /// Weiter
  public static var secretsRecoveryWithPassphraseRecoverAction: String { 
    return BWIL10n.tr("Bwi", "secrets_recovery_with_passphrase_recover_action") 
  }
  /// Wiederherstellungsschlüssel
  public static var secretsRecoveryWithPassphraseTitle: String { 
    return BWIL10n.tr("Bwi", "secrets_recovery_with_passphrase_title") 
  }
  /// Bestätige mit deinem Passwort!
  public static var secretsResetAuthenticationMessage: String { 
    return BWIL10n.tr("Bwi", "secrets_reset_authentication_message") 
  }
  /// Bitte fahre nur fort, wenn Du kein anderes Gerät hast um diesen Login zu verifizieren.
  public static var secretsResetInformation: String { 
    return BWIL10n.tr("Bwi", "secrets_reset_information") 
  }
  /// Zurücksetzen
  public static var secretsResetResetAction: String { 
    return BWIL10n.tr("Bwi", "secrets_reset_reset_action") 
  }
  /// Zurücksetzen
  public static var secretsResetTitle: String { 
    return BWIL10n.tr("Bwi", "secrets_reset_title") 
  }
  /// Das Zurücksetzen deines Wiederherstellungsschlüssels kann nicht rückgängig gemacht werden. Nach dem Zurücksetzen wirst du alte Nachrichten nicht mehr lesen können.
  public static var secretsResetWarningMessage: String { 
    return BWIL10n.tr("Bwi", "secrets_reset_warning_message") 
  }
  /// Willst du deinen Wiederherstellungsschlüssel wirklich zurücksetzen?
  public static var secretsResetWarningTitle: String { 
    return BWIL10n.tr("Bwi", "secrets_reset_warning_title") 
  }
  /// Fertig
  public static var secretsSetupRecoveryKeyDoneAction: String { 
    return BWIL10n.tr("Bwi", "secrets_setup_recovery_key_done_action") 
  }
  /// Geschafft!\n\nDer Wiederherstellungsschlüssel wurde erfolgreich gesetzt.
  public static var secretsSetupRecoveryKeyInformation: String { 
    return BWIL10n.tr("Bwi", "secrets_setup_recovery_key_information") 
  }
  /// Wiederherstellungsschlüssel
  public static var secretsSetupRecoveryKeyTitle: String { 
    return BWIL10n.tr("Bwi", "secrets_setup_recovery_key_title") 
  }
  /// ⚠️ Benutze hier nicht dein Passwort.
  public static var secretsSetupRecoveryPassphraseAdditionalInformation: String { 
    return BWIL10n.tr("Bwi", "secrets_setup_recovery_passphrase_additional_information") 
  }
  /// Gib deinen Wiederherstellungsschlüssel zur Bestätigung erneut ein.
  public static var secretsSetupRecoveryPassphraseConfirmInformation: String { 
    return BWIL10n.tr("Bwi", "secrets_setup_recovery_passphrase_confirm_information") 
  }
  /// Wiederherstellungsschlüssel
  public static var secretsSetupRecoveryPassphraseConfirmPassphrasePlaceholder: String { 
    return BWIL10n.tr("Bwi", "secrets_setup_recovery_passphrase_confirm_passphrase_placeholder") 
  }
  /// 
  public static var secretsSetupRecoveryPassphraseConfirmPassphraseTitle: String { 
    return BWIL10n.tr("Bwi", "secrets_setup_recovery_passphrase_confirm_passphrase_title") 
  }
  /// Ein Wiederherstellungsschlüssel kann ein Satz sein, den du dir gut merken kannst.\n\nWenn du diesen vergisst, können deine Nachrichten nicht mehr entschlüsselt werden.
  public static var secretsSetupRecoveryPassphraseInformation: String { 
    return BWIL10n.tr("Bwi", "secrets_setup_recovery_passphrase_information") 
  }
  /// Erinnere dich an deinen Wiederherstellungsschlüssel. Dieser dient zur Entschlüsselung deiner bereits empfangenen Nachrichten und Daten.
  public static var secretsSetupRecoveryPassphraseSummaryInformation: String { 
    return BWIL10n.tr("Bwi", "secrets_setup_recovery_passphrase_summary_information") 
  }
  /// Sichere deinen Wiederherstellungsschlüssel
  public static var secretsSetupRecoveryPassphraseSummaryTitle: String { 
    return BWIL10n.tr("Bwi", "secrets_setup_recovery_passphrase_summary_title") 
  }
  /// Wiederherstellungsschlüssel
  public static var secretsSetupRecoveryPassphraseTitle: String { 
    return BWIL10n.tr("Bwi", "secrets_setup_recovery_passphrase_title") 
  }
  /// Damit du deine Nachrichten bei einem erneuten Login auf diesem Gerät entschlüsseln kannst, richte einen Wiederherstellungsschlüssel ein.
  public static var secureKeyBackupSetupIntroInfo: String { 
    return BWIL10n.tr("Bwi", "secure_key_backup_setup_intro_info") 
  }
  /// Wiederherstellungsschlüssel
  public static var secureKeyBackupSetupIntroTitle: String { 
    return BWIL10n.tr("Bwi", "secure_key_backup_setup_intro_title") 
  }
  /// Nutze deinen Wiederherstellungsschlüssel, um alle deine Nachrichten zu entschlüsseln.
  public static var secureKeyBackupSetupIntroUseSecurityPassphraseInfo: String { 
    return BWIL10n.tr("Bwi", "secure_key_backup_setup_intro_use_security_passphrase_info") 
  }
  /// Wiederherstellungsschlüssel
  public static var secureKeyBackupSetupIntroUseSecurityPassphraseTitle: String { 
    return BWIL10n.tr("Bwi", "secure_key_backup_setup_intro_use_security_passphrase_title") 
  }
  /// Cross-signing ist angeschaltet.
  public static var securitySettingsCrosssigningInfoOk: String { 
    return BWIL10n.tr("Bwi", "security_settings_crosssigning_info_ok") 
  }
  /// Cross-signing zurücksetzen
  public static var securitySettingsCrosssigningReset: String { 
    return BWIL10n.tr("Bwi", "security_settings_crosssigning_reset") 
  }
  /// Vertraue Sitzungen, um Zugriff auf Ende-zu-Ende verschlüsselte Nachrichten zu gewähren. Wenn du eine Sitzung nicht kennst, ändere dein Passwort und setze dein Nachrichtenpasswort für die Nachrichtensicherung zurück.
  public static var securitySettingsCryptoSessionsDescription: String { 
    return BWIL10n.tr("Bwi", "security_settings_crypto_sessions_description") 
  }
  /// Nachrichten wiederherstellen
  public static var securitySettingsSecureBackup: String { 
    return BWIL10n.tr("Bwi", "security_settings_secure_backup") 
  }
  /// Lösche
  public static var securitySettingsSecureBackupDelete: String { 
    return BWIL10n.tr("Bwi", "security_settings_secure_backup_delete") 
  }
  /// Sofern du Probleme mit nicht-entschlüsselbaren Nachrichten hast, kann es helfen, die Schlüssel neu abzurufen. Klicke hierzu auf den Button und warte solange, bis alle Schlüssel wiederhergestellt wurden. Die App muss dazu geöffnet bleiben.
  public static var securitySettingsSecureBackupInfoValid: String { 
    return BWIL10n.tr("Bwi", "security_settings_secure_backup_info_valid") 
  }
  /// Bestätige deine Identität durch Eingabe des Passwort
  public static var securitySettingsUserPasswordDescription: String { 
    return BWIL10n.tr("Bwi", "security_settings_user_password_description") 
  }
  /// Senden an %@
  public static func sendTo(_ p1: String) -> String {
    return BWIL10n.tr("Bwi", "send_to", p1)
  }
  /// Senden
  public static var sending: String { 
    return BWIL10n.tr("Bwi", "sending") 
  }
  /// Erweitert
  public static var settingsAbout: String { 
    return BWIL10n.tr("Bwi", "settings_about") 
  }
  /// Call invitations
  public static var settingsCallInvitations: String { 
    return BWIL10n.tr("Bwi", "settings_call_invitations") 
  }
  /// Auch Nutzer aus föderierten Organisationen können dein Profilbild sehen.
  public static var settingsChangeProfileAvatarHint: String { 
    return BWIL10n.tr("Bwi", "settings_change_profile_avatar_hint") 
  }
  /// Neues Passwort bestätigen
  public static var settingsConfirmPassword: String { 
    return BWIL10n.tr("Bwi", "settings_confirm_password") 
  }
  /// Copyright
  public static var settingsCopyright: String { 
    return BWIL10n.tr("Bwi", "settings_copyright") 
  }
  /// Zurück
  public static var settingsCustomBackButton: String { 
    return BWIL10n.tr("Bwi", "settings_custom_back_button") 
  }
  /// Benachrichtigungen
  public static var settingsDefault: String { 
    return BWIL10n.tr("Bwi", "settings_default") 
  }
  /// Direktnachrichten
  public static var settingsDirectMessages: String { 
    return BWIL10n.tr("Bwi", "settings_direct_messages") 
  }
  /// Der %@ steht von %@, %@ Uhr (UTC%@) bis %@, %@ Uhr (UTC%@) nicht zur Verfügung. Nachrichten können in dieser Zeit nicht verschickt oder empfangen werden.
  public static func settingsDowntimeMessageDifferentDays(_ p1: String, _ p2: String, _ p3: String, _ p4: String, _ p5: String, _ p6: String, _ p7: String) -> String {
    return BWIL10n.tr("Bwi", "settings_downtime_message_different_days", p1, p2, p3, p4, p5, p6, p7)
  }
  /// Der %@ steht am %@, %@ von %@ bis %@ Uhr (UTC%@) nicht zur Verfügung. Nachrichten können in dieser Zeit nicht verschickt oder empfangen werden.
  public static func settingsDowntimeMessageSameDay(_ p1: String, _ p2: String, _ p3: String, _ p4: String, _ p5: String, _ p6: String) -> String {
    return BWIL10n.tr("Bwi", "settings_downtime_message_same_day", p1, p2, p3, p4, p5, p6)
  }
  /// Element-Version %@
  public static func settingsElementVersion(_ p1: String) -> String {
    return BWIL10n.tr("Bwi", "settings_element_version", p1)
  }
  /// Zeige Chateffekte
  public static var settingsEnableChatEffects: String { 
    return BWIL10n.tr("Bwi", "settings_enable_chat_effects") 
  }
  /// Zeige gelöschte Nachrichten
  public static var settingsEnableDeletedMessages: String { 
    return BWIL10n.tr("Bwi", "settings_enable_deleted_messages") 
  }
  /// Zeige Betreten und Verlassen
  public static var settingsEnableEnterRoom: String { 
    return BWIL10n.tr("Bwi", "settings_enable_enter_room") 
  }
  /// Zeige Kontoereignisse
  public static var settingsEnableNameChange: String { 
    return BWIL10n.tr("Bwi", "settings_enable_name_change") 
  }
  /// Benachrichtigungszeiten
  public static var settingsEnableNotificationTimes: String { 
    return BWIL10n.tr("Bwi", "settings_enable_notification_times") 
  }
  /// Zeige Raumereignisse
  public static var settingsEnableRoomAvatar: String { 
    return BWIL10n.tr("Bwi", "settings_enable_room_avatar") 
  }
  /// Zeige Zeitlinienereignisse
  public static var settingsEnableSimpleTimeLine: String { 
    return BWIL10n.tr("Bwi", "settings_enable_simple_time_line") 
  }
  /// Zeige Zeitstempel
  public static var settingsEnableTimeStamp: String { 
    return BWIL10n.tr("Bwi", "settings_enable_time_stamp") 
  }
  /// Zeige Kontoereignisse
  public static var settingsEnableUserAvatar: String { 
    return BWIL10n.tr("Bwi", "settings_enable_user_avatar") 
  }
  /// Direktnachrichten
  public static var settingsEncryptedDirectMessages: String { 
    return BWIL10n.tr("Bwi", "settings_encrypted_direct_messages") 
  }
  /// Räume
  public static var settingsEncryptedGroupMessages: String { 
    return BWIL10n.tr("Bwi", "settings_encrypted_group_messages") 
  }
  /// Das Ändern des Anzeigenamens ist nicht erlaubt
  public static var settingsFailToUpdateProfileMessage: String { 
    return BWIL10n.tr("Bwi", "settings_fail_to_update_profile_message") 
  }
  /// Räume
  public static var settingsGroupMessages: String { 
    return BWIL10n.tr("Bwi", "settings_group_messages") 
  }
  /// Impressum
  public static var settingsImprint: String { 
    return BWIL10n.tr("Bwi", "settings_imprint") 
  }
  /// Mentions and Keywords
  public static var settingsMentionsAndKeywords: String { 
    return BWIL10n.tr("Bwi", "settings_mentions_and_keywords") 
  }
  /// You won’t get notifications for mentions & keywords in encrypted rooms on mobile.
  public static var settingsMentionsAndKeywordsEncryptionNotice: String { 
    return BWIL10n.tr("Bwi", "settings_mentions_and_keywords_encryption_notice") 
  }
  /// Messages by a bot
  public static var settingsMessagesByABot: String { 
    return BWIL10n.tr("Bwi", "settings_messages_by_a_bot") 
  }
  /// @room
  public static var settingsMessagesContainingAtRoom: String { 
    return BWIL10n.tr("Bwi", "settings_messages_containing_at_room") 
  }
  /// Erwähnungen
  public static var settingsMessagesContainingDisplayName: String { 
    return BWIL10n.tr("Bwi", "settings_messages_containing_display_name") 
  }
  /// Keywords
  public static var settingsMessagesContainingKeywords: String { 
    return BWIL10n.tr("Bwi", "settings_messages_containing_keywords") 
  }
  /// Mein Nutzername
  public static var settingsMessagesContainingUserName: String { 
    return BWIL10n.tr("Bwi", "settings_messages_containing_user_name") 
  }
  /// Add new Keyword
  public static var settingsNewKeyword: String { 
    return BWIL10n.tr("Bwi", "settings_new_keyword") 
  }
  /// Neues Passwort
  public static var settingsNewPassword: String { 
    return BWIL10n.tr("Bwi", "settings_new_password") 
  }
  /// Benachrichtige mich für
  public static var settingsNotifyMeFor: String { 
    return BWIL10n.tr("Bwi", "settings_notify_me_for") 
  }
  /// Aktuelles Passwort
  public static var settingsOldPassword: String { 
    return BWIL10n.tr("Bwi", "settings_old_password") 
  }
  /// Erweitert
  public static var settingsOther: String { 
    return BWIL10n.tr("Bwi", "settings_other") 
  }
  /// Dein Passwort wurde erfolgreich geändert.
  public static var settingsPasswordChanged: String { 
    return BWIL10n.tr("Bwi", "settings_password_changed") 
  }
  /// Regeln: Mindestlänge 8 Zeichen, mind. 1 Sonderzeichen, 1 Groß- und 1 Kleinbuchstaben und 1 Ziffer
  public static var settingsPasswordCondition: String { 
    return BWIL10n.tr("Bwi", "settings_password_condition") 
  }
  /// Das Passwort muss mindestens eine Ziffer enthalten
  public static var settingsPasswordHasNoDigit: String { 
    return BWIL10n.tr("Bwi", "settings_password_has_no_digit") 
  }
  /// Das Passwort muss mindestens einen Kleinbuchstaben enthalten
  public static var settingsPasswordHasNoLowercaseLetter: String { 
    return BWIL10n.tr("Bwi", "settings_password_has_no_lowercase_letter") 
  }
  /// Das Passwort muss mindestens ein Sonderzeichen enthalten
  public static var settingsPasswordHasNoSymbol: String { 
    return BWIL10n.tr("Bwi", "settings_password_has_no_symbol") 
  }
  /// Das Passwort muss mindestens einen Großbuchstaben enthalten
  public static var settingsPasswordHasNoUppercaseLetter: String { 
    return BWIL10n.tr("Bwi", "settings_password_has_no_uppercase_letter") 
  }
  /// Das Passwort muss aus mindestens 8 Zeichen bestehen
  public static var settingsPasswordTooShortMessage: String { 
    return BWIL10n.tr("Bwi", "settings_password_too_short_message") 
  }
  /// Permalink Prefix
  public static var settingsPermalinkPrefixPickerTitle: String { 
    return BWIL10n.tr("Bwi", "settings_permalink_prefix_picker_title") 
  }
  /// Einladungen
  public static var settingsRoomInvitations: String { 
    return BWIL10n.tr("Bwi", "settings_room_invitations") 
  }
  /// Room upgrades
  public static var settingsRoomUpgrades: String { 
    return BWIL10n.tr("Bwi", "settings_room_upgrades") 
  }
  /// Nein
  public static var settingsSaveChangesAlertDoNotSaveButton: String { 
    return BWIL10n.tr("Bwi", "settings_save_changes_alert_do_not_save_button") 
  }
  /// Ja
  public static var settingsSaveChangesAlertSaveButton: String { 
    return BWIL10n.tr("Bwi", "settings_save_changes_alert_save_button") 
  }
  /// Möchtest du die Änderungen speichern?
  public static var settingsSaveChangesAlertTitle: String { 
    return BWIL10n.tr("Bwi", "settings_save_changes_alert_title") 
  }
  /// Support
  public static var settingsSupport: String { 
    return BWIL10n.tr("Bwi", "settings_support") 
  }
  /// Diese Aktion kann eine Weile dauern.
  public static var settingsUnignoreUserMessage: String { 
    return BWIL10n.tr("Bwi", "settings_unignore_user_message") 
  }
  /// Your Keywords
  public static var settingsYourKeywords: String { 
    return BWIL10n.tr("Bwi", "settings_your_keywords") 
  }
  /// Dies ist der QR Code zu deinem Profil.\nLasse den QR Code mit der %@ App scannen, damit andere mit dir in Kontakt treten können.
  public static func showMyQrScreenMessage(_ p1: String) -> String {
    return BWIL10n.tr("Bwi", "show_my_qr_screen_message", p1)
  }
  /// Mein QR Code
  public static var showMyQrScreenTitle: String { 
    return BWIL10n.tr("Bwi", "show_my_qr_screen_title") 
  }
  /// Meinen QR Code anzeigen
  public static var showMyQrSettingsTitle: String { 
    return BWIL10n.tr("Bwi", "show_my_qr_settings_title") 
  }
  /// Überspringen
  public static var skip: String { 
    return BWIL10n.tr("Bwi", "skip") 
  }
  /// Raumbild ändern
  public static var spaceAvatarViewAccessibilityHint: String { 
    return BWIL10n.tr("Bwi", "space_avatar_view_accessibility_hint") 
  }
  /// Raumbild
  public static var spaceAvatarViewAccessibilityLabel: String { 
    return BWIL10n.tr("Bwi", "space_avatar_view_accessibility_label") 
  }
  /// Chats mit Kameraden und Kollegen
  public static var splashScreenFirstInfo: String { 
    return BWIL10n.tr("Bwi", "splash_screen_first_info") 
  }
  /// Sicher Medien und Dateien übertragen
  public static var splashScreenSecondInfo: String { 
    return BWIL10n.tr("Bwi", "splash_screen_second_info") 
  }
  /// Loslegen
  public static var splashScreenStart: String { 
    return BWIL10n.tr("Bwi", "splash_screen_start") 
  }
  /// Immer verschlüsselt
  public static var splashScreenThirdInfo: String { 
    return BWIL10n.tr("Bwi", "splash_screen_third_info") 
  }
  /// Dein Messenger für unser Land
  public static var splashScreenTitle: String { 
    return BWIL10n.tr("Bwi", "splash_screen_title") 
  }
  /// Authentifizierung über Schema %@ konnte nicht geöffnet werden. Bitte wende dich an deinen Support.
  public static func ssoAuthenticationUrlSchemeErrorMessage(_ p1: String) -> String {
    return BWIL10n.tr("Bwi", "sso_authentication_url_scheme_error_message", p1)
  }
  /// Fehler bei Authentifizierung
  public static var ssoAuthenticationUrlSchemeErrorTitle: String { 
    return BWIL10n.tr("Bwi", "sso_authentication_url_scheme_error_title") 
  }
  /// Der Server ist momentan nicht erreichbar. Versuche es später erneut.
  public static var standardErrorAlertTitleDowntime: String { 
    return BWIL10n.tr("Bwi", "standard_error_alert_title_downtime") 
  }
  /// Der Server ist momentan nicht erreichbar. Versuche es später erneut.
  public static var standardErrorAlertTitleNoDowntime: String { 
    return BWIL10n.tr("Bwi", "standard_error_alert_title_no_downtime") 
  }
  /// Starte
  public static var start: String { 
    return BWIL10n.tr("Bwi", "start") 
  }
  /// Switch
  public static var `switch`: String { 
    return BWIL10n.tr("Bwi", "switch") 
  }
  /// Texteingabe verbergen
  public static var textfieldHideSecret: String { 
    return BWIL10n.tr("Bwi", "textfield_hide_secret") 
  }
  /// Texteingabe anzeigen
  public static var textfieldRevealSecret: String { 
    return BWIL10n.tr("Bwi", "textfield_reveal_secret") 
  }
  /// Favoriten
  public static var titleFavourites: String { 
    return BWIL10n.tr("Bwi", "title_favourites") 
  }
  /// Personen
  public static var titlePeople: String { 
    return BWIL10n.tr("Bwi", "title_people") 
  }
  /// Räume
  public static var titleRooms: String { 
    return BWIL10n.tr("Bwi", "title_rooms") 
  }
  /// Profilbild ändern
  public static var userAvatarViewAccessibilityHint: String { 
    return BWIL10n.tr("Bwi", "user_avatar_view_accessibility_hint") 
  }
  /// Profilbild
  public static var userAvatarViewAccessibilityLabel: String { 
    return BWIL10n.tr("Bwi", "user_avatar_view_accessibility_label") 
  }
  /// Alle anzeigen (%d)
  public static func userSessionButtonViewAll(_ p1: Int) -> String {
    return BWIL10n.tr("Bwi", "user_session_button_view_all", p1)
  }
  /// Du hast deine Sitzung durch Eingabe des Wiederherstellungsschlüssels oder durch die Verifizierung mit einem anderen Gerät bestätigt. Dies bedeutet, dass du alle Schlüssel zum Entschlüsseln deiner Nachrichten hast und anderen bestätigst, dieser Sitzung zu vertrauen.
  public static var userSessionVerifiedSessionDescription: String { 
    return BWIL10n.tr("Bwi", "user_session_verified_session_description") 
  }
  /// Sitzung verifizieren
  public static var userVerificationSessionDetailsVerifyActionCurrentUser: String { 
    return BWIL10n.tr("Bwi", "user_verification_session_details_verify_action_current_user") 
  }
  /// Ansehen
  public static var view: String { 
    return BWIL10n.tr("Bwi", "view") 
  }
  /// Sprachnachricht
  public static var voiceMessageLockScreenPlaceholder: String { 
    return BWIL10n.tr("Bwi", "voice_message_lock_screen_placeholder") 
  }
  /// Zur Aufnahme halten, zum Senden loslassen
  public static var voiceMessageReleaseToSend: String { 
    return BWIL10n.tr("Bwi", "voice_message_release_to_send") 
  }
  /// noch %@s
  public static func voiceMessageRemainingRecordingTime(_ p1: String) -> String {
    return BWIL10n.tr("Bwi", "voice_message_remaining_recording_time", p1)
  }
  /// Wischen zum Abbrechen
  public static var voiceMessageSlideToCancel: String { 
    return BWIL10n.tr("Bwi", "voice_message_slide_to_cancel") 
  }
  /// Klick die Nachricht um zu stoppen oder zu hören
  public static var voiceMessageStopLockedModeRecording: String { 
    return BWIL10n.tr("Bwi", "voice_message_stop_locked_mode_recording") 
  }
  /// Warnung
  public static var warning: String { 
    return BWIL10n.tr("Bwi", "warning") 
  }
  /// Aus Deutschland, für Deutschland: Der souveräne Messenger für die besonderen Anforderungen des öffentlichen Sektors.
  public static var welcomeExperienceDescription1: String { 
    return BWIL10n.tr("Bwi", "welcome_experience_description1") 
  }
  /// Von überall auf die eigenen Chats zugreifen und nahtlos zwischen Smartphone, Tablet und Computer wechseln. Und: Alle Daten sind Ende-zu-Ende verschlüsselt auf eigenen Servern gespeichert.
  public static var welcomeExperienceDescription2: String { 
    return BWIL10n.tr("Bwi", "welcome_experience_description2") 
  }
  /// Kolleg*innen aus der gesamten Organisation sofort finden, und zwar ohne erst nach Kontaktdaten suchen zu müssen.
  public static var welcomeExperienceDescription3: String { 
    return BWIL10n.tr("Bwi", "welcome_experience_description3") 
  }
  /// Text, Bilder, Videos, Dokumente, Standorte oder Sprachnachrichten versenden und vieles mehr – an einzelne Personen oder in Gruppen. Und auf Wunsch auch über die Organisationsgrenze hinweg.
  public static var welcomeExperienceDescription4: String { 
    return BWIL10n.tr("Bwi", "welcome_experience_description4") 
  }
  /// Ob deine Organisation den BundesMessenger schon eingeführt hat, findest du auf der nächsten Seite heraus. Falls nicht: Überzeuge deinen Admin doch einfach!
  public static var welcomeExperienceDescription5: String { 
    return BWIL10n.tr("Bwi", "welcome_experience_description5") 
  }
  /// Loslegen
  public static var welcomeExperienceStart: String { 
    return BWIL10n.tr("Bwi", "welcome_experience_start") 
  }
  /// Willkommen beim BundesMessenger
  public static var welcomeExperienceTitle1: String { 
    return BWIL10n.tr("Bwi", "welcome_experience_title1") 
  }
  /// Sicher und von überall
  public static var welcomeExperienceTitle2: String { 
    return BWIL10n.tr("Bwi", "welcome_experience_title2") 
  }
  /// Einfach vernetzen
  public static var welcomeExperienceTitle3: String { 
    return BWIL10n.tr("Bwi", "welcome_experience_title3") 
  }
  /// Chatten wie gewohnt
  public static var welcomeExperienceTitle4: String { 
    return BWIL10n.tr("Bwi", "welcome_experience_title4") 
  }
  /// Gleich loslegen?
  public static var welcomeExperienceTitle5: String { 
    return BWIL10n.tr("Bwi", "welcome_experience_title5") 
  }
}
// swiftlint:enable function_parameter_count identifier_name line_length type_body_length

// MARK: - Implementation Details

extension BWIL10n {
  static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = NSLocalizedString(key, tableName: table, bundle: bundle, comment: "")
    let locale = LocaleProvider.locale ?? Locale.current    
    return String(format: format, locale: locale, arguments: args)
  }
  /// The bundle to load strings from. This will be the app's bundle unless running
  /// the UI tests target, in which case the strings are contained in the tests bundle.
  static let bundle: Bundle = {
    if ProcessInfo.processInfo.environment["XCTestConfigurationFilePath"] != nil {
      // The tests bundle is embedded inside a runner. Find the bundle for VectorL10n.
      return Bundle(for: VectorL10n.self)
    }
    return Bundle.app
  }()
}

