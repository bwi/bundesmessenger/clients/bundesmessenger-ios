// 
// Copyright 2024 New Vector Ltd.
// Copyright 2020 Vector Creations Ltd
// Copyright (c) 2021 BWI GmbH
//
// SPDX-License-Identifier: AGPL-3.0-only
// Please see LICENSE in the repository root for full details.
//

import Foundation

// bwi: sexucity fix: lastActiveTime is now set by AppDelegate and only when state pinUnLocked is reached

@objcMembers
class LocalAuthenticationService: NSObject {
    
    private let pinCodePreferences: PinCodePreferences
    
    init(pinCodePreferences: PinCodePreferences) {
        self.pinCodePreferences = pinCodePreferences
        super.init()
        
        setup()
    }
    
    private var appLastActiveTime: TimeInterval?
    
    private var systemUptime: TimeInterval {
        var uptime = timespec()
        if 0 != clock_gettime(CLOCK_MONOTONIC_RAW, &uptime) {
            fatalError("Could not execute clock_gettime, errno: \(errno)")
        }

        return TimeInterval(uptime.tv_sec)
    }
    
    private func setup() {
        NotificationCenter.default.addObserver(self, selector: #selector(biometricsDidFallbackToPin), name: .biometricsDidFallbackToPin, object: nil)
    }
    
    var shouldShowPinCode: Bool {
        if !pinCodePreferences.isPinSet && !pinCodePreferences.isBiometricsSet {
            return false
        }
        if MXKAccountManager.shared()?.activeAccounts.count == 0 {
            return false
        }
        guard let appLastActiveTime = appLastActiveTime else {
            return true
        }
        return (systemUptime - appLastActiveTime) >= pinCodePreferences.graceTimeInSeconds
    }
    
    var isProtectionSet: Bool {
        return pinCodePreferences.isPinSet || pinCodePreferences.isBiometricsSet
    }
    
    func setLastActiveTime() {
        appLastActiveTime = systemUptime
    }
    
    @objc private func biometricsDidFallbackToPin() {
        //  fallback to pin from biometrics, do not care about timing
        appLastActiveTime = nil
    }
    
    func shouldLogOutUser() -> Bool {
        if BWIBuildSettings.shared.logOutUserWhenPINFailuresExceeded && pinCodePreferences.numberOfPinFailures >= pinCodePreferences.maxAllowedNumberOfPinFailures {
            return true
        }
        if BWIBuildSettings.shared.logOutUserWhenBiometricsFailuresExceeded && pinCodePreferences.numberOfBiometricsFailures >= pinCodePreferences.maxAllowedNumberOfBiometricsFailures {
            return true
        }
        return false
    }

}
