// 
// Copyright 2024 New Vector Ltd.
// Copyright 2020 Vector Creations Ltd
// Copyright (c) 2021 BWI GmbH
//
// SPDX-License-Identifier: AGPL-3.0-only
// Please see LICENSE in the repository root for full details.
//

import Foundation
import KeychainAccess
import MatrixSDK

@objcMembers
final class PushNotificationStore: NSObject {
    
    // MARK: - Constants
    
    private struct PushNotificationConstants {
        static let pushNotificationKeychainService: String = BuildSettings.baseBundleIdentifier + ".pushnotification-service"
    }
    
    private struct StoreKeys {
        static let pushToken: String = "pushtoken"
    }
    
    private let vault: KeyValueVault
    
    override init() {
        vault = KeychainVault(Keychain(service: PushNotificationConstants.pushNotificationKeychainService,
                                                     accessGroup: BuildSettings.keychainAccessGroup))
        super.init()
    }
    
    /// Saved PushKit token
    var pushKitToken: Data? {
        get {
            do {
                return try vault.data(forKey: StoreKeys.pushToken)
            } catch let error {
                MXLog.debug("[PinCodePreferences] Error when reading push token from vault: \(error)")
                return nil
            }
        } set {
            do {
                try vault.set(newValue, forKey: StoreKeys.pushToken)
            } catch let error {
                MXLog.debug("[PinCodePreferences] Error when storing push token to the vault: \(error)")

            }
        }
    }
    
    func callInvite(forEventId eventId: String) -> MXEvent? {
        guard let data = try? vault.data(forKey: eventId) else {
            return nil
        }
        return NSKeyedUnarchiver.unarchiveObject(with: data) as? MXEvent
    }
    
    func storeCallInvite(_ event: MXEvent) {
        let data = NSKeyedArchiver.archivedData(withRootObject: event)
        try? vault.set(data, forKey: event.eventId)
    }
    
    func removeCallInvite(withEventId eventId: String) {
        try? vault.removeObject(forKey: eventId)
    }
    
    func reset() {
        try? vault.reset()
    }
}
