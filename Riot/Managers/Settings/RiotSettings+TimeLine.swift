/*
 * Copyright (c) 2021 BWI GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Foundation


extension RiotSettings {
    private enum UserDefaultsTimelineKeys {
        static let settingsScreenShowSimpleTimeLineOptions = "settingsScreenShowSimpleTimeLineOptions"
        static let settingsScreenShowTimeStampOption = "settingsScreenShowTimeStampOption"
        static let settingsScreenShowDeletedMessagesOption = "settingsScreenShowDeletedMessagesOption"
        static let settingsScreenShowNameChangeOption = "settingsScreenShowNameChangeOption"
        static let settingsScreenShowChatEffectsOption = "settingsScreenShowChatEffectsOption"
        static let settingsScreenShowRoomAvatarChangeOption = "settingsScreenShowRoomAvatarChangeOption"
        static let settingsScreenShowUserAvatarChangeOption = "settingsScreenShowUserAvatarChangeOption"
        static let settingsScreenShowEnterRoomOption = "settingsScreenShowEnterRoomOption"
        static let settingSimpleTimeLineEnabled = "settingSimpleTimelineEnabled"
        static let settingTimeStampEnabled = "settingTimeStampEnabled"
        static let settingDeletedMessagesEnabled = "settingDeletedMessagesEnabled"
        static let settingNameChangeEnabled = "settingNameChangeEnabled"
        static let settingChatEffectsEnabled = "settingChatEffectsEnabled"
        static let settingRoomAvatarChangeEnabled = "settingRoomAvatarChangeEnabled"
        static let settingUserAvatarChangeEnabled = "settingUserAvatarChangeEnabled"
        static let settingEnterRoomEnabled = "settingEnterRoomEnabled"
    }
    
    var settingsScreenShowSimpleTimeLineOptions: Bool {
        get {
            guard RiotSettings.defaults.object(forKey: UserDefaultsTimelineKeys.settingsScreenShowSimpleTimeLineOptions) != nil else {
                return BWIBuildSettings.shared.settingsScreenShowSimpleTimeLineOptions
            }
            return RiotSettings.defaults.bool(forKey: UserDefaultsTimelineKeys.settingsScreenShowSimpleTimeLineOptions)
        } set {
            RiotSettings.defaults.set(newValue, forKey: UserDefaultsTimelineKeys.settingsScreenShowSimpleTimeLineOptions)
        }
    }
    
    var settingsScreenShowTimeStampOption: Bool {
        get {
            guard RiotSettings.defaults.object(forKey: UserDefaultsTimelineKeys.settingsScreenShowTimeStampOption) != nil else {
                return BWIBuildSettings.shared.settingsScreenShowTimeStampOption
            }
            return RiotSettings.defaults.bool(forKey: UserDefaultsTimelineKeys.settingsScreenShowTimeStampOption)
        } set {
            RiotSettings.defaults.set(newValue, forKey: UserDefaultsTimelineKeys.settingsScreenShowTimeStampOption)
        }
    }
    
    var settingsScreenShowDeletedMessagesOption: Bool {
        get {
            guard RiotSettings.defaults.object(forKey: UserDefaultsTimelineKeys.settingsScreenShowDeletedMessagesOption) != nil else {
                return BWIBuildSettings.shared.settingsScreenShowDeletedMessagesOption
            }
            return RiotSettings.defaults.bool(forKey: UserDefaultsTimelineKeys.settingsScreenShowDeletedMessagesOption)
        } set {
            RiotSettings.defaults.set(newValue, forKey: UserDefaultsTimelineKeys.settingsScreenShowDeletedMessagesOption)
        }
    }
    
    var settingsScreenShowNameChangeOption: Bool {
        get {
            guard RiotSettings.defaults.object(forKey: UserDefaultsTimelineKeys.settingsScreenShowNameChangeOption) != nil else {
                return BWIBuildSettings.shared.settingsScreenShowNameChangeOption
            }
            return RiotSettings.defaults.bool(forKey: UserDefaultsTimelineKeys.settingsScreenShowNameChangeOption)
        } set {
            RiotSettings.defaults.set(newValue, forKey: UserDefaultsTimelineKeys.settingsScreenShowNameChangeOption)
        }
    }
    
    var settingsScreenShowChatEffectsOption: Bool {
        get {
            guard RiotSettings.defaults.object(forKey: UserDefaultsTimelineKeys.settingsScreenShowChatEffectsOption) != nil else {
                return BWIBuildSettings.shared.settingsScreenShowChatEffectsOption
            }
            return RiotSettings.defaults.bool(forKey: UserDefaultsTimelineKeys.settingsScreenShowChatEffectsOption)
        } set {
            RiotSettings.defaults.set(newValue, forKey: UserDefaultsTimelineKeys.settingsScreenShowChatEffectsOption)
        }
    }
    
    var settingsScreenShowRoomAvatarChangeOption: Bool {
        get {
            guard RiotSettings.defaults.object(forKey: UserDefaultsTimelineKeys.settingsScreenShowRoomAvatarChangeOption) != nil else {
                return BWIBuildSettings.shared.settingsScreenShowRoomAvatarChangeOption
            }
            return RiotSettings.defaults.bool(forKey: UserDefaultsTimelineKeys.settingsScreenShowRoomAvatarChangeOption)
        } set {
            RiotSettings.defaults.set(newValue, forKey: UserDefaultsTimelineKeys.settingsScreenShowRoomAvatarChangeOption)
        }
    }
    
    var settingsScreenShowUserAvatarChangeOption: Bool {
        get {
            guard RiotSettings.defaults.object(forKey: UserDefaultsTimelineKeys.settingsScreenShowUserAvatarChangeOption) != nil else {
                return BWIBuildSettings.shared.settingsScreenShowUserAvatarChangeOption
            }
            return RiotSettings.defaults.bool(forKey: UserDefaultsTimelineKeys.settingsScreenShowUserAvatarChangeOption)
        } set {
            RiotSettings.defaults.set(newValue, forKey: UserDefaultsTimelineKeys.settingsScreenShowUserAvatarChangeOption)
        }
    }
    
    var settingsScreenShowEnterRoomOption: Bool {
        get {
            guard RiotSettings.defaults.object(forKey: UserDefaultsTimelineKeys.settingsScreenShowEnterRoomOption) != nil else {
                return BWIBuildSettings.shared.settingsScreenShowEnterRoomOption
            }
            return RiotSettings.defaults.bool(forKey: UserDefaultsTimelineKeys.settingsScreenShowEnterRoomOption)
        } set {
            RiotSettings.defaults.set(newValue, forKey: UserDefaultsTimelineKeys.settingsScreenShowEnterRoomOption)
        }
    }
    
    var settingSimpleTimelineEnabled: Bool {
        get {
            return RiotSettings.defaults.bool(forKey: UserDefaultsTimelineKeys.settingSimpleTimeLineEnabled)
        } set {
            RiotSettings.defaults.set(newValue, forKey: UserDefaultsTimelineKeys.settingSimpleTimeLineEnabled)
        }
    }
    
    var settingTimeStampEnabled: Bool {
        get {
            return RiotSettings.defaults.bool(forKey: UserDefaultsTimelineKeys.settingTimeStampEnabled)
        } set {
            RiotSettings.defaults.set(newValue, forKey: UserDefaultsTimelineKeys.settingTimeStampEnabled)
        }
    }
    
    var settingDeletedMessagesEnabled: Bool {
        get {
            return RiotSettings.defaults.bool(forKey: UserDefaultsTimelineKeys.settingDeletedMessagesEnabled)
        } set {
            RiotSettings.defaults.set(newValue, forKey: UserDefaultsTimelineKeys.settingDeletedMessagesEnabled)
        }
    }
    
    var settingNameChangeEnabled: Bool {
        get {
            return RiotSettings.defaults.bool(forKey: UserDefaultsTimelineKeys.settingNameChangeEnabled)
        } set {
            RiotSettings.defaults.set(newValue, forKey: UserDefaultsTimelineKeys.settingNameChangeEnabled)
        }
    }
    
    var settingChatEffectsEnabled: Bool {
        get {
            return RiotSettings.defaults.bool(forKey: UserDefaultsTimelineKeys.settingChatEffectsEnabled)
        } set {
            RiotSettings.defaults.set(newValue, forKey: UserDefaultsTimelineKeys.settingChatEffectsEnabled)
        }
    }
    
    var settingRoomAvatarChangeEnabled: Bool {
        get {
            return RiotSettings.defaults.bool(forKey: UserDefaultsTimelineKeys.settingRoomAvatarChangeEnabled)
        } set {
            RiotSettings.defaults.set(newValue, forKey: UserDefaultsTimelineKeys.settingRoomAvatarChangeEnabled)
        }
    }
    
    var settingUserAvatarChangeEnabled: Bool {
        get {
            return RiotSettings.defaults.bool(forKey: UserDefaultsTimelineKeys.settingUserAvatarChangeEnabled)
        } set {
            RiotSettings.defaults.set(newValue, forKey: UserDefaultsTimelineKeys.settingUserAvatarChangeEnabled)
        }
    }
    
    var settingEnterRoomEnabled: Bool {
        get {
            return RiotSettings.defaults.bool(forKey: UserDefaultsTimelineKeys.settingEnterRoomEnabled)
        } set {
            RiotSettings.defaults.set(newValue, forKey: UserDefaultsTimelineKeys.settingEnterRoomEnabled)
        }
    }
}
