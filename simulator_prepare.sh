#!/bin/sh

deviceName=$1
deviceType=$2

xcrun simctl delete $deviceName

deviceID=$(xcrun simctl create $deviceName com.apple.CoreSimulator.SimDeviceType.$deviceType)
echo $deviceID

xcrun simctl boot $deviceName

xcrun simctl spawn $deviceName launchctl print system | grep group.com.apple.FileProvider.LocalStorage

echo "booted"

pwdPath=$(pwd)
mediaResPath=$(printf "%s/../RiotUITests/testAssets/Media" "$pwdPath")

for filePath in $(find $mediaResPath*/* -maxdepth 1)
do
	echo "Add $filePath"
    xcrun simctl addmedia $deviceName $filePath
done

echo "Media Added"

sleep 1

documentPath=$(printf "%s/Library/Developer/CoreSimulator/Devices/%s/data/Containers/Shared/AppGroup/%s" $HOME $deviceID)
fileResPath=$(printf "%s/RiotUITests/testAssets/Files" "$pwdPath")

IFS=$'\n'

echo $documentPath

for directory in `find $documentPath*/File* -type d`
do
	for filePath in `find $fileResPath*/* -maxdepth 1 -type f`
	do
		echo "Add $filePath"
		target=$(printf "%s/" "$directory")
    	cp $filePath "$target"
	done
done

echo "Files Added"

echo "done"

exit 0