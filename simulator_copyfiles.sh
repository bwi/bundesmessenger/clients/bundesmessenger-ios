#!/bin/sh


documentPath=$(printf $HOME "/Library/Developer/CoreSimulator/Devices/%s/data/Containers/Shared/AppGroup/" $1)
printf "Docpath %s\n" $documentPath 

for directory in $(find $documentPath*/* -maxdepth 1)
do
    echo $directory
done

echo "done"