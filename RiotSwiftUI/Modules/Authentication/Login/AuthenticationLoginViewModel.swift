//
// Copyright 2021-2024 New Vector Ltd.
//
// SPDX-License-Identifier: AGPL-3.0-only
// Please see LICENSE in the repository root for full details.
//

import SwiftUI

typealias AuthenticationLoginViewModelType = StateStoreViewModel<AuthenticationLoginViewState, AuthenticationLoginViewAction>

class AuthenticationLoginViewModel: AuthenticationLoginViewModelType, AuthenticationLoginViewModelProtocol {
    // MARK: - Properties

    // MARK: Public

    var callback: (@MainActor (AuthenticationLoginViewModelResult) -> Void)?

    // MARK: - Setup

    init(homeserver: AuthenticationHomeserverViewData) {
        let bindings = AuthenticationLoginBindings()
        let viewState = AuthenticationLoginViewState(homeserver: homeserver, bindings: bindings)
        
        super.init(initialViewState: viewState)
    }
    
    // MARK: - Public

    override func process(viewAction: AuthenticationLoginViewAction) {
        switch viewAction {
        case .selectServer:
            Task { await callback?(.selectServer) }
        case .parseUsername:
            Task { await callback?(.parseUsername(state.bindings.username)) }
        case .forgotPassword:
            Task { await callback?(.forgotPassword) }
        case .next:
            Task { await callback?(.login(username: state.bindings.username, password: state.bindings.password)) }
        case .fallback:
            Task { await callback?(.fallback) }
        case .continueWithSSO(let provider):
            Task { await callback?(.continueWithSSO(provider)) }
        case .qrLogin:
            Task { await callback?(.qrLogin) }
        case .register:
            Task { await callback?(.register) }
        case .accessibilityDeclaration:
            Task { await callback?(.accessibilityDeclaration) }
        }
    }
    
    @MainActor func update(isLoading: Bool) {
        guard state.isLoading != isLoading else { return }
        state.isLoading = isLoading
    }
    
    @MainActor func update(homeserver: AuthenticationHomeserverViewData) {
        state.homeserver = homeserver
    }
    
    @MainActor func displayError(_ type: AuthenticationLoginErrorType) {
        switch type {
        case .mxError(let message):
            state.bindings.alertInfo = AlertInfo(id: type,
                                                 title: VectorL10n.error,
                                                 message: message)
        case .invalidHomeserver:
            state.bindings.alertInfo = AlertInfo(id: type,
                                                 title: VectorL10n.error,
                                                 message: VectorL10n.authenticationServerSelectionGenericError)
        case .unknown:
            state.bindings.alertInfo = AlertInfo(id: type)
        case .appVersion:
            state.bindings.alertInfo = AlertInfo(id: type,
                                                 title: BWIL10n.bwiDeprecatedVersionWarningTitle,
                                                 message: BWIL10n.bwiDeprecatedVersionWarningMessage,
                                                 primaryButton: (BWIL10n.bwiDeprecatedVersionAppstoreButton, {
                
                UIApplication.shared.vc_open(URL(string: BWIBuildSettings.shared.itunesAppLink)!, completionHandler: nil)
            }),
                                                 secondaryButton: (VectorL10n.ok, {}))

        }
    }
    
    // bwi: show custom alert
    @MainActor func displayInfoAlert(_ type: AuthenticationLoginViewAction) {
        switch type {
        case .forgotPassword:
            state.bindings.alertInfo = AlertInfo(id: .unknown,
                                                 title: BWIL10n.authForgotPassword,
                                                 message: BWIL10n.bwiAuthForgotPasswordAlertText)
        case .register:
            state.bindings.alertInfo = AlertInfo(id: .unknown,
                                                 title: BWIL10n.bwiAuthRegisterAlertTitle,
                                                 message: BWIL10n.bwiAuthRegisterAlertText)
        default:
            return
        }
    }
}
