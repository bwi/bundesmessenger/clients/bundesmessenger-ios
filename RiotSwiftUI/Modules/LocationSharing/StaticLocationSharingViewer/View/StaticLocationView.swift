//
// Copyright 2021-2024 New Vector Ltd.
//
// SPDX-License-Identifier: AGPL-3.0-only
// Please see LICENSE in the repository root for full details.
//

import SwiftUI

struct StaticLocationView: View {
    // MARK: - Properties
    
    // MARK: Private
    
    @Environment(\.theme) private var theme
    @Environment(\.openURL) var openURL
    @Environment(\.safeAreaInsets) private var safeAreaInsets
    
    // MARK: Public
    
    @ObservedObject var viewModel: StaticLocationViewingViewModel.Context
    
    // MARK: Views
    
    var mapView: LocationSharingMapView {
        LocationSharingMapView(tileServerMapURL: viewModel.viewState.mapStyleURL,
                               annotations: [viewModel.viewState.sharedAnnotation],
                               highlightedAnnotation: viewModel.viewState.sharedAnnotation,
                               userAvatarData: nil,
                               showsUserLocationMode: viewModel.viewState.showsUserLocationMode,
                               userLocation: Binding.constant(nil),
                               mapCenterCoordinate: Binding.constant(nil),
                               errorSubject: viewModel.viewState.errorSubject,
                               attributionsChanged: { attribution in
                                    viewModel.send(viewAction: .showMapCredit(mapAttribution: attribution))
                               })
    }
    
    var body: some View {
        NavigationView {
            ZStack(alignment: .bottom) {
                LocationSharingMapView(tileServerMapURL: viewModel.viewState.mapStyleURL,
                                       annotations: [viewModel.viewState.sharedAnnotation],
                                       highlightedAnnotation: viewModel.viewState.sharedAnnotation,
                                       userAvatarData: viewModel.viewState.userAvatarData,
                                       showsUserLocationMode: ShowUserLocationMode.hide,
                                       userLocation: Binding.constant(nil),
                                       mapCenterCoordinate: Binding.constant(nil),
                                       errorSubject: viewModel.viewState.errorSubject,
                                       attributionsChanged: { attribution in
                                            viewModel.send(viewAction: .showMapCredit(mapAttribution: attribution))
                                       })
                MapCreditsView(attributions: viewModel.viewState.attribution, action: {
                    viewModel.send(viewAction: .mapCreditsDidTap)
                })
                .padding(.bottom, 10.0 + safeAreaInsets.bottom)
                .actionSheet(isPresented: $viewModel.showMapCreditsSheet) {
                    MapCreditsActionSheet(attribution:viewModel.viewState.attribution,
                                          openURL: { url in
                        UIApplication.shared.vc_open(url, completionHandler: nil)
                    }).sheet
                }
            }
            .overlay(CenterToUserLocationButton(action: {
                viewModel.send(viewAction: .showUserLocation)
            }).offset(x: -11.0, y: 52), alignment: .topTrailing)
            .ignoresSafeArea(.all, edges: [.bottom])
            .toolbar {
                ToolbarItem(placement: .navigationBarLeading) {
                    Button {
                        viewModel.send(viewAction: .close)
                    } label: {
                        Text(VectorL10n.cancel)
                            .foregroundColor(Color(ThemeService.shared().theme.tintColor))
                    }

                }
                ToolbarItem(placement: .principal) {
                    Text(VectorL10n.locationSharingTitle)
                        .font(.headline)
                        .foregroundColor(theme.colors.primaryContent)
                }
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button {
                        viewModel.send(viewAction: .share)
                    } label: {
                        Image(uiImage: Asset.Images.locationShareIcon.image)
                            .foregroundColor(Color(ThemeService.shared().theme.tintColor))
                    }
                    .disabled(!viewModel.viewState.shareButtonEnabled)
                    .accessibilityIdentifier("shareButton")
                    .opacity(BWIBuildSettings.shared.bwiLocationShareButtonVisible ? 1.0 : 0.0)
                }
            }
            .navigationBarTitleDisplayMode(.inline)
            // bwi release 2.20.0 disable location share button
             //   .introspectNavigationController { navigationController in
             //       ThemeService.shared().theme.applyStyle(onNavigationBar: navigationController.navigationBar)
            .alert(item: $viewModel.alertInfo) { info in
                info.alert
            }
        }
        .accentColor(theme.colors.accent)
        .activityIndicator(show: viewModel.viewState.showLoadingIndicator)
        .navigationViewStyle(StackNavigationViewStyle())
    }
    
    @ViewBuilder
    private var activityIndicator: some View {
        if viewModel.viewState.showLoadingIndicator {
            ActivityIndicator()
        }
    }
}

// MARK: - Previews

struct StaticLocationSharingViewer_Previews: PreviewProvider {
    static let stateRenderer = MockStaticLocationViewingScreenState.stateRenderer
    static var previews: some View {
        stateRenderer.screenGroup()
    }
}
