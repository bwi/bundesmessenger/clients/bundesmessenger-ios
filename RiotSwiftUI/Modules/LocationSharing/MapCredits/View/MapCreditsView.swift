//
// Copyright 2021-2024 New Vector Ltd.
//
// SPDX-License-Identifier: AGPL-3.0-only
// Please see LICENSE in the repository root for full details.
//

import SwiftUI

struct MapCreditsView: View {
    // MARK: - Properties
    
    // MARK: Private
    
    @Environment(\.theme) private var theme: ThemeSwiftUI
    
    // MARK: Public
    
    // MARK: Public
    
    // bwi #5379 use dynamic attributions from style.json
    @ObservedObject var attributions:LocationSharingAttribution
    
    var action: (() -> Void)?
    
    var body: some View {
        HStack {
            Spacer() 
            Button {
                action?()
            } label: {
                Text(copyrightText(attribution:attributions))
                    .font(theme.fonts.footnote)
                    .foregroundColor(theme.colors.accent)
            }
            .padding(.horizontal)
        }
    }
    
    func copyrightText( attribution: LocationSharingAttribution) -> String {
        var copyright = ""
        
        for copyrightText in attribution.copyrightTexts {
            copyright.append(copyrightText)
        }
        
        return copyright
    }
}

struct MapCreditsView_Previews: PreviewProvider {
    static var previews: some View {
        
        MapCreditsView(attributions: LocationSharingAttribution(copyrightTexts: [""], copyrightLinks: [URL(string: "www.someurl.org")]))
    }
}
