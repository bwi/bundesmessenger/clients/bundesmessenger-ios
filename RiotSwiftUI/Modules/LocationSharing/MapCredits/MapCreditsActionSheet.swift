//
// Copyright 2022-2024 New Vector Ltd.
//
// SPDX-License-Identifier: AGPL-3.0-only
// Please see LICENSE in the repository root for full details.
//

import SwiftUI

struct MapCreditsActionSheet {
    // bwi #5379 dynamic attribution from style.json
    let attribution: LocationSharingAttribution
    
    // Open URL action
    let openURL: (URL) -> Void
    
    // Map credits action sheet
    var sheet: ActionSheet {
        ActionSheet(title: Text(BWIL10n.locationSharingMapCreditsTitle),
                    buttons: self.creditButtons(attribution: attribution))
    }
    
    func creditButtons( attribution: LocationSharingAttribution) -> [ActionSheet.Button] {
        
        var buttons = [ActionSheet.Button]()
        
        // bwi #5379 a bit scetchy but you can asume that url and text have the same index
        for (index, copyright) in attribution.copyrightTexts.enumerated() {
            buttons.append(.default(Text(copyright)) {
                if let url = attribution.copyrightLinks[index] {
                    openURL(url)
                }
            })
        }
        buttons.append(.cancel())
        
        return buttons
    }
}
