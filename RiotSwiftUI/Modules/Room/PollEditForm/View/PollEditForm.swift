//
// Copyright 2021-2024 New Vector Ltd.
//
// SPDX-License-Identifier: AGPL-3.0-only
// Please see LICENSE in the repository root for full details.
//

import SwiftUI

struct PollEditForm: View {
    // MARK: - Properties
    
    // MARK: Private
    
    @Environment(\.theme) private var theme: ThemeSwiftUI
    
    // MARK: Public
    
    @ObservedObject var viewModel: PollEditFormViewModel.Context
    
    var body: some View {
        NavigationView {
            GeometryReader { proxy in
                ScrollView {
                    VStack(alignment: .leading, spacing: 32.0) {
                        PollEditFormTypePicker(selectedType: $viewModel.type)
                        
                        // bwi (#4483) Adds a boolean shoparticpants to the view models, the event and the view
                        if BWIBuildSettings.shared.bwiPollShowParticipantsToggle {
                            PollEditFormParticipationToggle(showParticipants: $viewModel.showParticipants)
                        }
                            
                        VStack(alignment: .leading, spacing: 16.0) {
                            Text(VectorL10n.pollEditFormPollQuestionOrTopic)
                                .font(theme.fonts.title3SB)
                                .foregroundColor(theme.colors.primaryContent)
                            
                            VStack(alignment: .leading, spacing: 8.0) {
                                Text(VectorL10n.pollEditFormQuestionOrTopic)
                                    .font(theme.fonts.subheadline)
                                    .foregroundColor(theme.colors.primaryContent)
                                
                                MultilineTextField(VectorL10n.pollEditFormInputPlaceholder, text: $viewModel.question.text)
                            }
                        }
                        
                        VStack(alignment: .leading, spacing: 16.0) {
                            Text(VectorL10n.pollEditFormCreateOptions)
                                .font(theme.fonts.title3SB)
                                .foregroundColor(theme.colors.primaryContent)
                            
                            ForEach(0..<viewModel.answerOptions.count, id: \.self) { index in
                                SafeBindingCollectionEnumerator($viewModel.answerOptions, index: index) { binding in
                                    PollEditFormAnswerOptionView(text: binding.text, index: index) {
                                        withAnimation(.easeInOut(duration: 0.2)) {
                                            viewModel.send(viewAction: .deleteAnswerOption(viewModel.answerOptions[index]))
                                        }
                                    }
                                }
                            }
                        }
                        
                        // bwi: 4769
                        Button {
                            withAnimation(.easeInOut(duration: 0.2)) {
                                viewModel.send(viewAction: .addAnswerOption)
                            }
                        } label: {
                            Text(VectorL10n.pollEditFormAddOption)
                                .foregroundColor(Color(ThemeService.shared().theme.tintColor))
                                .font(theme.fonts.bodySB)
                        }
                        .disabled(!viewModel.viewState.addAnswerOptionButtonEnabled)
                        
                        Spacer()
                        
                        if viewModel.viewState.mode == .creation {
                            Button(BWIL10n.pollEditFormCreatePoll) {
                                viewModel.send(viewAction: .create)
                            }
                            .buttonStyle(PrimaryActionButtonStyle())
                            .disabled(!viewModel.viewState.confirmationButtonEnabled)
                        }
                    }
                    .padding(.vertical, 24.0)
                    .padding(.horizontal, 16.0)
                    .background(Color(ThemeService.shared().theme.backgroundColor))   // bwi: 4769
                    .activityIndicator(show: viewModel.viewState.showLoadingIndicator)
                    .alert(item: $viewModel.alertInfo) { info in
                        Alert(title: Text(info.title),
                              message: Text(info.subtitle),
                              dismissButton: .default(Text(VectorL10n.ok)))
                    }
                    .frame(minHeight: proxy.size.height) // Make the VStack fill the ScrollView's parent
                    .toolbar {
                        ToolbarItem(placement: .navigationBarLeading) {
                            Button {
                                viewModel.send(viewAction: .cancel)
                            } label: {
                                Text(VectorL10n.cancel)
                                    .foregroundColor(Color(ThemeService.shared().theme.tintColor))
                            }
                        }
                        ToolbarItem(placement: .principal) {
                            Text(BWIL10n.pollEditFormCreatePoll)
                                .font(.headline)
                                .foregroundColor(theme.colors.primaryContent)
                        }
                        
                        ToolbarItem(placement: .navigationBarTrailing) {
                            if viewModel.viewState.mode == .editing {
                                Button {
                                    viewModel.send(viewAction: .update)
                                } label: {
                                    Text(VectorL10n.save)
                                        .foregroundColor(Color(ThemeService.shared().theme.tintColor))
                                }
                                .disabled(!viewModel.viewState.confirmationButtonEnabled)
                            }
                        }
                    }
                    .navigationBarTitleDisplayMode(.inline)
                    .introspectNavigationController { navigationController in
                        ThemeService.shared().theme.applyStyle(onNavigationBar: navigationController.navigationBar)
                    }
                }
            }
        }
        .accentColor(theme.colors.accent)
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

// MARK: - Previews

struct PollEditForm_Previews: PreviewProvider {
    static let stateRenderer = MockPollEditFormScreenState.stateRenderer
    static var previews: some View {
        stateRenderer.screenGroup()
    }
}
