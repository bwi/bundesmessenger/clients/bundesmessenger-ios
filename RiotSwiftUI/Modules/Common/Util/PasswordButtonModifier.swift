//
// Copyright 2021-2024 New Vector Ltd.
//
// SPDX-License-Identifier: AGPL-3.0-only
// Please see LICENSE in the repository root for full details.
//

import SwiftUI

/// Adds a reveal password button (e.g. an eye button) on the
/// right side of the view. For use with `ThemableTextField`.
struct PasswordButtonModifier: ViewModifier {
    // MARK: - Properties
    
    let text: String
    @Binding var isSecureTextVisible: Bool
    let alignment: VerticalAlignment
    
    // MARK: - Private
    
    @Environment(\.theme) private var theme: ThemeSwiftUI
    @ScaledMetric private var iconSize = BWIBuildSettings.shared.bwiEnableBuMUI ? 22 : 16

    // MARK: - Public
    
    public func body(content: Content) -> some View {
        HStack(alignment: .center) {
            content
            
            if !text.isEmpty {
                Button { isSecureTextVisible.toggle() } label: {
                    // bwi: MESSENGER-3948
                    Image(isSecureTextVisible ? Asset.Images.hidePasswordButton.name : Asset.Images.revealPasswordButton.name)
                        .renderingMode(.template)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: iconSize, height: iconSize)
                        .foregroundColor(theme.colors.secondaryContent)
                }
                .accessibilityLabel(isSecureTextVisible ? BWIL10n.textfieldHideSecret : BWIL10n.textfieldRevealSecret)
                .padding(.top, alignment == .top ? 8 : 0)
                .padding(.bottom, alignment == .bottom ? 8 : 0)
                .padding(.trailing, 12)
            }
        }
    }
}
